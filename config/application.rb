require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

# Store security critical values in a separate file on the server, not in the repository
SECRETS_ENV_PATH = File.expand_path('rails_secrets_sp.rb', '/etc')
require SECRETS_ENV_PATH if File.exists?(SECRETS_ENV_PATH)

# Outgoing mail settings
ActionMailer::Base.smtp_settings = {
  address:        ENV['MAILER_SMTP_SERVER'],
  port:           ENV['MAILER_SMTP_PORT'] || 25,
  user_name:      ENV['MAILER_SMTP_USERNAME'],
  password:       ENV['MAILER_SMTP_PASSWORD'],
  authentication: :plain
}

module Main
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    config.i18n.available_locales = %w{ en de }
    config.i18n.default_locale = :en

    config.action_mailer.default_url_options = {
      host: 'www.racespace.net'
    }
    
    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true
    
    config.active_job.queue_adapter = :sidekiq
    config.active_job.queue_name_prefix = Rails.env
  end
end
