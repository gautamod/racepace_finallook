Rails.application.routes.draw do

  apipie
  #
  # Web Application
  #
  resources :virtual_races
  resources :metrics
  mount FullcalendarEngine::Engine => "/trainingplan"
  
  resources :trainingplans
  resources :planedunits
  resources :unitpools
  resources :workouts
  resources :zones
  resources :comments

  root to: 'landing#index'
  get 'dashboards/dashboard_0'
  get 'dashboards/dashboard_1'
  
  get 'racepaces/index'
  
   # All routes
  get "virtual_race/race_view"
  
  
  # Jasmine
  mount JasmineRails::Engine => '/specs' if defined?(JasmineRails)

  # Devise
  devise_for :users

  # EasyCaptcha
  match 'captcha', to: EasyCaptcha::Controller.action(:captcha), via: :get
  
  # Root route
  #root 'imported_race_results#search'
  # root 'visitors#home'
  
  # VisitorsController
  get 'visitors/home'

  # UsersController
  resources :users, only: [ :index, :show ] do
    match 'roles', to: 'users#show_roles', via: [ :get ]
    match 'roles', to: 'users#update_roles', via: [ :patch, :put ]
    match 'users/:id', to: 'users#show', via: :get
  end

  resources :users do
    member do
      get :follow
      get :stop_following
    end
  end

  # PreferencesController
  resources :preferences, only: [ :show, :update ]

  # GroupsController
  resources :groups

  # ImportedRaceResultsController
  get  'imported_race_results/search'
  post 'imported_race_results/search'
  get "appviews/profile"

  match 'imported_race_results/add_new_comment' => 'imported_race_results#add_new_comment', :as => 'add_new_comment_to_posts', :via => [:post]
  match 'imported_race_results/:id/like' => 'imported_race_results#like', :as => 'like_imported_race_result', :via => [:get]
  match 'imported_race_results/:id/unlike' => 'imported_race_results#unlike', :as => 'unlike_imported_race_result', :via => [:get]
  
  match '/users/:id/allocate_race_result',    to: 'users#allocate_race_result',    via: 'get', :as => :allocate_race_result
  match '/users/:id/unallocate_race_result',    to: 'users#unallocate_race_result',    via: 'get', :as => :unallocate_race_result

  match '/tools/calc_vdot', to: 'tools#calc_vdot', via: 'get', :as => :calc_vdot
  match '/tools/qualigel', to: 'tools#qualigel', via: 'get', :as => :qualigel
  
  resources :imported_race_results, only: [ :show ]


  #
  # API
  #

  namespace :api, defaults: { format: :json }, path: '/api'  do

    #
    # v1
    #

    namespace :v1 do
    
      # ImportedRaceResultsController
      get 'imported_race_results/search'
      
      resources :imported_race_results, only: [ :show ]

      # PreferencesController
      resources :preferences, only: [ :show, :update ]
      
      # GroupsController
      resources :groups

    end
  
  end

end
