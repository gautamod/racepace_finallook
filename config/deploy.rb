# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'sp'
#set :repo_url, 'ssh://git@bitbucket.org/sports4u/main.git'
set :repo_url, 'ssh://git@bitbucket.org/racepace/racepace.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :branch, ENV['BRANCH'] if ENV['BRANCH'] 
puts "Deploying branch #{ENV['BRANCH'] if ENV['BRANCH']}"

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/assets', 'public/system', 'storage', 'tmp/data')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Detailed error log
# set :bundle_flags, '--verbose'

# Use home directory for temporary storage
set :tmp_dir, "/home/#{%x{whoami}.chomp}/tmp"

# Configuration for capistrano_passenger
set :passenger_restart_with_touch, true

# Configuration for capistrano_rbenv
set :rbenv_ruby, File.read('.ruby-version').strip
set :rbenv_custom_path, '/opt/rbenv'

namespace :deploy do

  after :restart, 'cache:clear'
  before :updated, 'bower:install'

end
