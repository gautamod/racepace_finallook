module Greif

	def self.create_greif_marathon_plan(athlete_id, coach_id, raceduration, racedate, unitsperweek) 

	autor="Peter Greif"
	target="Marathon 8 Weeks"

	#User Input, this should later be entered by the user in a form
	#Raceduration=3*3600 + 48*60 # 3h48 in seconds
	#Raceduration=3*3600 + 55*60 # 4h00 in seconds
	#Racedate="2015-10-25"
	#UnitsPerWeek=4 #fixme: plan only for 4 units per week implemented
	racepace=raceduration/42.195 # Unit min/km in seconds eg. 4:00min/km = 240

	#Define all different RacePaces
	#fixme: add :unique key to DB instead of find_or_create_by to avoid dublicates

	puts "############ Create ZONES ####################"
	z=Zone.find_or_create_by(name: "M-Renntempo", description: "Marathon Renntempo", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id) 
	z.update(low: racepace, high: racepace)
    z=Zone.find_or_create_by(name: "HM-Renntempo", description: "Halbmarathon Renntempo", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id) 
    z.update(low: racepace-14, high: racepace-14)
    z=Zone.find_or_create_by(name: "15km-Tempo-DL", description: "15km Tempo Dauerlauf", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id) 
    z.update(low: racepace, high: racepace)
    z=Zone.find_or_create_by(name: "10km-Tempo-DL", description: "10km Tempo Dauerlauf", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id) 
    z.update(low: racepace-10, high: racepace-10)
    z=Zone.find_or_create_by(name: "int.-DL", description: "intensiver Dauerlauf", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id) 
    z.update(low: racepace+10, high: racepace+15)
    z=Zone.find_or_create_by(name: "ext.-DL", description: "extensiver Dauerlauf", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id) 
    z.update(low: racepace+45, high: racepace+60)
    z=Zone.find_or_create_by(name: "reg.-DL", description: "Regenerativer Dauerlauf", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id) 
    z.update(low: racepace+60, high: racepace+75)
    z=Zone.find_or_create_by(name: "1km Intervalle", description: "1km Intervalle", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id) 
    z.update(low: racepace-30, high: racepace-25)
    z=Zone.find_or_create_by(name: "2km Intervalle", description: "2km Intervalle", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id) 
    z.update(low: racepace-28, high: racepace-22)
    z=Zone.find_or_create_by(name: "3km Intervalle", description: "3km Intervalle", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id)
	z.update(low: racepace-20, high: racepace-15)


	zone_mara=Zone.where(name: "M-Renntempo", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id).first
	zone_hmara=Zone.where(name: "HM-Renntempo", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id).first
	zone_15TDL=Zone.where(name: "15km-Tempo-DL", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id).first
	zone_10TDL=Zone.where(name: "10km-Tempo-DL", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id).first
	zone_intDL=Zone.where(name: "int.-DL", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id).first
	zone_extDL=Zone.where(name: "ext.-DL", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id).first
	zone_regDL=Zone.where(name: "reg.-DL", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id).first
	zone_1kmIV=Zone.where(name: "1km Intervalle", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id).first
	zone_2kmIV=Zone.where(name: "2km Intervalle", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id).first
	zone_3kmIV=Zone.where(name: "3km Intervalle", variant: "Pace", 
			    calc_base: "DistanceTime", method: "Peter Greif", 
			    sport_type_id: SportType.find_by_description("Run").id, 
			    user_id: athlete_id).first
	# rp_mara=Runpace.find_or_create_by(rpname: "M-Renntempo", rpfast: racepace, rpslow:racepace)
	# rp_hmara=Runpace.find_or_create_by(rpname: "HM-Renntempo", rpfast: racepace-14, rpslow: racepace-14)
	# rp_15TDL=Runpace.find_or_create_by(rpname: "15km-Tempo-DL", rpfast: racepace, rpslow: racepace)
	# rp_10TDL=Runpace.find_or_create_by(rpname: "10km-Tempo-DL", rpfast: racepace-10, rpslow: racepace-10)
	# rp_intDL=Runpace.find_or_create_by(rpname: "int. DL", rpfast: racepace+10, rpslow: racepace+15)
	# rp_extDL=Runpace.find_or_create_by(rpname: "ext. DL", rpfast: racepace+45, rpslow: racepace+60)
	# rp_regDL=Runpace.find_or_create_by(rpname: "reg. DL", rpfast: racepace+60, rpslow: racepace+75)
	# rp_1kmIV=Runpace.find_or_create_by(rpname: "1km-Wiederholung", rpfast: racepace-30, rpslow: racepace-25)
	# rp_2kmIV=Runpace.find_or_create_by(rpname: "2km-Wiederholung", rpfast: racepace-28, rpslow: racepace-22)
	# rp_3kmIV=Runpace.find_or_create_by(rpname: "3km-Wiederholung", rpfast: racepace-20, rpslow: racepace-15)
	# rp_trab=Runpace.find_or_create_by(rpname: "Trab", rpfast: 99901, rpslow: 99901) #fixme: Traben has no fixed speed: 99901 used for trab speed

	
	#Create all needed Unittemplates
	intDL=Unitpool.create(upname: "intensiver Dauerlauf", upnoiv: 1, upivpace: zone_mara.low, upivdistance: 0, upivhr: 76)
	intervalls_5x1km_1kmPause_mr=Unitpool.create(upname: "5x1km(MRT) mit 1km Trabpause", upnoiv: 6, upivpace: zone_mara.low, upivdistance: 1000, upivhr: 90, uppausedistance: 1000, uppausepace: zone_regDL.low)
	intervalls_6x1km_1kmPause=Unitpool.create(upname: "6x1km mit 1km Trabpause", upnoiv: 6, upivpace: zone_1kmIV.low, upivdistance: 1000, upivhr: 90, uppausedistance: 1000, uppausepace: zone_regDL.low)
	intervalls_3x3km_2kmPause=Unitpool.create(upname: "3x3km mit 2km Trabpause", upnoiv: 3, upivpace: zone_3kmIV.low, upivdistance: 3000, upivhr: 90, uppausedistance: 2000, uppausepace: zone_regDL.low)
	intervalls_3x3km_2kmPause_fast=Unitpool.create(upname: "3x3km mit 2km Trabpause", upnoiv: 3, upivpace: zone_3kmIV.high, upivdistance: 3000, upivhr: 90, uppausedistance: 2000, uppausepace: zone_regDL.low)
	intervalls_4x2km_1600mPause=Unitpool.create(upname: "4x2km mit 1.6km Trabpause", upnoiv: 2, upivpace: zone_2kmIV.low, upivdistance: 2000, upivhr: 90, uppausedistance: 1600, uppausepace: zone_regDL.low)
	intervalls_4x2km_1600mPause_fast=Unitpool.create(upname: "4x2km mit 1.6km Trabpause", upnoiv: 2, upivpace: zone_2kmIV.high, upivdistance: 2000, upivhr: 90, uppausedistance: 1600, uppausepace: zone_regDL.low)
	intervalls_4x2km_1600mPause_mr=Unitpool.create(upname: "4x2km(MRT) mit 1.6km Trabpause", upnoiv: 2, upivpace: zone_mara.low, upivdistance: 2000, upivhr: 90, uppausedistance: 1600, uppausepace: zone_regDL.low)
	extDL=Unitpool.create(upname: "ext. Dauerlauf", upnoiv: 1, upivpace: zone_extDL.low, upivdistance: 0, upivhr: 69)
	extDLm3kmEB=Unitpool.create(upname: "ext. Dauerlauf mit 3km Endbeschleunigung", upnoiv: 1, upivpace: zone_extDL.low, upivdistance: 0, upivhr: 69)#fixme upper hr: 85
	extDLm6kmEB=Unitpool.create(upname: "ext. Dauerlauf mit 6km Endbeschleunigung", upnoiv: 1, upivpace: zone_extDL.low, upivdistance: 0, upivhr: 69)#upper hr:85
	extDLm9kmEB=Unitpool.create(upname: "ext. Dauerlauf mit 9km Endbeschleunigung", upnoiv: 1, upivpace: zone_extDL.low, upivdistance: 0, upivhr: 69)#uper hr:85
	extDLm12kmEB_fast=Unitpool.create(upname: "ext. Dauerlauf mit 12km Endbeschleunigung", upnoiv: 1, upivpace: zone_extDL.high, upivdistance: 0, upivhr: 69)#uper hr:85
	extDLm15kmEB_fast=Unitpool.create(upname: "ext. Dauerlauf mit 15km Endbeschleunigung", upnoiv: 1, upivpace: zone_extDL.high, upivdistance: 0, upivhr: 69)#uper hr:85
	regDL=Unitpool.create(upname: "reg. Dauerlauf", upnoiv: 1, upivpace: zone_regDL.low, upivdistance: 0, upivhr: 65)
	regDL_fast=Unitpool.create(upname: "reg. Dauerlauf", upnoiv: 1, upivpace: zone_regDL.high, upivdistance: 0, upivhr: 65)
	tDL10km=Unitpool.create(upname: "10km Tempo Dauerlauf", upnoiv: 1, upivpace: zone_10TDL.low, upivdistance: 10000, upivhr: 86)
	tDL15km=Unitpool.create(upname: "15km Tempo Dauerlauf", upnoiv: 1, upivpace: zone_15TDL.low, upivdistance: 15000, upivhr: 84)
	race_marathon=Unitpool.create(upname: "Wettkampf: Marathon", upnoiv: 1, upivpace: zone_mara.low, upivdistance: 42195)

	#Create Trainingplan #fixme: change tpracetime into tpraceduration
	tp=Trainingplan.create(user_id: athlete_id, tpracedate: racedate, tpracetime: Time.at(raceduration).utc.strftime("%I:%M%p"), tpunitsperweek: unitsperweek)

	#Create Units
	
    ############################################################################################
    #Week1
	days_before_race = (55 - 0)
	unit=Unitpool.find_by_upname("intensiver Dauerlauf")
	planed_distance=15000
	planed_duration= planed_distance/1000*unit.upivpace
    planed_avg_pace=unit.upivpace
    tp.workout.create(
		sport_type_id: SportType.find_by_description("Run").id, 
		title: unit.upname, 
		description: "#{(planed_distance/1000).to_s}km #{unit.upname} bei einer Pace von #{Time.at(unit.upivpace).utc.strftime("%M:%S")}min/km und einem Puls von #{unit.upivhr}%. Dauer ca. #{Time.at(planed_duration).utc.strftime("%H:%M:%S")}.", 
		planed_distance: planed_distance, 
		planed_duration: planed_duration,
		autor: autor, target: target, athlete_id: athlete_id, coach_id: coach_id,
		days_before_race: days_before_race, planed_datetime: racedate-days_before_race.days
		)

	days_before_race = (55 - 2)
	unit=Unitpool.find_by_upname("6x1km mit 1km Trabpause")
	planed_distance=12000
	tp.workout.create(
		sport_type_id: SportType.find_by_description("Run").id, 
		title: unit.upname, 
		description: "#{(planed_distance/1000).to_s}km #{unit.upname} bei einer Pace #{Time.at(unit.upivpace).utc.strftime("%M:%S")}min/km #{Time.at(unit.upivpace).utc.strftime("%M:%S")}min/km und einem Puls von #{unit.upivhr}%", 
		planed_distance: planed_distance, 
		autor: autor, target: target, athlete_id: athlete_id, coach_id: coach_id,
		days_before_race: days_before_race, planed_datetime: racedate-days_before_race.days
		)

	days_before_race = (55 - 5)
	unit=Unitpool.find_by_upname("ext. Dauerlauf")
	planed_distance=35000
	planed_duration= planed_distance/1000*unit.upivpace
	tp.workout.create(
		sport_type_id: SportType.find_by_description("Run").id, 
		title: unit.upname, 
		description: "#{(planed_distance/1000).to_s}km #{unit.upname} bei einer Pace #{Time.at(unit.upivpace).utc.strftime("%M:%S")}min/km #{Time.at(unit.upivpace).utc.strftime("%M:%S")}min/km und einem Puls von #{unit.upivhr}%. Dauer ca. #{Time.at(planed_duration).utc.strftime("%H:%M:%S")}.", 
		planed_distance: planed_distance, 
		planed_duration: planed_duration,
		autor: autor, target: target, athlete_id: athlete_id, coach_id: coach_id,
		days_before_race: days_before_race, planed_datetime: racedate-days_before_race.days
		)

    days_before_race = (55 - 6)
	unit=Unitpool.find_by_upname("reg. Dauerlauf")
	planed_distance=20000
	planed_duration= planed_distance/1000*unit.upivpace
	tp.workout.create(
		sport_type_id: SportType.find_by_description("Run").id, 
		title: unit.upname, 
		description: "#{(planed_distance/1000).to_s}km #{unit.upname} bei einer Pace #{Time.at(unit.upivpace).utc.strftime("%M:%S")}min/km #{Time.at(unit.upivpace).utc.strftime("%M:%S")}min/km und einem Puls von #{unit.upivhr}%. Dauer ca. #{Time.at(planed_duration).utc.strftime("%H:%M:%S")}.", 
		planed_distance: planed_distance, 
		planed_duration: planed_duration,
		autor: autor, target: target, athlete_id: athlete_id, coach_id: coach_id,
		days_before_race: days_before_race, planed_datetime: racedate-days_before_race.days
		)

	#Week2
    days_before_race = (55 - 7)
	unit=Unitpool.find_by_upname("10km Tempo Dauerlauf")
	planed_distance=10000
	planed_duration= planed_distance/1000*unit.upivpace
	tp.workout.create(
		sport_type_id: SportType.find_by_description("Run").id, 
		title: unit.upname, 
		description: "#{(planed_distance/1000).to_s}km #{unit.upname} bei einer Pace #{Time.at(unit.upivpace).utc.strftime("%M:%S")}min/km #{Time.at(unit.upivpace).utc.strftime("%M:%S")}min/km und einem Puls von #{unit.upivhr}%. Dauer ca. #{Time.at(planed_duration).utc.strftime("%H:%M:%S")}.", 
		planed_distance: planed_distance, 
		planed_duration: planed_duration,
		autor: autor, target: target, athlete_id: athlete_id, coach_id: coach_id,
		days_before_race: days_before_race, planed_datetime: racedate-days_before_race.days
		)

    days_before_race = (55 - 9)
	unit=Unitpool.find_by_upname("3x3km mit 2km Trabpause")
	planed_distance=15000
	tp.workout.create(
		sport_type_id: SportType.find_by_description("Run").id, 
		title: unit.upname, 
		description: "#{(planed_distance/1000).to_s}km #{unit.upname} bei einer Pace #{Time.at(unit.upivpace).utc.strftime("%M:%S")}min/km #{Time.at(unit.upivpace).utc.strftime("%M:%S")}min/km und einem Puls von #{unit.upivhr}%.", 
		planed_distance: planed_distance, 
		autor: autor, target: target, athlete_id: athlete_id, coach_id: coach_id,
		days_before_race: days_before_race, planed_datetime: racedate-days_before_race.days
		)

    days_before_race = (55 - 12)
	unit=Unitpool.find_by_upname("ext. Dauerlauf mit 3km Endbeschleunigung")
	planed_distance=35000
	tp.workout.create(
		sport_type_id: SportType.find_by_description("Run").id, 
		title: unit.upname, 
		description: "#{(planed_distance/1000).to_s}km #{unit.upname} bei einer Pace #{Time.at(unit.upivpace).utc.strftime("%M:%S")}min/km #{Time.at(unit.upivpace).utc.strftime("%M:%S")}min/km und einem Puls von #{unit.upivhr}%.", 
		planed_distance: planed_distance, 
		autor: autor, target: target, athlete_id: athlete_id, coach_id: coach_id,
		days_before_race: days_before_race, planed_datetime: racedate-days_before_race.days
		)
	
    days_before_race = (55 - 13)
	unit=Unitpool.find_by_upname("reg. Dauerlauf")
	planed_distance=20000
	tp.workout.create(
		sport_type_id: SportType.find_by_description("Run").id, 
		title: unit.upname, 
		description: "#{(planed_distance/1000).to_s}km #{unit.upname} bei einer Pace #{Time.at(unit.upivpace).utc.strftime("%M:%S")}min/km #{Time.at(unit.upivpace).utc.strftime("%M:%S")}min/km und einem Puls von #{unit.upivhr}%.", 
		planed_distance: planed_distance, 
		autor: autor, target: target, athlete_id: athlete_id, coach_id: coach_id,
		days_before_race: days_before_race, planed_datetime: racedate-days_before_race.days
		)
	


	#Create Units
	#Week1
	#pu2=tp.planedunit.create(pudate: tp.tpracedate - (55 - 2).day, puunitpoolid: intervalls_6x1km_1kmPause.id, pudistance: 12000)
	#pu3=tp.planedunit.create(pudate: tp.tpracedate - (55 - 5).day, puunitpoolid: extDL.id, pudistance: 35000)
	#pu4=tp.planedunit.create(pudate: tp.tpracedate - (55 - 6).day, puunitpoolid: regDL.id, pudistance: 20000)
	#Week2
	#pu5=tp.planedunit.create(pudate: tp.tpracedate - (55 - 7).day, puunitpoolid: tDL10km.id, pudistance: tDL10km.upivdistance)
	#pu6=tp.planedunit.create(pudate: tp.tpracedate - (55 - 9).day, puunitpoolid: intervalls_3x3km_2kmPause.id, pudistance: 15000)
	#pu7=tp.planedunit.create(pudate: tp.tpracedate - (55 - 12).day, puunitpoolid: extDLm3kmEB.id, pudistance: 35000)
	#pu8=tp.planedunit.create(pudate: tp.tpracedate - (55 - 13).day, puunitpoolid: regDL.id, pudistance: 20000)
	#Week3
	pu9=tp.planedunit.create(pudate: tp.tpracedate - (55 - 14).day, puunitpoolid: tDL15km.id, pudistance: tDL15km.upivdistance)
	pu10=tp.planedunit.create(pudate: tp.tpracedate - (55 - 16).day, puunitpoolid: intervalls_4x2km_1600mPause.id, pudistance: 14000)
	pu11=tp.planedunit.create(pudate: tp.tpracedate - (55 - 19).day, puunitpoolid: extDLm6kmEB.id, pudistance: 35000)
	pu12=tp.planedunit.create(pudate: tp.tpracedate - (55 - 20).day, puunitpoolid: regDL.id, pudistance: 20000)
	#Week4
	pu13=tp.planedunit.create(pudate: tp.tpracedate - (55 - 21).day, puunitpoolid: tDL10km.id, pudistance: tDL10km.upivdistance)
	pu14=tp.planedunit.create(pudate: tp.tpracedate - (55 - 23).day, puunitpoolid: intervalls_6x1km_1kmPause.id, pudistance: 12000)
	pu15=tp.planedunit.create(pudate: tp.tpracedate - (55 - 26).day, puunitpoolid: extDLm9kmEB.id, pudistance: 35000)
	pu16=tp.planedunit.create(pudate: tp.tpracedate - (55 - 27).day, puunitpoolid: regDL.id, pudistance: 20000)
	#Week5
	pu17=tp.planedunit.create(pudate: tp.tpracedate - (55 - 28).day, puunitpoolid: regDL.id, pudistance: 15000)
	pu18=tp.planedunit.create(pudate: tp.tpracedate - (55 - 30).day, puunitpoolid: intervalls_3x3km_2kmPause_fast.id, pudistance: 15000)
	pu19=tp.planedunit.create(pudate: tp.tpracedate - (55 - 33).day, puunitpoolid: extDLm12kmEB_fast.id, pudistance: 35000)
	pu20=tp.planedunit.create(pudate: tp.tpracedate - (55 - 34).day, puunitpoolid: regDL_fast.id, pudistance: 20000)
	#Week6
	pu21=tp.planedunit.create(pudate: tp.tpracedate - (55 - 35).day, puunitpoolid: tDL10km.id, pudistance: tDL10km.upivdistance)
	pu22=tp.planedunit.create(pudate: tp.tpracedate - (55 - 37).day, puunitpoolid: intervalls_4x2km_1600mPause_fast.id, pudistance: 14000)
	pu23=tp.planedunit.create(pudate: tp.tpracedate - (55 - 40).day, puunitpoolid: extDLm15kmEB_fast.id, pudistance: 35000)
	pu24=tp.planedunit.create(pudate: tp.tpracedate - (55 - 41).day, puunitpoolid: regDL_fast.id, pudistance: 20000)
	#Week7
	pu25=tp.planedunit.create(pudate: tp.tpracedate - (55 - 42).day, puunitpoolid: intervalls_6x1km_1kmPause.id, pudistance: 12000)
	pu26=tp.planedunit.create(pudate: tp.tpracedate - (55 - 44).day, puunitpoolid: tDL15km.id, pudistance: tDL15km.upivdistance)
	pu27=tp.planedunit.create(pudate: tp.tpracedate - (55 - 47).day, puunitpoolid: regDL_fast.id, pudistance: 35000)
	pu28=tp.planedunit.create(pudate: tp.tpracedate - (55 - 48).day, puunitpoolid: regDL_fast.id, pudistance: 15000)
	#Week8
	pu29=tp.planedunit.create(pudate: tp.tpracedate - (55 - 49).day, puunitpoolid: intervalls_4x2km_1600mPause_mr.id, pudistance: 14000)
	pu30=tp.planedunit.create(pudate: tp.tpracedate - (55 - 51).day, puunitpoolid: intervalls_5x1km_1kmPause_mr.id, pudistance: 10000)
	pu31=tp.planedunit.create(pudate: tp.tpracedate - (55 - 54).day, puunitpoolid: regDL_fast.id, pudistance: 2000) #fixme: add planed time eg:moring
	pu32=tp.planedunit.create(pudate: tp.tpracedate - (55 - 54).day, puunitpoolid: regDL_fast.id, pudistance: 2000) #fixme: add planed time eg:evening
	pu33=tp.planedunit.create(pudate: tp.tpracedate - (55 - 55).day, puunitpoolid: race_marathon.id, pudistance: 42195) 


	puts "############ GREIF MARATHON TRAINING PLAN ####################"
	puts "# Raceday: #{tp.tpracedate.strftime("%d/%m/%Y")}"
	puts "# Planed duration: #{tp.tpracetime.utc.strftime("%I:%M:%S")}"
	puts "# Target pace: #{(zone_mara.low/60).to_s}:#{(zone_mara.low % 60).to_s}min/km " 
	puts "# Units per week: #{tp.tpunitsperweek}"
	puts " "

	tp.workout.each do |workout| 
		workout.build_event(
			:title => workout.title, 
		    :description => workout.description, 
		    :starttime => workout.planed_datetime, 
		    :endtime =>  workout.planed_datetime + workout.planed_duration.to_i.seconds,
		    :all_day =>  false
			).save
 	   puts "Creating event for #{workout.title}" 
	end
	
    end

end	