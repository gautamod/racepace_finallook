# Store uploaded files in $RAILS_ROOT/storage instead of public directory
Paperclip::Attachment.default_options[:path] = ":rails_root/storage:url"

# Include environment in path in order to avoid collisions
Paperclip::Attachment.default_options[:url] = "/system/:rails_env/:class/:attachment/:id_partition/:style/:hash"

# Use hashes as file names, this is an extra precaution against possible exploits in user-supplied data
Paperclip::Attachment.default_options[:hash_secret] = ENV['PAPERCLIP_HASH_SECRET'] || '8ea8b5883a87eb1074f219003e0c9f5a5512bea0f5a8dd3cc4f416c1fcfc8b67aead408887dad6a3f5aaf4cd7eeae444956528cc34d5120b72ca5b6e360b58a9'

# Content types as reported by 'file -b --mime-type'
Paperclip.options[:content_type_mappings] = {
  tcx: 'application/xml',
  fit: 'application/octet-stream'
}
