
ActiveModel::Serializer.config.adapter = :json_api

ActiveModel::Serializer.config.jsonapi_resource_type = :plural
ActiveModel::Serializer.config.jsonapi_include_toplevel_object = true
