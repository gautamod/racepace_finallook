module NegativeCaptchaHelper
  extend ActiveSupport::Concern

  included do
  end

private

  def negative_captcha_setup
    @negative_captcha = NegativeCaptcha.new(
      secret: ENV['NEGATIVE_CAPTCHA_SECRET'] || 'c26ebe9e2da500f9c2e23e0eb2033a66bf23f736e817900028203dd29e14954f6e458f3107f6e4540dc799d7c34dd546cf87c1bfd75abb22815cd4a16ad3995c',
      spinner: request.remote_ip,
      fields: %i{ my_password captcha },
      css: "position: absolute; left: -3000px;",
      params: params
    )
  end

  def negative_captcha_check
    unless request.get?
      if params[:timestamp] && params[:spinner]
        unless @negative_captcha.valid?
          flash[:alert] = I18n.t 'common.errors.negative_captcha'
          redirect_to root_path
        else
          params[:captcha] = @negative_captcha.values['captcha'] if @negative_captcha.values['captcha']
        end
      end
    end
  end

end
