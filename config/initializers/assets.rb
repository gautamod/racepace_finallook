# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path

# Assets managed by Bower
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'bower_components')

# Bootstrap
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'bower_components', 'bootstrap-sass-official', 'assets', 'fonts')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w{ *.svg *.eot *.woff *.woff2 *.ttf }

Rails.application.config.assets.precompile += %w( erd/cancel.png )
Rails.application.config.assets.precompile += %w( erd/background.png )

Rails.application.config.assets.precompile += %w( preferences.css )
Rails.application.config.assets.precompile += %w( preferences.js )

Rails.application.config.assets.precompile += %w( imported_races.css )
Rails.application.config.assets.precompile += %w( imported_races.js )
Rails.application.config.assets.precompile += %w( imported_race_results.css )
Rails.application.config.assets.precompile += %w( imported_race_results.js )
Rails.application.config.assets.precompile += %w( race_statistics.css )
Rails.application.config.assets.precompile += %w( race_statistics.js )
Rails.application.config.assets.precompile += %w( workouts.css )
Rails.application.config.assets.precompile += %w( workouts.js )
Rails.application.config.assets.precompile += %w( metrics.css )
Rails.application.config.assets.precompile += %w( metrics.js )

Rails.application.config.assets.precompile += %w( trainingplans.css )
Rails.application.config.assets.precompile += %w( trainingplans.js )

Rails.application.config.assets.precompile += %w( users.css )
Rails.application.config.assets.precompile += %w( users.js )

Rails.application.config.assets.precompile += %w( result_allocations.css )
Rails.application.config.assets.precompile += %w( result_allocations.js )

Rails.application.config.assets.precompile += %w( sport_types.css )
Rails.application.config.assets.precompile += %w( sport_types.js )

Rails.application.config.assets.precompile += %w( appviews.css )
Rails.application.config.assets.precompile += %w( appviews.js )
Rails.application.config.assets.precompile += %w( cssanimations.css )
Rails.application.config.assets.precompile += %w( cssanimations.js )
Rails.application.config.assets.precompile += %w( dashboards.css )
Rails.application.config.assets.precompile += %w( dashboards.js )
Rails.application.config.assets.precompile += %w( forms.css )
Rails.application.config.assets.precompile += %w( forms.js )
Rails.application.config.assets.precompile += %w( gallery.css )
Rails.application.config.assets.precompile += %w( gallery.js )
Rails.application.config.assets.precompile += %w( graphs.css )
Rails.application.config.assets.precompile += %w( graphs.js )
Rails.application.config.assets.precompile += %w( mailbox.css )
Rails.application.config.assets.precompile += %w( mailbox.js )
Rails.application.config.assets.precompile += %w( miscellaneous.css )
Rails.application.config.assets.precompile += %w( miscellaneous.js )
Rails.application.config.assets.precompile += %w( pages.css )
Rails.application.config.assets.precompile += %w( pages.js )
Rails.application.config.assets.precompile += %w( tables.css )
Rails.application.config.assets.precompile += %w( tables.js )
Rails.application.config.assets.precompile += %w( uielements.css )
Rails.application.config.assets.precompile += %w( uielements.js )
Rails.application.config.assets.precompile += %w( widgets.css )
Rails.application.config.assets.precompile += %w( widgets.js )
Rails.application.config.assets.precompile += %w( commerce.css )
Rails.application.config.assets.precompile += %w( commerce.js )
Rails.application.config.assets.precompile += %w( metrics.js )
Rails.application.config.assets.precompile += %w( comments.css )
Rails.application.config.assets.precompile += %w( groups.css )
Rails.application.config.assets.precompile += %w( groups.js )
Rails.application.config.assets.precompile += %w( slick.css )

Rails.application.config.assets.precompile += %w( turnjs/all.js )
Rails.application.config.assets.precompile += %w( turnjs/bookshelf.js )
Rails.application.config.assets.precompile += %w( turnjs/docs1.js )
Rails.application.config.assets.precompile += %w( turnjs/docs2.js )
Rails.application.config.assets.precompile += %w( turnjs/docs3.js )
Rails.application.config.assets.precompile += %w( turnjs/docs4.js )
Rails.application.config.assets.precompile += %w( turnjs/docs5.js )
Rails.application.config.assets.precompile += %w( turnjs/hash.js )
Rails.application.config.assets.precompile += %w( turnjs/turn.min.js )
Rails.application.config.assets.precompile += %w( turnjs/zoom.min.js )
