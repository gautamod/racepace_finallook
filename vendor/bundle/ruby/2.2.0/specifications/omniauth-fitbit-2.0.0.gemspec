# -*- encoding: utf-8 -*-
# stub: omniauth-fitbit 2.0.0 ruby lib

Gem::Specification.new do |s|
  s.name = "omniauth-fitbit"
  s.version = "2.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["TK Gospodinov"]
  s.date = "2016-02-03"
  s.description = "OmniAuth OAuth2 strategy for Fitbit"
  s.email = ["tk@gospodinov.net"]
  s.homepage = "http://github.com/tkgospodinov/omniauth-fitbit"
  s.rubygems_version = "2.4.5.1"
  s.summary = "OmniAuth OAuth2 strategy for Fitbit"

  s.installed_by_version = "2.4.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<omniauth-oauth2>, ["~> 1.4"])
      s.add_runtime_dependency(%q<multi_xml>, [">= 0"])
    else
      s.add_dependency(%q<omniauth-oauth2>, ["~> 1.4"])
      s.add_dependency(%q<multi_xml>, [">= 0"])
    end
  else
    s.add_dependency(%q<omniauth-oauth2>, ["~> 1.4"])
    s.add_dependency(%q<multi_xml>, [">= 0"])
  end
end
