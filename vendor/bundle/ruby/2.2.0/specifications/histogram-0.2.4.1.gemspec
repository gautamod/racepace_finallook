# -*- encoding: utf-8 -*-
# stub: histogram 0.2.4.1 ruby lib

Gem::Specification.new do |s|
  s.name = "histogram"
  s.version = "0.2.4.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["John T. Prince"]
  s.date = "2015-10-03"
  s.description = "gives objects the ability to 'histogram' in several useful ways"
  s.email = ["jtprince@gmail.com"]
  s.executables = ["histogram"]
  s.files = ["bin/histogram"]
  s.homepage = "https://github.com/jtprince/histogram"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.5.1"
  s.summary = "histograms data in different ways"

  s.installed_by_version = "2.4.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<bundler>, ["~> 1.3"])
      s.add_development_dependency(%q<rake>, ["~> 10.1.0"])
      s.add_development_dependency(%q<simplecov>, ["~> 0.7.1"])
      s.add_development_dependency(%q<rspec>, ["~> 2.13.0"])
      s.add_development_dependency(%q<rdoc>, [">= 0"])
      s.add_development_dependency(%q<narray>, [">= 0"])
    else
      s.add_dependency(%q<bundler>, ["~> 1.3"])
      s.add_dependency(%q<rake>, ["~> 10.1.0"])
      s.add_dependency(%q<simplecov>, ["~> 0.7.1"])
      s.add_dependency(%q<rspec>, ["~> 2.13.0"])
      s.add_dependency(%q<rdoc>, [">= 0"])
      s.add_dependency(%q<narray>, [">= 0"])
    end
  else
    s.add_dependency(%q<bundler>, ["~> 1.3"])
    s.add_dependency(%q<rake>, ["~> 10.1.0"])
    s.add_dependency(%q<simplecov>, ["~> 0.7.1"])
    s.add_dependency(%q<rspec>, ["~> 2.13.0"])
    s.add_dependency(%q<rdoc>, [">= 0"])
    s.add_dependency(%q<narray>, [">= 0"])
  end
end
