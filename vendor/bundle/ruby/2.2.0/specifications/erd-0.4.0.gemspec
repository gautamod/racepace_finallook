# -*- encoding: utf-8 -*-
# stub: erd 0.4.0 ruby lib

Gem::Specification.new do |s|
  s.name = "erd"
  s.version = "0.4.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Akira Matsuda"]
  s.date = "2016-04-09"
  s.description = "erd engine on Rails"
  s.email = ["ronnie@dio.jp"]
  s.homepage = "https://github.com/amatsuda/erd"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.5.1"
  s.summary = "erd engine on Rails"

  s.installed_by_version = "2.4.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rails-erd>, [">= 0.4.5"])
      s.add_runtime_dependency(%q<nokogiri>, [">= 0"])
      s.add_development_dependency(%q<rails>, [">= 3.2"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<test-unit-rails>, [">= 0"])
      s.add_development_dependency(%q<capybara>, [">= 2"])
      s.add_development_dependency(%q<rr>, [">= 0"])
      s.add_development_dependency(%q<sqlite3>, [">= 0"])
    else
      s.add_dependency(%q<rails-erd>, [">= 0.4.5"])
      s.add_dependency(%q<nokogiri>, [">= 0"])
      s.add_dependency(%q<rails>, [">= 3.2"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<test-unit-rails>, [">= 0"])
      s.add_dependency(%q<capybara>, [">= 2"])
      s.add_dependency(%q<rr>, [">= 0"])
      s.add_dependency(%q<sqlite3>, [">= 0"])
    end
  else
    s.add_dependency(%q<rails-erd>, [">= 0.4.5"])
    s.add_dependency(%q<nokogiri>, [">= 0"])
    s.add_dependency(%q<rails>, [">= 3.2"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<test-unit-rails>, [">= 0"])
    s.add_dependency(%q<capybara>, [">= 2"])
    s.add_dependency(%q<rr>, [">= 0"])
    s.add_dependency(%q<sqlite3>, [">= 0"])
  end
end
