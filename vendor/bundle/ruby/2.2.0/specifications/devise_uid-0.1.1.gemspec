# -*- encoding: utf-8 -*-
# stub: devise_uid 0.1.1 ruby lib

Gem::Specification.new do |s|
  s.name = "devise_uid"
  s.version = "0.1.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Jingwen Owen Ou"]
  s.date = "2014-08-12"
  s.description = "Add UID support to Devise"
  s.email = ["jingweno@gmail.com"]
  s.homepage = "https://github.com/jingweno/devise_uid"
  s.rubygems_version = "2.4.5.1"
  s.summary = "Add UID support to Devise"

  s.installed_by_version = "2.4.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<devise>, [">= 3.0.0"])
      s.add_runtime_dependency(%q<railties>, [">= 3.0"])
      s.add_development_dependency(%q<rspec>, ["~> 2.12"])
    else
      s.add_dependency(%q<devise>, [">= 3.0.0"])
      s.add_dependency(%q<railties>, [">= 3.0"])
      s.add_dependency(%q<rspec>, ["~> 2.12"])
    end
  else
    s.add_dependency(%q<devise>, [">= 3.0.0"])
    s.add_dependency(%q<railties>, [">= 3.0"])
    s.add_dependency(%q<rspec>, ["~> 2.12"])
  end
end
