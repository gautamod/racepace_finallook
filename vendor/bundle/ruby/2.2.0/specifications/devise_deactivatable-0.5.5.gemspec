# -*- encoding: utf-8 -*-
# stub: devise_deactivatable 0.5.5 ruby lib

Gem::Specification.new do |s|
  s.name = "devise_deactivatable"
  s.version = "0.5.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Elan Meng"]
  s.date = "2015-09-12"
  s.description = "Deactivate support for Devise."
  s.email = ["dreamwords@gmail.com"]
  s.homepage = "https://github.com/dreamwords/devise_deactivatable"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.5.1"
  s.summary = "Deactivate support for Devise"

  s.installed_by_version = "2.4.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<devise>, [">= 3.0.0"])
    else
      s.add_dependency(%q<devise>, [">= 3.0.0"])
    end
  else
    s.add_dependency(%q<devise>, [">= 3.0.0"])
  end
end
