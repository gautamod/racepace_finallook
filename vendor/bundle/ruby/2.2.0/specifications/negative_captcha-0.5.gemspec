# -*- encoding: utf-8 -*-
# stub: negative_captcha 0.5 ruby lib/

Gem::Specification.new do |s|
  s.name = "negative_captcha"
  s.version = "0.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib/"]
  s.authors = ["Erik Peterson"]
  s.date = "2016-04-07"
  s.description = "A library to make creating negative captchas less painful"
  s.email = ["erik@subwindow.com"]
  s.homepage = "http://github.com/subwindow/negative-captcha"
  s.rubygems_version = "2.4.5.1"
  s.summary = "A library to make creating negative captchas less painful"

  s.installed_by_version = "2.4.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<actionpack>, [">= 0"])
      s.add_runtime_dependency(%q<activesupport>, [">= 0"])
    else
      s.add_dependency(%q<actionpack>, [">= 0"])
      s.add_dependency(%q<activesupport>, [">= 0"])
    end
  else
    s.add_dependency(%q<actionpack>, [">= 0"])
    s.add_dependency(%q<activesupport>, [">= 0"])
  end
end
