# -*- encoding: utf-8 -*-
# stub: ruby-termios 0.9.6 ruby lib .
# stub: extconf.rb

Gem::Specification.new do |s|
  s.name = "ruby-termios"
  s.version = "0.9.6"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib", "."]
  s.authors = ["akira yamada"]
  s.bindir = false
  s.date = "2009-08-28"
  s.description = "Termios module is simple wrapper of termios(3).  It can be included into\nIO-family classes and can extend IO-family objects.  In addition, the methods\ncan use as module function.\n"
  s.email = "akira@arika.org"
  s.extensions = ["extconf.rb"]
  s.extra_rdoc_files = ["README", "ChangeLog"]
  s.files = ["ChangeLog", "README", "extconf.rb"]
  s.homepage = "http://arika.org/ruby/termios"
  s.rdoc_options = ["--title", "ruby-termios documentation", "--charset", "utf-8", "--opname", "index.html", "--line-numbers", "--main", "README", "--inline-source", "--exclude", "^(examples|extras)/"]
  s.rubyforge_project = "termios"
  s.rubygems_version = "2.4.5.1"
  s.summary = "a simple wrapper of termios(3)"

  s.installed_by_version = "2.4.5.1" if s.respond_to? :installed_by_version
end
