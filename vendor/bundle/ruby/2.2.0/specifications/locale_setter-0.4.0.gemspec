# -*- encoding: utf-8 -*-
# stub: locale_setter 0.4.0 ruby lib

Gem::Specification.new do |s|
  s.name = "locale_setter"
  s.version = "0.4.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Jeff Casimir"]
  s.date = "2013-04-04"
  s.description = "Automatically set per-request locale in Rails applications"
  s.email = ["jeff@casimircreative.com"]
  s.homepage = "http://github.com/jcasimir/locale_setter"
  s.rubygems_version = "2.4.5.1"
  s.summary = "Automatically set per-request locale in Rails applications"

  s.installed_by_version = "2.4.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rake>, ["~> 10.0.3"])
      s.add_development_dependency(%q<rspec>, ["~> 2.13.0"])
      s.add_development_dependency(%q<capybara>, [">= 0"])
    else
      s.add_dependency(%q<rake>, ["~> 10.0.3"])
      s.add_dependency(%q<rspec>, ["~> 2.13.0"])
      s.add_dependency(%q<capybara>, [">= 0"])
    end
  else
    s.add_dependency(%q<rake>, ["~> 10.0.3"])
    s.add_dependency(%q<rspec>, ["~> 2.13.0"])
    s.add_dependency(%q<capybara>, [">= 0"])
  end
end
