# -*- encoding: utf-8 -*-
# stub: easy_captcha 0.6.5 ruby lib

Gem::Specification.new do |s|
  s.name = "easy_captcha"
  s.version = "0.6.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Marco Scholl", "Alexander Dreher"]
  s.date = "2011-09-15"
  s.description = "Captcha-Plugin for Rails"
  s.email = "team@phatworx.de"
  s.extra_rdoc_files = ["LICENSE.txt", "README.rdoc"]
  s.files = ["LICENSE.txt", "README.rdoc"]
  s.homepage = "http://github.com/phatworx/easy_captcha"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.5.1"
  s.summary = "Captcha-Plugin for Rails"

  s.installed_by_version = "2.4.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rails>, [">= 3.0.0"])
      s.add_runtime_dependency(%q<bundler>, [">= 1.1.0"])
      s.add_runtime_dependency(%q<simplecov>, [">= 0.3.8"])
      s.add_runtime_dependency(%q<rspec-rails>, [">= 2.8.1"])
      s.add_runtime_dependency(%q<yard>, [">= 0.7.0"])
      s.add_runtime_dependency(%q<rmagick>, [">= 2.13.1"])
    else
      s.add_dependency(%q<rails>, [">= 3.0.0"])
      s.add_dependency(%q<bundler>, [">= 1.1.0"])
      s.add_dependency(%q<simplecov>, [">= 0.3.8"])
      s.add_dependency(%q<rspec-rails>, [">= 2.8.1"])
      s.add_dependency(%q<yard>, [">= 0.7.0"])
      s.add_dependency(%q<rmagick>, [">= 2.13.1"])
    end
  else
    s.add_dependency(%q<rails>, [">= 3.0.0"])
    s.add_dependency(%q<bundler>, [">= 1.1.0"])
    s.add_dependency(%q<simplecov>, [">= 0.3.8"])
    s.add_dependency(%q<rspec-rails>, [">= 2.8.1"])
    s.add_dependency(%q<yard>, [">= 0.7.0"])
    s.add_dependency(%q<rmagick>, [">= 2.13.1"])
  end
end
