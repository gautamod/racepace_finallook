# -*- encoding: utf-8 -*-
# stub: devise_security_extension 0.10.0 ruby lib

Gem::Specification.new do |s|
  s.name = "devise_security_extension"
  s.version = "0.10.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Marco Scholl", "Alexander Dreher"]
  s.date = "2016-03-10"
  s.description = "An enterprise security extension for devise, trying to meet industrial standard security demands for web applications."
  s.email = "team@phatworx.de"
  s.homepage = "https://github.com/phatworx/devise_security_extension"
  s.licenses = ["MIT"]
  s.required_ruby_version = Gem::Requirement.new(">= 1.9.3")
  s.rubyforge_project = "devise_security_extension"
  s.rubygems_version = "2.4.5.1"
  s.summary = "Security extension for devise"

  s.installed_by_version = "2.4.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<railties>, ["< 5.0", ">= 3.2.6"])
      s.add_runtime_dependency(%q<devise>, ["< 4.0", ">= 3.0.0"])
      s.add_development_dependency(%q<bundler>, ["< 2.0", ">= 1.3.0"])
      s.add_development_dependency(%q<sqlite3>, ["~> 1.3.10"])
      s.add_development_dependency(%q<rubocop>, ["~> 0"])
      s.add_development_dependency(%q<minitest>, [">= 0"])
      s.add_development_dependency(%q<easy_captcha>, ["~> 0"])
      s.add_development_dependency(%q<rails_email_validator>, ["~> 0"])
    else
      s.add_dependency(%q<railties>, ["< 5.0", ">= 3.2.6"])
      s.add_dependency(%q<devise>, ["< 4.0", ">= 3.0.0"])
      s.add_dependency(%q<bundler>, ["< 2.0", ">= 1.3.0"])
      s.add_dependency(%q<sqlite3>, ["~> 1.3.10"])
      s.add_dependency(%q<rubocop>, ["~> 0"])
      s.add_dependency(%q<minitest>, [">= 0"])
      s.add_dependency(%q<easy_captcha>, ["~> 0"])
      s.add_dependency(%q<rails_email_validator>, ["~> 0"])
    end
  else
    s.add_dependency(%q<railties>, ["< 5.0", ">= 3.2.6"])
    s.add_dependency(%q<devise>, ["< 4.0", ">= 3.0.0"])
    s.add_dependency(%q<bundler>, ["< 2.0", ">= 1.3.0"])
    s.add_dependency(%q<sqlite3>, ["~> 1.3.10"])
    s.add_dependency(%q<rubocop>, ["~> 0"])
    s.add_dependency(%q<minitest>, [">= 0"])
    s.add_dependency(%q<easy_captcha>, ["~> 0"])
    s.add_dependency(%q<rails_email_validator>, ["~> 0"])
  end
end
