# -*- encoding: utf-8 -*-
# stub: omniauth-runkeeper 1.0.2 ruby lib

Gem::Specification.new do |s|
  s.name = "omniauth-runkeeper"
  s.version = "1.0.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Masaki Takeuchi"]
  s.date = "2016-08-31"
  s.description = "OmniAuth strategy for RunKeeper"
  s.email = ["m.ishihara@gmail.com"]
  s.homepage = "https://github.com/m4i/omniauth-runkeeper"
  s.rubyforge_project = "omniauth-runkeeper"
  s.rubygems_version = "2.4.8"
  s.summary = "OmniAuth strategy for RunKeeper"

  s.installed_by_version = "2.4.8" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_runtime_dependency(%q<omniauth-oauth2>, ["~> 1.0"])
      s.add_runtime_dependency(%q<multi_json>, ["~> 1.0"])
    else
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<omniauth-oauth2>, ["~> 1.0"])
      s.add_dependency(%q<multi_json>, ["~> 1.0"])
    end
  else
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<omniauth-oauth2>, ["~> 1.0"])
    s.add_dependency(%q<multi_json>, ["~> 1.0"])
  end
end
