# -*- encoding: utf-8 -*-
# stub: devise_security_extension 0.9.2 ruby lib

Gem::Specification.new do |s|
  s.name = "devise_security_extension"
  s.version = "0.9.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Marco Scholl", "Alexander Dreher"]
  s.date = "2015-04-02"
  s.description = "An enterprise security extension for devise, trying to meet industrial standard security demands for web applications."
  s.email = "team@phatworx.de"
  s.extra_rdoc_files = ["LICENSE.txt", "README.md"]
  s.files = ["LICENSE.txt", "README.md"]
  s.homepage = "http://github.com/phatworx/devise_security_extension"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.8"
  s.summary = "Security extension for devise"

  s.installed_by_version = "2.4.8" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rails>, [">= 3.1.1"])
      s.add_runtime_dependency(%q<devise>, [">= 2.0.0"])
      s.add_development_dependency(%q<rails_email_validator>, [">= 0"])
      s.add_development_dependency(%q<easy_captcha>, [">= 0"])
      s.add_development_dependency(%q<bundler>, [">= 1.0.0"])
      s.add_development_dependency(%q<jeweler>, ["~> 2.0.1"])
      s.add_development_dependency(%q<sqlite3>, [">= 0"])
    else
      s.add_dependency(%q<rails>, [">= 3.1.1"])
      s.add_dependency(%q<devise>, [">= 2.0.0"])
      s.add_dependency(%q<rails_email_validator>, [">= 0"])
      s.add_dependency(%q<easy_captcha>, [">= 0"])
      s.add_dependency(%q<bundler>, [">= 1.0.0"])
      s.add_dependency(%q<jeweler>, ["~> 2.0.1"])
      s.add_dependency(%q<sqlite3>, [">= 0"])
    end
  else
    s.add_dependency(%q<rails>, [">= 3.1.1"])
    s.add_dependency(%q<devise>, [">= 2.0.0"])
    s.add_dependency(%q<rails_email_validator>, [">= 0"])
    s.add_dependency(%q<easy_captcha>, [">= 0"])
    s.add_dependency(%q<bundler>, [">= 1.0.0"])
    s.add_dependency(%q<jeweler>, ["~> 2.0.1"])
    s.add_dependency(%q<sqlite3>, [">= 0"])
  end
end
