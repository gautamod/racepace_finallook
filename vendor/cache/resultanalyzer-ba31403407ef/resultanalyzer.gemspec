# -*- encoding: utf-8 -*-
# stub: resultanalyzer 1.2.1 ruby lib

Gem::Specification.new do |s|
  s.name = "resultanalyzer"
  s.version = "1.2.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Nico Weling"]
  s.date = "2016-11-09"
  s.description = "Fills some tables and views with statistical race_result analysis"
  s.email = ["nico.weling@googlemail.com"]
  s.files = ["MIT-LICENSE", "README.rdoc", "Rakefile", "app/assets", "app/assets/images", "app/assets/images/resultanalyzer", "app/assets/javascripts", "app/assets/javascripts/race_statistics.js", "app/assets/javascripts/resultanalyzer", "app/assets/stylesheets", "app/assets/stylesheets/race_statistics.css", "app/assets/stylesheets/resultanalyzer", "app/assets/stylesheets/scaffold.css", "app/controllers", "app/controllers/race_statistics_controller.rb", "app/helpers", "app/helpers/race_statistics_helper.rb", "app/mailers", "app/models", "app/models/race_statistic.rb", "app/policies", "app/policies/result_analyzer_policy.rb", "app/views", "app/views/race_statistics", "app/views/race_statistics/_form.html.erb", "app/views/race_statistics/edit.html.erb", "app/views/race_statistics/index.html.erb", "app/views/race_statistics/ld_vdot_stats.html.erb", "app/views/race_statistics/md_vdot_stats.html.erb", "app/views/race_statistics/new.html.erb", "app/views/race_statistics/show.html.erb", "app/views/race_statistics/time_stats.html.erb", "app/views/race_statistics/vdot_stats.html.erb", "config/routes.rb", "db/migrate", "db/migrate/20151122131604_create_race_statistics.rb", "db/migrate/20160202170637_add_vdot_level_count_to_race_statistics.rb", "lib/resultanalyzer", "lib/resultanalyzer.rb", "lib/resultanalyzer/engine.rb", "lib/resultanalyzer/version.rb", "lib/tasks", "lib/tasks/resultanalyzer_tasks.rake", "test/controllers", "test/controllers/race_statistics_controller_test.rb", "test/dummy", "test/dummy/README.rdoc", "test/dummy/Rakefile", "test/dummy/app", "test/dummy/app/assets", "test/dummy/app/assets/images", "test/dummy/app/assets/javascripts", "test/dummy/app/assets/javascripts/application.js", "test/dummy/app/assets/stylesheets", "test/dummy/app/assets/stylesheets/application.css", "test/dummy/app/controllers", "test/dummy/app/controllers/application_controller.rb", "test/dummy/app/controllers/concerns", "test/dummy/app/helpers", "test/dummy/app/helpers/application_helper.rb", "test/dummy/app/mailers", "test/dummy/app/models", "test/dummy/app/models/concerns", "test/dummy/app/views", "test/dummy/app/views/layouts", "test/dummy/app/views/layouts/application.html.erb", "test/dummy/bin", "test/dummy/bin/bundle", "test/dummy/bin/rails", "test/dummy/bin/rake", "test/dummy/config", "test/dummy/config.ru", "test/dummy/config/application.rb", "test/dummy/config/boot.rb", "test/dummy/config/database.yml", "test/dummy/config/environment.rb", "test/dummy/config/environments", "test/dummy/config/environments/development.rb", "test/dummy/config/environments/production.rb", "test/dummy/config/environments/test.rb", "test/dummy/config/initializers", "test/dummy/config/initializers/assets.rb", "test/dummy/config/initializers/backtrace_silencers.rb", "test/dummy/config/initializers/cookies_serializer.rb", "test/dummy/config/initializers/filter_parameter_logging.rb", "test/dummy/config/initializers/inflections.rb", "test/dummy/config/initializers/mime_types.rb", "test/dummy/config/initializers/resultcrawler.rb", "test/dummy/config/initializers/session_store.rb", "test/dummy/config/initializers/wrap_parameters.rb", "test/dummy/config/locales", "test/dummy/config/locales/en.yml", "test/dummy/config/routes.rb", "test/dummy/config/secrets.yml", "test/dummy/db", "test/dummy/db/schema.rb", "test/dummy/lib", "test/dummy/lib/assets", "test/dummy/log", "test/dummy/public", "test/dummy/public/404.html", "test/dummy/public/422.html", "test/dummy/public/500.html", "test/dummy/public/favicon.ico", "test/fixtures", "test/fixtures/race_statistics.yml", "test/helpers", "test/helpers/race_statistics_helper_test.rb", "test/integration", "test/integration/navigation_test.rb", "test/models", "test/models/race_statistic_test.rb", "test/resultanalyzer_test.rb", "test/test_helper.rb"]
  s.homepage = "https://bitbucket.org/sports4u/resultanalyzer"
  s.licenses = [""]
  s.rubygems_version = "2.4.8"
  s.summary = "Analyzer for race results"
  s.test_files = ["test/resultanalyzer_test.rb", "test/integration", "test/integration/navigation_test.rb", "test/dummy", "test/dummy/log", "test/dummy/Rakefile", "test/dummy/README.rdoc", "test/dummy/bin", "test/dummy/bin/rake", "test/dummy/bin/bundle", "test/dummy/bin/rails", "test/dummy/lib", "test/dummy/lib/assets", "test/dummy/public", "test/dummy/public/404.html", "test/dummy/public/422.html", "test/dummy/public/favicon.ico", "test/dummy/public/500.html", "test/dummy/config.ru", "test/dummy/db", "test/dummy/db/schema.rb", "test/dummy/app", "test/dummy/app/mailers", "test/dummy/app/controllers", "test/dummy/app/controllers/application_controller.rb", "test/dummy/app/controllers/concerns", "test/dummy/app/assets", "test/dummy/app/assets/images", "test/dummy/app/assets/javascripts", "test/dummy/app/assets/javascripts/application.js", "test/dummy/app/assets/stylesheets", "test/dummy/app/assets/stylesheets/application.css", "test/dummy/app/helpers", "test/dummy/app/helpers/application_helper.rb", "test/dummy/app/views", "test/dummy/app/views/layouts", "test/dummy/app/views/layouts/application.html.erb", "test/dummy/app/models", "test/dummy/app/models/concerns", "test/dummy/config", "test/dummy/config/initializers", "test/dummy/config/initializers/backtrace_silencers.rb", "test/dummy/config/initializers/assets.rb", "test/dummy/config/initializers/cookies_serializer.rb", "test/dummy/config/initializers/resultcrawler.rb", "test/dummy/config/initializers/wrap_parameters.rb", "test/dummy/config/initializers/session_store.rb", "test/dummy/config/initializers/inflections.rb", "test/dummy/config/initializers/mime_types.rb", "test/dummy/config/initializers/filter_parameter_logging.rb", "test/dummy/config/environment.rb", "test/dummy/config/secrets.yml", "test/dummy/config/boot.rb", "test/dummy/config/database.yml", "test/dummy/config/environments", "test/dummy/config/environments/production.rb", "test/dummy/config/environments/development.rb", "test/dummy/config/environments/test.rb", "test/dummy/config/application.rb", "test/dummy/config/routes.rb", "test/dummy/config/locales", "test/dummy/config/locales/en.yml", "test/test_helper.rb", "test/controllers", "test/controllers/race_statistics_controller_test.rb", "test/helpers", "test/helpers/race_statistics_helper_test.rb", "test/fixtures", "test/fixtures/race_statistics.yml", "test/models", "test/models/race_statistic_test.rb"]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rails>, [">= 4.0.9"])
      s.add_runtime_dependency(%q<active_median>, [">= 0"])
      s.add_runtime_dependency(%q<chronic_duration>, [">= 0"])
      s.add_runtime_dependency(%q<chartkick>, [">= 0"])
      s.add_runtime_dependency(%q<histogram>, [">= 0"])
      s.add_development_dependency(%q<sqlite3>, [">= 0"])
    else
      s.add_dependency(%q<rails>, [">= 4.0.9"])
      s.add_dependency(%q<active_median>, [">= 0"])
      s.add_dependency(%q<chronic_duration>, [">= 0"])
      s.add_dependency(%q<chartkick>, [">= 0"])
      s.add_dependency(%q<histogram>, [">= 0"])
      s.add_dependency(%q<sqlite3>, [">= 0"])
    end
  else
    s.add_dependency(%q<rails>, [">= 4.0.9"])
    s.add_dependency(%q<active_median>, [">= 0"])
    s.add_dependency(%q<chronic_duration>, [">= 0"])
    s.add_dependency(%q<chartkick>, [">= 0"])
    s.add_dependency(%q<histogram>, [">= 0"])
    s.add_dependency(%q<sqlite3>, [">= 0"])
  end
end
