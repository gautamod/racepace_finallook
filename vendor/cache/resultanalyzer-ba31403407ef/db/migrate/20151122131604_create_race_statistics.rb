class CreateRaceStatistics < ActiveRecord::Migration
  def change
    create_table :race_statistics do |t|
      t.references :imported_race, index: true
      t.float :total_time_avg, limit: 12
      t.float :swim_time_avg, limit: 12
      t.float :bike_time_avg, limit: 12
      t.float :run_time_avg, limit: 12
      t.float :transition1_time_avg, limit: 12
      t.float :transition2_time_avg, limit: 12
      t.integer :num_results
      t.integer :num_dnf_results
      t.integer :num_finisher_results

      t.float :vdot_avg, limit: 12
      t.float :improved_vdot_avg, limit: 12
      t.float :tritotaltime_vdot_avg, limit: 12
      t.float :triswim_vdot_avg, limit: 12
      t.float :tribike_vdot_avg, limit: 12
      t.float :trirun_vdot_avg, limit: 12
      t.float :transition1_vdot_avg, limit: 12
      t.float :transition2_vdot_avg, limit: 12
      
      t.float :vdot_std, limit: 12
      t.float :improved_vdot_std, limit: 12
      t.float :tritotaltime_vdot_std, limit: 12
      t.float :triswim_vdot_std, limit: 12
      t.float :tribike_vdot_std, limit: 12
      t.float :trirun_vdot_std, limit: 12
      t.float :transition1_vdot_std, limit: 12
      t.float :transition2_vdot_std, limit: 12
      
      t.timestamps
    end
  end
end
