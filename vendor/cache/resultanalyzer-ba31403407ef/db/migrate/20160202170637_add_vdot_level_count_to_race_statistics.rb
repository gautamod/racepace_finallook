class AddVdotLevelCountToRaceStatistics < ActiveRecord::Migration
  def change
    add_column :race_statistics, :num_vdot_level1, :integer
    add_column :race_statistics, :num_vdot_level2, :integer
    add_column :race_statistics, :num_vdot_level3, :integer
    add_column :race_statistics, :num_vdot_level4, :integer
    add_column :race_statistics, :num_vdot_level5, :integer
    add_column :race_statistics, :num_vdot_level6, :integer
    add_column :race_statistics, :num_vdot_level7, :integer
    add_column :race_statistics, :num_vdot_level8, :integer
    add_column :race_statistics, :num_vdot_level9, :integer
    add_column :race_statistics, :num_vdot_level10, :integer
    add_column :race_statistics, :num_vdot_level11, :integer
    add_column :race_statistics, :num_vdot_level12, :integer
    add_column :race_statistics, :num_vdot_level13, :integer
    add_column :race_statistics, :num_vdot_level14, :integer
    add_column :race_statistics, :num_vdot_level15, :integer
  end
end
