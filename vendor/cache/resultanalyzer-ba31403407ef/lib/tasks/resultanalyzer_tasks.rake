namespace :resultanalyzer do
 desc "Run race statistics and fill race_statistic table"
 task create_statistics: :environment do
   puts "Creating num_results statistic"
   Resultanalyzer.insert_num_results_per_race_into_race_statistic
   puts "Creating num_dnf_results statistic"
   Resultanalyzer.insert_num_dnf_results_per_ld_race_into_race_statistic
   puts "Creating total_time_avg statistic"
   Resultanalyzer.insert_total_time_avg_by_race
   puts "Creating swim_time_avg statistic"
   Resultanalyzer.insert_swim_time_avg_by_race
   puts "Creating bike_time_avg statistic"
   Resultanalyzer.insert_bike_time_avg_by_race
   puts "Creating run_time_avg statistic"
   Resultanalyzer.insert_run_time_avg_by_race
   puts "Creating transition1_time_avg statistic"
   Resultanalyzer.insert_transition1_time_avg_by_race
   puts "Creating transition2_time_avg statistic"
   Resultanalyzer.insert_transition2_time_avg_by_race
   puts "Creating vdot statistics"
   Resultanalyzer.insert_vdots_into_race_statistic
 end

  desc "Create vdot statistics (will fill vdot level table)"
 task create_vdot_statistics: :environment do
   puts "Creating vdot statistics"
   Resultanalyzer.insert_vdots_into_race_statistic
 end
end