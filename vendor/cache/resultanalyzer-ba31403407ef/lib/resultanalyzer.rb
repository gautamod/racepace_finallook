require "resultanalyzer/engine"
require 'chronic_duration'
require 'active_median'
require 'chartkick'
require 'descriptive_statistics'
require 'histogram/array'

module Resultanalyzer

	Dir["tasks/**/*.rake"].each { |ext| load ext } if defined?(Rake)
	#load 'tasks/resultimporter.rake' if defined?(Rake)

	def self.sayhello
	   "Hello"
	end

	##############################################################
	# Fill IRONMAN number of results (independant on race length #
	##############################################################
	def self.insert_num_results_per_race_into_race_statistic
		sql="select imported_race_id, count(*) as num_results
			from imported_race_results 
			inner join imported_races on imported_race_id = imported_races.id 
			where (imported_races.organizer = 'World Triathlon Corporation')
		    group by imported_race_id"
 		records_array = ActiveRecord::Base.connection.select_all(sql)
		records_array.to_hash.each do |record_as_hash|
			rs=RaceStatistic.find_or_create_by(imported_race_id: record_as_hash["imported_race_id"])	
			rs.update(num_results: record_as_hash["num_results"])
		end
	end

	################################################################
	# Fill IRONMAN number of dnf results (long_distance (bike=180) #
	################################################################
	def self.insert_num_dnf_results_per_ld_race_into_race_statistic
		sql="select imported_race_id, count(*) as num_dnf_results
			from imported_race_results 
			inner join imported_races on imported_race_id = imported_races.id 
			where (imported_races.organizer = 'World Triathlon Corporation' 
				and imported_races.bike_distance=180 
				and run_time<(2.5*3600*100))
		    group by imported_race_id"

		records_array = ActiveRecord::Base.connection.select_all(sql)
		records_array.to_hash.each do |record_as_hash|
			rs=RaceStatistic.find_or_create_by(imported_race_id: record_as_hash["imported_race_id"])	
			rs.update(num_dnf_results: record_as_hash["num_dnf_results"])
		end
	end

	################################################################
	# Fill IRONMAN total_time_avg                                  #
	################################################################
	def self.insert_total_time_avg_by_race
	    #sql = "select avg(total_time) as avg from imported_race_results where imported_race_id=#{imported_race_id}"
	    #sql = "select imported_race_id, avg(total_time) as total_time_avg from imported_race_results inner join imported_races on imported_race_id = imported_races.id where (imported_races.organizer = \"World Triathlon Corporation\") group by imported_race_id"
	    sql = "select imported_race_id, avg(total_time) as total_time_avg from imported_race_results inner join imported_races on imported_race_id = imported_races.id group by imported_race_id"
	    records_array = ActiveRecord::Base.connection.select_all(sql)
	    records_array.to_hash.each do |record_as_hash|
			rs=RaceStatistic.find_or_create_by(imported_race_id: record_as_hash["imported_race_id"])	
			rs.update(total_time_avg: record_as_hash["total_time_avg"].to_i)
		end
	end

	################################################################
	# Fill IRONMAN swim_time_avg                                  #
	################################################################
	def self.insert_swim_time_avg_by_race
	    #sql = "select avg(total_time) as avg from imported_race_results where imported_race_id=#{imported_race_id}"
	    #sql = "select imported_race_id, avg(swim_time) as swim_time_avg from imported_race_results inner join imported_races on imported_race_id = imported_races.id where (imported_races.organizer = \"World Triathlon Corporation\") group by imported_race_id"
	    sql = "select imported_race_id, avg(swim_time) as swim_time_avg from imported_race_results inner join imported_races on imported_race_id = imported_races.id group by imported_race_id"
	    records_array = ActiveRecord::Base.connection.select_all(sql)
	    records_array.to_hash.each do |record_as_hash|
			rs=RaceStatistic.find_or_create_by(imported_race_id: record_as_hash["imported_race_id"])	
			rs.update(swim_time_avg: record_as_hash["swim_time_avg"].to_i)
		end
	end

	################################################################
	# Fill IRONMAN bike_time_avg                                  #
	################################################################
	def self.insert_bike_time_avg_by_race
	    #sql = "select avg(total_time) as avg from imported_race_results where imported_race_id=#{imported_race_id}"
	    #sql = "select imported_race_id, avg(bike_time) as bike_time_avg from imported_race_results inner join imported_races on imported_race_id = imported_races.id where (imported_races.organizer = \"World Triathlon Corporation\") group by imported_race_id"
	    sql = "select imported_race_id, avg(bike_time) as bike_time_avg from imported_race_results inner join imported_races on imported_race_id = imported_races.id group by imported_race_id"
	    records_array = ActiveRecord::Base.connection.select_all(sql)
	    records_array.to_hash.each do |record_as_hash|
			rs=RaceStatistic.find_or_create_by(imported_race_id: record_as_hash["imported_race_id"])	
			rs.update(bike_time_avg: record_as_hash["bike_time_avg"].to_i)
		end
	end

	################################################################
	# Fill IRONMAN run_time_avg                                  #
	################################################################
	def self.insert_run_time_avg_by_race
	    #sql = "select avg(total_time) as avg from imported_race_results where imported_race_id=#{imported_race_id}"
	    #sql = "select imported_race_id, avg(run_time) as run_time_avg from imported_race_results inner join imported_races on imported_race_id = imported_races.id where (imported_races.organizer = \"World Triathlon Corporation\") group by imported_race_id"
	    sql = "select imported_race_id, avg(run_time) as run_time_avg from imported_race_results inner join imported_races on imported_race_id = imported_races.id group by imported_race_id"
	    records_array = ActiveRecord::Base.connection.select_all(sql)
	    records_array.to_hash.each do |record_as_hash|
			rs=RaceStatistic.find_or_create_by(imported_race_id: record_as_hash["imported_race_id"])	
			rs.update(run_time_avg: record_as_hash["run_time_avg"].to_i)
		end
	end

	################################################################
	# Fill IRONMAN transition1_time_avg                                  #
	################################################################
	def self.insert_transition1_time_avg_by_race
	    #sql = "select avg(transition1_time) as avg from imported_race_results where imported_race_id=#{imported_race_id}"
	    #sql = "select imported_race_id, avg(transition1_time) as transition1_time_avg from imported_race_results inner join imported_races on imported_race_id = imported_races.id where (imported_races.organizer = \"World Triathlon Corporation\") group by imported_race_id"
	    sql = "select imported_race_id, avg(transition1_time) as transition1_time_avg from imported_race_results inner join imported_races on imported_race_id = imported_races.id group by imported_race_id"
	    records_array = ActiveRecord::Base.connection.select_all(sql)
	    records_array.to_hash.each do |record_as_hash|
			rs=RaceStatistic.find_or_create_by(imported_race_id: record_as_hash["imported_race_id"])	
			rs.update(transition1_time_avg: record_as_hash["transition1_time_avg"].to_i)
		end
	end

	################################################################
	# Fill IRONMAN transition2_time_avg                                  #
	################################################################
	def self.insert_transition2_time_avg_by_race
	    #sql = "select avg(transition1_time) as avg from imported_race_results where imported_race_id=#{imported_race_id}"
	    #sql = "select imported_race_id, avg(transition2_time) as transition2_time_avg from imported_race_results inner join imported_races on imported_race_id = imported_races.id where (imported_races.organizer = \"World Triathlon Corporation\") group by imported_race_id"
	    sql = "select imported_race_id, avg(transition2_time) as transition2_time_avg from imported_race_results inner join imported_races on imported_race_id = imported_races.id group by imported_race_id"
	    records_array = ActiveRecord::Base.connection.select_all(sql)
	    records_array.to_hash.each do |record_as_hash|
			rs=RaceStatistic.find_or_create_by(imported_race_id: record_as_hash["imported_race_id"])	
			rs.update(transition2_time_avg: record_as_hash["transition2_time_avg"].to_i)
		end
	end

	## fixme 
	##Order.group("date_trunc('week', created_at)").median(:total)

	def self.get_ironman_division_names
	 	#sql = "select * from imported_race_results limit 10"
	 	#sql = "select (floor(total_time/10) div 1800) * 1800 as NewX, count(*) from imported_race_results where imported_race_id=232 group by NewX limit 100"
		sql = "select division from imported_race_results inner join imported_races on imported_race_id = imported_races.id where (imported_races.organizer = 'World Triathlon Corporation') group by division"
		records_array = ActiveRecord::Base.connection.select_all(sql)
		puts records_array
	 end

	 def self.total_time_hist_by_race(imported_race_id)
	    #sql = "select (floor(total_time/10) div 1800) * 1800 as NewX, count(*) from imported_race_results where imported_race_id=#{imported_race_id} group by NewX"
	    #records_array = ActiveRecord::Base.connection.select_all(sql)
		#records_array
		max=17*3600*100 #17hours
		min=0
		inc=5*60*100 #5min
		bands=bands=(max-min)/inc
		(bins, all_rroverall_histvals) = Histogramsql.hist("imported_race_results", "total_time", {max: max, min: min, bands: bands,conditions: "imported_race_id = #{imported_race_id}"})
		[bins, all_rroverall_histvals]
	 end

	 def self.run_time_hist_by_race(imported_race_id)
		max=7*3600*100 #17hours
		min=0
		inc=5*60*100 #5min
		bands=bands=(max-min)/inc
		(bins, all_rroverall_histvals) = Histogramsql.hist("imported_race_results", "run_time", {max: max, min: min, bands: bands,conditions: "imported_race_id = #{imported_race_id}"})
		[bins, all_rroverall_histvals]
	 end

    ################################################################
	# Fill vdot statistics                                         #
	################################################################
	def self.insert_vdots_into_race_statistic
   
	   puts "Creating IRONMAN vdot statistics"
	   ironman_races= ImportedRace.where("organizer = ?",'World Triathlon Corporation')
	   num=ironman_races.length
	   idx=0
	   ironman_races.each do |rc|
	      idx+=1
	      rs=RaceStatistic.find_or_create_by(imported_race_id: rc.id)
	      tt_vdot_std=rc.imported_race_result.where("tritotaltime_vdot > 10").pluck(:tritotaltime_vdot).standard_deviation.to_f.round(2)
	      rs.update(tritotaltime_vdot_std: tt_vdot_std)
	      swim_vdot_std=rc.imported_race_result.where("triswim_vdot > 10").pluck(:triswim_vdot).standard_deviation.to_f.round(2)
	      rs.update(triswim_vdot_std: swim_vdot_std)
	      bike_vdot_std=rc.imported_race_result.where("tribike_vdot > 10").pluck(:tribike_vdot).standard_deviation.to_f.round(2)
	      rs.update(tribike_vdot_std: bike_vdot_std)
	      run_vdot_std=rc.imported_race_result.where("trirun_vdot > 10").pluck(:trirun_vdot).standard_deviation.to_f.round(2)
	      rs.update(trirun_vdot_std: run_vdot_std)
	      t1_vdot_std=rc.imported_race_result.where("transition1_vdot > 10").pluck(:transition1_vdot).standard_deviation.to_f.round(2)
	      rs.update(transition1_vdot_std: t1_vdot_std)
	      t2_vdot_std=rc.imported_race_result.where("transition2_vdot > 10").pluck(:transition2_vdot).standard_deviation.to_f.round(2)
	      rs.update(transition2_vdot_std: t2_vdot_std)
	      
	      tt_vdot_avg=rc.imported_race_result.where("tritotaltime_vdot > 10").pluck(:tritotaltime_vdot).mean.to_f.round(2)
	      rs.update(tritotaltime_vdot_avg: tt_vdot_avg)
	      swim_vdot_avg=rc.imported_race_result.where("triswim_vdot > 10").pluck(:triswim_vdot).mean.to_f.round(2)
	      rs.update(triswim_vdot_avg: swim_vdot_avg)
	      bike_vdot_avg=rc.imported_race_result.where("tribike_vdot > 10").pluck(:tribike_vdot).mean.to_f.round(2)
	      rs.update(tribike_vdot_avg: bike_vdot_avg)
	      run_vdot_avg=rc.imported_race_result.where("trirun_vdot > 10").pluck(:trirun_vdot).mean.to_f.round(2)
	      rs.update(trirun_vdot_avg: run_vdot_avg)
	      t1_vdot_avg=rc.imported_race_result.where("transition1_vdot > 10").pluck(:transition1_vdot).mean.to_f.round(2)
	      rs.update(transition1_vdot_avg: t1_vdot_avg)
	      t2_vdot_avg=rc.imported_race_result.where("transition2_vdot > 10").pluck(:transition2_vdot).mean.to_f.round(2)
	      rs.update(transition2_vdot_avg: t2_vdot_avg)
	      
	      #Build statistic result strings
	      tt_stat="#{tt_vdot_avg.round(1).to_s.rjust(4,' ')}(#{tt_vdot_std.round(0).to_s.rjust(2,' ')})"
	      swim_stat="#{swim_vdot_avg.round(1).to_s.rjust(4,' ')}(#{swim_vdot_std.round(0).to_s.rjust(2,' ')})"
	      bike_stat="#{bike_vdot_avg.round(1).to_s.rjust(4,' ')}(#{bike_vdot_std.round(0).to_s.rjust(2,' ')})"
	      run_stat="#{run_vdot_avg.round(1).to_s.rjust(4,' ')}(#{run_vdot_std.round(0).to_s.rjust(2,' ')})"
	      t1_stat="#{t1_vdot_avg.round(1).to_s.rjust(4,' ')}(#{t1_vdot_std.round(0).to_s.rjust(2,' ')})"
	      t2_stat="#{t2_vdot_avg.round(1).to_s.rjust(4,' ')}(#{t2_vdot_std.round(0).to_s.rjust(2,' ')})"
	      puts "(#{idx}/#{num}) MEAN(STD) | tt: #{tt_stat} s: #{swim_stat} b: #{bike_stat} r: #{run_stat} t1: #{t1_stat}  t2: #{t2_stat} #{rc.name}"
          

	      #Fixme: could be solved nicer: tritotaltime_vdot's are counted into num_vdot_levels
	      #       instead of separate num_tritotlatime_vdot_levels to save the number of columns
		  
		  #Warning: histogram function is eroneous if using following parameters (max: 87.5, min: 12.5, bands: 15)
	      #(bins,counts)=Histogramsql.hist("imported_race_results", "tritotaltime_vdot", {max: 87.5, min: 12.5, bands: 15,conditions: "imported_race_id = #{rc.id}"})
          vdots=ImportedRaceResult.where("imported_race_id = #{rc.id}").where("tritotaltime_vdot > 10").pluck(:tritotaltime_vdot).map(&:to_f)
          (bins, counts) = vdots.histogram([0,20,25,30,35,40,45,50,55,60,65,70,75,80,85], :bin_boundary => :min)
          puts "Vdot levels histogram: " + counts.inspect  
	      rs.update(num_vdot_level1: counts[0])
	      rs.update(num_vdot_level2: counts[1])
	      rs.update(num_vdot_level3: counts[2])
	      rs.update(num_vdot_level4: counts[3])
	      rs.update(num_vdot_level5: counts[4])
	      rs.update(num_vdot_level6: counts[5])
	      rs.update(num_vdot_level7: counts[6])
	      rs.update(num_vdot_level8: counts[7])
	      rs.update(num_vdot_level9: counts[8])
	      rs.update(num_vdot_level10: counts[9])
	      rs.update(num_vdot_level11: counts[10])
	      rs.update(num_vdot_level12: counts[11])
	      rs.update(num_vdot_level13: counts[12])
	      rs.update(num_vdot_level14: counts[13])
	      rs.update(num_vdot_level15: counts[14])

	   end
	  
	   puts "Creating run vdot statistics"
	      #find all run races which have a total_distance>0
	      races= ImportedRace.where("sport_type_id = ? and total_distance > 0",SportType.where("description=?","Run").first.id)
	      num=races.length
	      idx=0
	      races.each do |rc|
	         idx+=1
	         rs=RaceStatistic.find_or_create_by(imported_race_id: rc.id)

	         #vdot standard deviation
	         vdot_std=rc.imported_race_result.where("vdot > 10").pluck(:vdot).standard_deviation.to_f.round(2)
	         rs.update(vdot_std: vdot_std)

	         #vdot average
	         vdot_avg=rc.imported_race_result.where("vdot > 10").pluck(:vdot).mean.to_f.round(2)
	         rs.update(vdot_avg: vdot_avg)
	         vdot_stat="#{vdot_avg.round(1).to_s.rjust(4,' ')}(#{vdot_std.round(0).to_s.rjust(2,' ')})"
	         puts "(#{idx}/#{num}) MEAN(STD) | #{vdot_stat}  #{rc.name}"

	        #Warning: histogram function is eroneous if using following parameters (max: 87.5, min: 12.5, bands: 15)
	        #(bins,counts)=Histogramsql.hist("imported_race_results", "vdot", {max: 87.5, min: 12.5, bands: 15,conditions: "imported_race_id = #{rc.id}"})
            vdots=ImportedRaceResult.where("imported_race_id = #{rc.id}").pluck(:vdot).map(&:to_f)
            (bins, counts) = vdots.histogram([0,20,25,30,35,40,45,50,55,60,65,70,75,80,85], :bin_boundary => :min)
            puts "Vdot levels histogram: " + counts.inspect
            rs.update(num_vdot_level1: counts[0])
	        rs.update(num_vdot_level2: counts[1])
	        rs.update(num_vdot_level3: counts[2])
	        rs.update(num_vdot_level4: counts[3])
	        rs.update(num_vdot_level5: counts[4])
	        rs.update(num_vdot_level6: counts[5])
	        rs.update(num_vdot_level7: counts[6])
	        rs.update(num_vdot_level8: counts[7])
	        rs.update(num_vdot_level9: counts[8])
	        rs.update(num_vdot_level10: counts[9])
	        rs.update(num_vdot_level11: counts[10])
	        rs.update(num_vdot_level12: counts[11])
	        rs.update(num_vdot_level13: counts[12])
	        rs.update(num_vdot_level14: counts[13])
	        rs.update(num_vdot_level15: counts[14])

	      end
	end

    #############################################################
    # Create one xy array from xval and yval array              #
    # e.g. xval=[1,2,3,4]                                       #
    #      yval=[2,4,6,3]                                       #
    # return  [[1,2],[2,4],[3,6],[4,3]]                         #
    #############################################################
    def self.map_xy_array (xvals, yvals)
        xvals.map.with_index { |xval,index| [xval,yvals[index]] }
    end


end