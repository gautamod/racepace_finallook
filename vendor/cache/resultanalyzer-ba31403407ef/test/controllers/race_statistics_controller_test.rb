require 'test_helper'

class RaceStatisticsControllerTest < ActionController::TestCase
  setup do
    @race_statistic = race_statistics(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:race_statistics)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create race_statistic" do
    assert_difference('RaceStatistic.count') do
      post :create, race_statistic: { bike_time_avg: @race_statistic.bike_time_avg, imported_race_id: @race_statistic.imported_race_id, num_dnf_results: @race_statistic.num_dnf_results, num_finisher_results: @race_statistic.num_finisher_results, num_results: @race_statistic.num_results, run_time_avg: @race_statistic.run_time_avg, swim_time_avg: @race_statistic.swim_time_avg, total_time_avg: @race_statistic.total_time_avg, transition1_time_avg: @race_statistic.transition1_time_avg, transition2_time_avg: @race_statistic.transition2_time_avg }
    end

    assert_redirected_to race_statistic_path(assigns(:race_statistic))
  end

  test "should show race_statistic" do
    get :show, id: @race_statistic
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @race_statistic
    assert_response :success
  end

  test "should update race_statistic" do
    patch :update, id: @race_statistic, race_statistic: { bike_time_avg: @race_statistic.bike_time_avg, imported_race_id: @race_statistic.imported_race_id, num_dnf_results: @race_statistic.num_dnf_results, num_finisher_results: @race_statistic.num_finisher_results, num_results: @race_statistic.num_results, run_time_avg: @race_statistic.run_time_avg, swim_time_avg: @race_statistic.swim_time_avg, total_time_avg: @race_statistic.total_time_avg, transition1_time_avg: @race_statistic.transition1_time_avg, transition2_time_avg: @race_statistic.transition2_time_avg }
    assert_redirected_to race_statistic_path(assigns(:race_statistic))
  end

  test "should destroy race_statistic" do
    assert_difference('RaceStatistic.count', -1) do
      delete :destroy, id: @race_statistic
    end

    assert_redirected_to race_statistics_path
  end
end
