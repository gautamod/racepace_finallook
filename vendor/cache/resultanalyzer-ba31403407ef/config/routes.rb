Rails.application.routes.draw do
  get '/race_statistics/time_stats' => 'race_statistics#time_stats'
  get '/race_statistics/vdot_stats' => 'race_statistics#vdot_stats'
  get '/race_statistics/ld_vdot_stats' => 'race_statistics#ld_vdot_stats'
  get '/race_statistics/md_vdot_stats' => 'race_statistics#md_vdot_stats'
  resources :race_statistics
end
