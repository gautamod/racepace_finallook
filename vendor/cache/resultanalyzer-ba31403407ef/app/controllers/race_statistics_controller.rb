class RaceStatisticsController < ApplicationController

  skip_before_action :authenticate_user!
  
  skip_after_action :verify_authorized
  skip_after_action :verify_policy_scoped

  before_action :set_race_statistic, only: [:show, :edit, :update, :destroy]

  # GET time statistics
  def time_stats
    ########################################################################################################
    # Marathon
    run42km_totaltime=ImportedRaceResult.run42km_only.where("total_time > 90000").pluck(:total_time)
    bin_width=5*6000 # 5minutes
    num_ticks = (run42km_totaltime.max/bin_width).round + 1 #eg 34 half-an-hour ticks for 17hrs
    @run42km_totaltime_hist = run42km_totaltime.histogram(num_ticks, :min => -bin_width.to_f/2, :max => num_ticks*bin_width-(bin_width.to_f/2)).transpose

    ########################################################################################################
    # Half-Marathon
    run21km_totaltime=ImportedRaceResult.run21km_only.where("total_time > 0").pluck(:total_time)
    num_ticks = (run21km_totaltime.max/bin_width).round + 1 #eg 34 half-an-hour ticks for 17hrs
    @run21km_totaltime_hist = run21km_totaltime.histogram(num_ticks, :min => -bin_width.to_f/2, :max => num_ticks*bin_width-(bin_width.to_f/2)).transpose

    ########################################################################################################
    # Run 10km
    run10km_totaltime=ImportedRaceResult.run10km_only.where("total_time > 0").pluck(:total_time)
    num_ticks = (run10km_totaltime.max/bin_width).round + 1 #eg 34 half-an-hour ticks for 17hrs
    @run10km_totaltime_hist = run10km_totaltime.histogram(num_ticks, :min => -bin_width.to_f/2, :max => num_ticks*bin_width-(bin_width.to_f/2)).transpose

    ########################################################################################################
    # Long-Distance
    (ld_totaltime, ld_swimtime, ld_biketime, ld_runtime, ld_t1time, ld_t2time) = ImportedRaceResult.ld_only.where("total_time > 0 and swim_time > 0 and bike_time > 0 and run_time > 0").pluck(:total_time, :swim_time, :bike_time, :run_time, :transition1_time, :transition2_time).transpose # in minutes
    num_ticks = (ld_totaltime.max/bin_width).round + 1 #eg 34 half-an-hour ticks for 17hrs
    @ld_totaltime_hist = ld_totaltime.histogram(num_ticks, :min => -bin_width.to_f/2, :max => num_ticks*bin_width-(bin_width.to_f/2)).transpose
    #ld_swimtime=ImportedRaceResult.ld_only.where("total_time > 0").pluck(:swim_time).collect{ |n| n.to_f / 6000} # in minutes
    num_ticks = (ld_swimtime.max/bin_width).round + 1 #eg 34 half-an-hour ticks for 17hrs
    @ld_swimtime_hist = ld_swimtime.histogram(num_ticks, :min => -bin_width.to_f/2, :max => num_ticks*bin_width-(bin_width.to_f/2)).transpose
    #ld_biketime=ImportedRaceResult.ld_only.where("total_time > 0").pluck(:bike_time).collect{ |n| n.to_f / 6000} # in minutes
    num_ticks = (ld_biketime.max/bin_width).round + 1 #eg 34 half-an-hour ticks for 17hrs
    @ld_biketime_hist = ld_biketime.histogram(num_ticks, :min => -bin_width.to_f/2, :max => num_ticks*bin_width-(bin_width.to_f/2)).transpose
    #ld_runtime=ImportedRaceResult.ld_only.where("total_time > 0").pluck(:run_time).collect{ |n| n.to_f / 6000} # in minutes
    num_ticks = (ld_runtime.max/bin_width).round + 1 #eg 34 half-an-hour ticks for 17hrs
    @ld_runtime_hist = ld_runtime.histogram(num_ticks, :min => -bin_width.to_f/2, :max => num_ticks*bin_width-(bin_width.to_f/2)).transpose
    #ld_t1time=ImportedRaceResult.ld_only.where("total_time > 0").pluck(:transition1_time).collect{ |n| n.to_f / 6000}.select{|x| x<30} # in minutes, and only less than 30mins
    num_ticks = (ld_t1time.max).round + 1 #eg 34 minute ticks for 34minutes
    @ld_t1time_hist = ld_t1time.histogram(num_ticks, :min => -bin_width.to_f/2, :max => num_ticks*bin_width-(bin_width.to_f/2)).transpose
    #ld_t2time=ImportedRaceResult.ld_only.where("total_time > 0").pluck(:transition2_time).collect{ |n| n.to_f / 6000}.select{|x| x<30} # in minutes, and only less than 30mins
    num_ticks = (ld_t2time.max).round + 1 #eg 34 minute ticks for 34minutes
    @ld_t2time_hist = ld_t2time.histogram(num_ticks, :min => -bin_width.to_f/2, :max => num_ticks*bin_width-(bin_width.to_f/2)).transpose
    
    ########################################################################################################
    # Half-Distance
    (md_totaltime, md_swimtime, md_biketime, md_runtime, md_t1time, md_t2time) = ImportedRaceResult.md_only.where("total_time > 0 and swim_time > 0 and bike_time > 0 and run_time > 0").pluck(:total_time, :swim_time, :bike_time, :run_time, :transition1_time, :transition2_time).transpose # in minutes
    #md_totaltime=ImportedRaceResult.md_only.where("total_time > 0").pluck(:total_time).collect{ |n| n.to_f / 6000} # in minutes
    num_ticks = (md_totaltime.max/bin_width).round + 1 #eg 34 half-an-hour ticks for 17hrs
    @md_totaltime_hist = md_totaltime.histogram(num_ticks, :min => -bin_width.to_f/2, :max => num_ticks*bin_width-(bin_width.to_f/2)).transpose
    #md_swimtime=ImportedRaceResult.md_only.where("total_time > 0").pluck(:swim_time).collect{ |n| n.to_f / 6000} # in minutes
    num_ticks = (md_swimtime.max/bin_width).round + 1 #eg 34 half-an-hour ticks for 17hrs
    @md_swimtime_hist = md_swimtime.histogram(num_ticks, :min => -bin_width.to_f/2, :max => num_ticks*bin_width-(bin_width.to_f/2)).transpose
    #md_biketime=ImportedRaceResult.md_only.pluck(:bike_time).collect{ |n| n.to_f / 6000} # in minutes
    num_ticks = (md_biketime.max/bin_width).round + 1 #eg 34 half-an-hour ticks for 17hrs
    @md_biketime_hist = md_biketime.histogram(num_ticks, :min => -bin_width.to_f/2, :max => num_ticks*bin_width-(bin_width.to_f/2)).transpose
    #md_runtime=ImportedRaceResult.md_only.pluck(:run_time).collect{ |n| n.to_f / 6000} # in minutes
    num_ticks = (md_runtime.max/bin_width).round + 1 #eg 34 half-an-hour ticks for 17hrs
    @md_runtime_hist = md_runtime.histogram(num_ticks, :min => -bin_width.to_f/2, :max => num_ticks*bin_width-(bin_width.to_f/2)).transpose
    #md_t1time=ImportedRaceResult.md_only.pluck(:transition1_time).collect{ |n| n.to_f / 6000}.select{|x| x<30} # in minutes, and only less than 30mins
    num_ticks = (md_t1time.max).round + 1 #eg 34 minute ticks for 34minutes
    @md_t1time_hist = md_t1time.histogram(num_ticks, :min => -bin_width.to_f/2, :max => num_ticks*bin_width-(bin_width.to_f/2)).transpose
    #md_t2time=ImportedRaceResult.md_only.pluck(:transition2_time).collect{ |n| n.to_f / 6000}.select{|x| x<30} # in minutes, and only less than 30mins
    num_ticks = (md_t2time.max).round + 1 #eg 34 minute ticks for 34minutes
    @md_t2time_hist = md_t2time.histogram(num_ticks, :min => -bin_width.to_f/2, :max => num_ticks*bin_width-(bin_width.to_f/2)).transpose

    run10km_time_percentiles=[]
    run10km_vdot_percentiles=[]
    run21km_time_percentiles=[]
    run21km_vdot_percentiles=[]
    run42km_time_percentiles=[]
    run42km_vdot_percentiles=[]
    
    ld_totaltime_time_percentiles=[]
    ld_totaltime_vdot_percentiles=[]
    ld_swimtime_time_percentiles=[]
    ld_swimtime_vdot_percentiles=[]
    ld_biketime_time_percentiles=[]
    ld_biketime_vdot_percentiles=[]
    ld_runtime_time_percentiles=[]
    ld_runtime_vdot_percentiles=[]
    ld_t1_time_percentiles=[]
    ld_t1_vdot_percentiles=[]
    ld_t2_time_percentiles=[]
    ld_t2_vdot_percentiles=[]
    
    md_totaltime_time_percentiles=[]
    md_totaltime_vdot_percentiles=[]
    md_swimtime_time_percentiles=[]
    md_swimtime_vdot_percentiles=[]
    md_biketime_time_percentiles=[]
    md_biketime_vdot_percentiles=[]
    md_runtime_time_percentiles=[]
    md_runtime_vdot_percentiles=[]
    md_t1_time_percentiles=[]
    md_t1_vdot_percentiles=[]
    md_t2_time_percentiles=[]
    md_t2_vdot_percentiles=[]
    
    @percentiles= [0.05,0.1,1,3,10,20,30,40,50,60,70,80,90,99]
    @percentiles.each do |percentile|
       run10km_time_percentile = run10km_totaltime.percentile(percentile).to_f
       run10km_time_percentiles << run10km_time_percentile 
       run10km_vdot_percentile = Resultimporter.calc_vdot(10000, run10km_time_percentile/100)
       run10km_vdot_percentiles << run10km_vdot_percentile 
       
       run21km_time_percentile = run21km_totaltime.percentile(percentile).to_f
       run21km_time_percentiles << run21km_time_percentile 
       run21km_vdot_percentile = Resultimporter.calc_vdot(21100, run21km_time_percentile/100)
       run21km_vdot_percentiles << run21km_vdot_percentile 
       
       run42km_time_percentile = run42km_totaltime.percentile(percentile).to_f
       run42km_time_percentiles << run42km_time_percentile 
       run42km_vdot_percentile = Resultimporter.calc_vdot(42195, run42km_time_percentile/100)
       run42km_vdot_percentiles << run42km_vdot_percentile 
    
       ld_totaltime_time_percentile = ld_totaltime.percentile(percentile).to_f
       ld_totaltime_time_percentiles << ld_totaltime_time_percentile
       ld_totaltime_vdot_percentile = Resultimporter.calc_tridot(3800, 180000, 42195, ld_totaltime_time_percentile/100)
       ld_totaltime_vdot_percentiles << ld_totaltime_vdot_percentile 
       ld_swimtime_time_percentile = ld_swimtime.percentile(percentile).to_f
       ld_swimtime_time_percentiles << ld_swimtime_time_percentile
       ld_swimtime_vdot_percentile = Resultimporter.calc_sdot(3800, ld_swimtime_time_percentile/100, 10*3600)
       ld_swimtime_vdot_percentiles << ld_swimtime_vdot_percentile 
       ld_biketime_time_percentile = ld_biketime.percentile(percentile).to_f
       ld_biketime_time_percentiles << ld_biketime_time_percentile
       ld_biketime_vdot_percentile = Resultimporter.calc_bdot(180000, ld_biketime_time_percentile/100, 10*3600)
       ld_biketime_vdot_percentiles << ld_biketime_vdot_percentile 
       ld_runtime_time_percentile = ld_runtime.percentile(percentile).to_f
       ld_runtime_time_percentiles << ld_runtime_time_percentile
       ld_runtime_vdot_percentile = Resultimporter.calc_rdot(42195, ld_runtime_time_percentile/100, 10*3600)
       ld_runtime_vdot_percentiles << ld_biketime_vdot_percentile 
       ld_t1_time_percentile = ld_t1time.percentile(percentile).to_f
       ld_t1_time_percentiles << ld_t1_time_percentile
       ld_t1_vdot_percentile = Resultimporter.calc_t1dot(1000, ld_t1_time_percentile/100, 10*3600)
       ld_t1_vdot_percentiles << ld_t1_vdot_percentile
       ld_t2_time_percentile = ld_t2time.percentile(percentile).to_f
       ld_t2_time_percentiles << ld_t2_time_percentile
       ld_t2_vdot_percentile = Resultimporter.calc_t2dot(1000, ld_t2_time_percentile/100, 10*3600)
       ld_t2_vdot_percentiles << ld_t2_vdot_percentile

       md_totaltime_time_percentile = md_totaltime.percentile(percentile).to_f
       md_totaltime_time_percentiles << md_totaltime_time_percentile
       md_totaltime_vdot_percentile = Resultimporter.calc_tridot(1900, 90000, 21100, md_totaltime_time_percentile/100)
       md_totaltime_vdot_percentiles << md_totaltime_vdot_percentile 
       md_swimtime_time_percentile = md_swimtime.percentile(percentile).to_f
       md_swimtime_time_percentiles << md_swimtime_time_percentile
       md_swimtime_vdot_percentile = Resultimporter.calc_sdot(1900, md_swimtime_time_percentile/100, 5*3600)
       md_swimtime_vdot_percentiles << md_swimtime_vdot_percentile 
       md_biketime_time_percentile = md_biketime.percentile(percentile).to_f
       md_biketime_time_percentiles << md_biketime_time_percentile
       md_biketime_vdot_percentile = Resultimporter.calc_bdot(90000, md_biketime_time_percentile/100, 5*3600)
       md_biketime_vdot_percentiles << md_biketime_vdot_percentile 
       md_runtime_time_percentile = md_runtime.percentile(percentile).to_f
       md_runtime_time_percentiles << md_runtime_time_percentile
       md_runtime_vdot_percentile = Resultimporter.calc_rdot(21100, md_runtime_time_percentile/100, 5*3600)
       md_runtime_vdot_percentiles << md_biketime_vdot_percentile 
       md_t1_time_percentile = md_t1time.percentile(percentile).to_f
       md_t1_time_percentiles << md_t1_time_percentile
       md_t1_vdot_percentile = Resultimporter.calc_t1dot(1000, md_t1_time_percentile/100, 5*3600)
       md_t1_vdot_percentiles << md_t1_vdot_percentile
       md_t2_time_percentile = md_t2time.percentile(percentile).to_f
       md_t2_time_percentiles << md_t2_time_percentile
       md_t2_vdot_percentile = Resultimporter.calc_t2dot(1000, md_t2_time_percentile/100, 5*3600)
       md_t2_vdot_percentiles << md_t2_vdot_percentile
      
    end

    @table_headers=["10km","21km","42km","LDtt","LDsw","LDbk","LDrn","LDt1","LDt2","MDtt","MDsw","MDbk","MDrn","MDt1","MDt2"]
    @time_per_percentiles_per_racetypes=[run10km_time_percentiles,run21km_time_percentiles,run42km_time_percentiles,ld_totaltime_time_percentiles,ld_swimtime_time_percentiles,ld_biketime_time_percentiles,ld_runtime_time_percentiles,ld_t1_time_percentiles,ld_t2_time_percentiles,md_totaltime_time_percentiles,md_swimtime_time_percentiles,md_biketime_time_percentiles,md_runtime_time_percentiles,md_t1_time_percentiles,md_t2_time_percentiles]
    @vdot_per_percentiles_per_racetypes=[run10km_vdot_percentiles,run21km_vdot_percentiles,run42km_vdot_percentiles,ld_totaltime_vdot_percentiles,ld_swimtime_vdot_percentiles,ld_biketime_vdot_percentiles,ld_runtime_vdot_percentiles,ld_t1_vdot_percentiles,ld_t2_vdot_percentiles,md_totaltime_vdot_percentiles,md_swimtime_vdot_percentiles,md_biketime_vdot_percentiles,md_runtime_vdot_percentiles,md_t1_vdot_percentiles,md_t2_vdot_percentiles]
   
    (md_totaltime, md_swimtime, md_biketime, md_runtime, md_t1time, md_t2time) = [nil,nil,nil,nil,nil,nil] #free up mem
    (ld_totaltime, ld_swimtime, ld_biketime, ld_runtime, ld_t1time, ld_t2time) = [nil,nil,nil,nil,nil,nil] #free up mem
    (run42km_totaltime, run21km_totaltime, run10km_totaltime) = [nil,nil,nil] #free up mem
   end  
    
   #  @times_per_percentiles_per_racetypes=[]
   #  all_times_per_racetypes.each do |all_times_per_racetype|
   #    times_per_percentiles_per_racetype=[]
   #    @percentiles.each do |percentile|
   #      times_per_percentile_per_racetype = all_times_per_racetype.percentile(percentile).to_f
   #      times_per_percentiles_per_racetype << times_per_percentile_per_racetype
   #    end
   #    @times_per_percentiles_per_racetypes << times_per_percentiles_per_racetype
   #  end
   # end

 # GET VDOT statistics
  def vdot_stats
    #Get vdot statistics for Marathon run races
    bins=[0,20,25,30,35,40,45,50,55,60,65,70,75,80,85]
    run_42k_totaltime_vdots=RaceStatistic.run42km_only.pluck(:vdot_avg).map(&:to_f)
    @run_42k_totaltime_vdot_hist_byrace = run_42k_totaltime_vdots.histogram(bins, :bin_boundary => :min).transpose
    @run_42k_totaltime_vdot_mean = run_42k_totaltime_vdots.mean

    #Get vdot statistics for Half-Marathon run races
    run_21k_totaltime_vdots=RaceStatistic.run21km_only.pluck(:vdot_avg).map(&:to_f)
    @run_21k_totaltime_vdot_hist_byrace = run_21k_totaltime_vdots.histogram(bins, :bin_boundary => :min).transpose
    @run_21k_totaltime_vdot_mean = run_21k_totaltime_vdots.mean

    #Get vdot statistics for 10km run races
    run_10k_totaltime_vdots=RaceStatistic.run10km_only.pluck(:vdot_avg).map(&:to_f)
    @run_10k_totaltime_vdot_hist_byrace = run_10k_totaltime_vdots.histogram(bins, :bin_boundary => :min).transpose
    @run_10k_totaltime_vdot_mean = run_10k_totaltime_vdots.mean
   

    #Get vdot statistics for Longdistance races
    ld_tritotaltime_vdots=RaceStatistic.ld_only.pluck(:tritotaltime_vdot_avg).map(&:to_f)
    @ld_tritotaltime_vdot_hist_byrace = ld_tritotaltime_vdots.histogram(bins, :bin_boundary => :min).transpose   
    @ld_tritotaltime_vdot_mean = ld_tritotaltime_vdots.mean

    ld_triswim_vdots=RaceStatistic.ld_only.pluck(:triswim_vdot_avg).map(&:to_f)
    @ld_triswim_vdot_hist_byrace = ld_triswim_vdots.histogram(bins, :bin_boundary => :min).transpose
    @ld_triswim_vdot_mean = ld_triswim_vdots.mean
    
    ld_tribike_vdots=RaceStatistic.ld_only.pluck(:tribike_vdot_avg).map(&:to_f)
    @ld_tribike_vdot_hist_byrace = ld_tribike_vdots.histogram(bins, :bin_boundary => :min).transpose
    @ld_tribike_vdot_mean = ld_tribike_vdots.mean
    
    ld_trirun_vdots=RaceStatistic.ld_only.pluck(:trirun_vdot_avg).map(&:to_f)
    @ld_trirun_vdot_hist_byrace = ld_trirun_vdots.histogram(bins, :bin_boundary => :min).transpose
    @ld_trirun_vdot_mean = ld_trirun_vdots.mean
    
    ld_t1_vdots=RaceStatistic.ld_only.pluck(:transition1_vdot_avg).map(&:to_f)
    @ld_t1_vdot_hist_byrace = ld_t1_vdots.histogram(bins, :bin_boundary => :min).transpose
    @ld_t1_vdot_mean = ld_t1_vdots.mean
    
    ld_t2_vdots=RaceStatistic.ld_only.pluck(:transition2_vdot_avg).map(&:to_f)
    @ld_t2_vdot_hist_byrace = ld_t2_vdots.histogram(bins, :bin_boundary => :min).transpose
    @ld_t2_vdot_mean = ld_t2_vdots.mean
     
    #Get vdot statistics for Halfdistance races
    md_tritotaltime_vdots=RaceStatistic.md_only.pluck(:tritotaltime_vdot_avg).map(&:to_f)
    @md_tritotaltime_vdot_hist_byrace = md_tritotaltime_vdots.histogram(bins, :bin_boundary => :min).transpose
    @md_tritotaltime_vdot_mean = md_tritotaltime_vdots.mean

    md_triswim_vdots=RaceStatistic.md_only.pluck(:triswim_vdot_avg).map(&:to_f)
    @md_triswim_vdot_hist_byrace = md_triswim_vdots.histogram(bins, :bin_boundary => :min).transpose
    @md_triswim_vdot_mean = md_triswim_vdots.mean
    
    md_tribike_vdots=RaceStatistic.md_only.pluck(:tribike_vdot_avg).map(&:to_f)
    @md_tribike_vdot_hist_byrace = md_tribike_vdots.histogram(bins, :bin_boundary => :min).transpose
    @md_tribike_vdot_mean = md_tribike_vdots.mean
    
    md_trirun_vdots=RaceStatistic.md_only.pluck(:trirun_vdot_avg).map(&:to_f)
    @md_trirun_vdot_hist_byrace = md_trirun_vdots.histogram(bins, :bin_boundary => :min).transpose
    @md_trirun_vdot_mean = md_trirun_vdots.mean
    
    md_t1_vdots=RaceStatistic.md_only.pluck(:transition1_vdot_avg).map(&:to_f)
    @md_t1_vdot_hist_byrace = md_t1_vdots.histogram(bins, :bin_boundary => :min).transpose
    @md_t1_vdot_mean = md_t1_vdots.mean
    
    md_t2_vdots=RaceStatistic.md_only.pluck(:transition2_vdot_avg).map(&:to_f)
    @md_t2_vdot_hist_byrace = md_t2_vdots.histogram(bins, :bin_boundary => :min).transpose
    @md_t2_vdot_mean = md_t2_vdots.mean


    ########################################################################################################
    # Run 42km
    run42km_vdots=ImportedRaceResult.run42km_only.pluck(:vdot).map(&:to_f)
    @run42km_vdot_hist = run42km_vdots.histogram(bins, :bin_boundary => :min).transpose
    
    run42km_vdots_recalc=ImportedRaceResult.run42km_only.pluck(:total_time).collect{ |n| Resultimporter.calc_vdot(42195,n.to_f/100)} # in minutes
    @run42km_vdot_recalc_hist = run42km_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose

    ########################################################################################################
    # Run 21km
    run21km_vdots=ImportedRaceResult.run21km_only.pluck(:vdot).map(&:to_f)
    @run21km_vdot_hist = run21km_vdots.histogram(bins, :bin_boundary => :min).transpose

    run21km_vdots_recalc=ImportedRaceResult.run21km_only.pluck(:total_time).collect{ |n| Resultimporter.calc_vdot(21100,n.to_f/100)} # in minutes
    @run21km_vdot_recalc_hist = run21km_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose


    ########################################################################################################
    # Run 10km
    run10km_vdots=ImportedRaceResult.run10km_only.pluck(:vdot).map(&:to_f)
    @run10km_vdot_hist = run10km_vdots.histogram(bins, :bin_boundary => :min).transpose
    
    run10km_vdots_recalc=ImportedRaceResult.run10km_only.pluck(:total_time).collect{ |n| Resultimporter.calc_vdot(10100,n.to_f/100)} # in minutes
    @run10km_vdot_recalc_hist = run21km_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose

    ########################################################################################################
    # Long-Distance
    ld_tritotaltime_vdots=ImportedRaceResult.ld_only.pluck(:tritotaltime_vdot).map(&:to_f)
    @ld_tritotaltime_vdot_hist = ld_tritotaltime_vdots.histogram(bins, :bin_boundary => :min).transpose
    
    ld_triswim_vdots=ImportedRaceResult.ld_only.pluck(:triswim_vdot).map(&:to_f)
    @ld_triswim_vdot_hist = ld_triswim_vdots.histogram(bins, :bin_boundary => :min).transpose
    
    ld_tribike_vdots=ImportedRaceResult.ld_only.pluck(:tribike_vdot).map(&:to_f)
    @ld_tribike_vdot_hist = ld_tribike_vdots.histogram(bins, :bin_boundary => :min).transpose
    
    ld_trirun_vdots=ImportedRaceResult.ld_only.pluck(:trirun_vdot).map(&:to_f)
    @ld_trirun_vdot_hist = ld_trirun_vdots.histogram(bins, :bin_boundary => :min).transpose
    
    ld_t1_vdots_recalc=ImportedRaceResult.ld_only.pluck(:transition1_time).collect{ |n| Resultimporter.calc_tdot(800,n.to_f/100,12*3600)} # in minutes
    @ld_t1_vdot_recalc_hist = ld_t1_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose


    ld_t1_vdots=ImportedRaceResult.ld_only.pluck(:transition1_vdot).map(&:to_f)
    @ld_t1_vdot_hist = ld_t1_vdots.histogram(bins, :bin_boundary => :min).transpose

    ld_t2_vdots=ImportedRaceResult.ld_only.pluck(:transition2_vdot).map(&:to_f)
    @ld_t2_vdot_hist = ld_t2_vdots.histogram(bins, :bin_boundary => :min).transpose

    ########################################################################################################
    # Half-Distance
    md_tritotaltime_vdots=ImportedRaceResult.md_only.pluck(:tritotaltime_vdot).map(&:to_f)
    @md_tritotaltime_vdot_hist = md_tritotaltime_vdots.histogram(bins, :bin_boundary => :min).transpose
    
    md_triswim_vdots=ImportedRaceResult.md_only.pluck(:triswim_vdot).map(&:to_f)
    @md_triswim_vdot_hist = md_triswim_vdots.histogram(bins, :bin_boundary => :min).transpose
    
    md_tribike_vdots=ImportedRaceResult.md_only.pluck(:tribike_vdot).map(&:to_f)
    @md_tribike_vdot_hist = md_tribike_vdots.histogram(bins, :bin_boundary => :min).transpose
    
    md_trirun_vdots=ImportedRaceResult.md_only.pluck(:trirun_vdot).map(&:to_f)
    @md_trirun_vdot_hist = md_trirun_vdots.histogram(bins, :bin_boundary => :min).transpose
    
    md_t1_vdots=ImportedRaceResult.md_only.pluck(:transition1_vdot).map(&:to_f)
    @md_t1_vdot_hist = md_t1_vdots.histogram(bins, :bin_boundary => :min).transpose

    md_t2_vdots=ImportedRaceResult.md_only.pluck(:transition2_vdot).map(&:to_f)
    @md_t2_vdot_hist = md_t2_vdots.histogram(bins, :bin_boundary => :min).transpose
  
     # Percentile VDOT table
    lim=-1 #20000
    all_vdots_per_racetypes=[run10km_vdots[0..lim], run21km_vdots[0..lim], run42km_vdots[0..lim], ld_tritotaltime_vdots[0..lim], ld_triswim_vdots[0..lim], ld_tribike_vdots[0..lim], ld_trirun_vdots[0..lim], md_tritotaltime_vdots[0..lim], md_triswim_vdots[0..lim], md_tribike_vdots[0..lim], md_trirun_vdots[0..lim]]
    #all_vdots_per_racetypes=[run10km_vdots[0..10000], run21km_vdots[0..1000], run42km_vdots[0..1000], ld_tritotaltime_vdots[0..1000], md_tritotaltime_vdots[0..1000]] 
    @table_headers=["10km","21km","42km","LDtt","LDsw","LDbk","LDrn","MDtt","MDsw","MDbk","MDrn"]
    @percentiles=[5,10,25,50,75,90,95,97,99,99.9]
    @vdot_per_percentiles_per_racetypes=[]
    all_vdots_per_racetypes.each do |all_vdots_per_racetype|
      vdot_per_percentiles_per_racetype=[]
      @percentiles.each do |percentile|
        vdot_per_percentile_per_racetype = all_vdots_per_racetype.percentile(percentile).to_f
        vdot_per_percentiles_per_racetype << vdot_per_percentile_per_racetype
      end
      @vdot_per_percentiles_per_racetypes << vdot_per_percentiles_per_racetype
    end
    
  end
 
  # GET MD VDOT statistics
  def md_vdot_stats
    bins=[0,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,64,66,68,70,72,74,76,78,80]
    #bins=[0,20,25,30,35,40,45,50,55,60,65,70,75,80,85]
    run42km_vdots_recalc=ImportedRaceResult.run42km_only.pluck(:total_time).collect{ |n| Resultimporter.calc_vdot(42195,n.to_f/100)} # in minutes
    @run42km_vdot_recalc_hist = Resultimporter.normalize_histogram(run42km_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    run21km_vdots_recalc=ImportedRaceResult.run21km_only.pluck(:total_time).collect{ |n| Resultimporter.calc_vdot(21100,n.to_f/100)} # in minutes
    @run21km_vdot_recalc_hist = Resultimporter.normalize_histogram(run21km_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
    ########################################################################################################
    # Half-Distance
    (md_total_times, md_swim_times, md_bike_times, md_run_times) = ImportedRaceResult.md_only.pluck(:total_time, :swim_time, :bike_time, :run_time).transpose
    
    md_tritotaltime_vdots=ImportedRaceResult.md_only.pluck(:tritotaltime_vdot).map(&:to_f)
    @md_tritotaltime_vdot_hist = Resultimporter.normalize_histogram(md_tritotaltime_vdots.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
    md_tritotaltime_vdots_recalc=md_total_times.collect{ |n| Resultimporter.calc_tridot(1900, 90000, 21100, n.to_f/100)} 
    @md_tritotaltime_vdot_recalc_hist = Resultimporter.normalize_histogram(md_tritotaltime_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
    md_triswim_vdots=ImportedRaceResult.md_only.pluck(:triswim_vdot).map(&:to_f)
    @md_triswim_vdot_hist = Resultimporter.normalize_histogram(md_triswim_vdots.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
    md_triswim_vdots_recalc=md_swim_times.collect{ |n| Resultimporter.calc_sdot(1900,n.to_f/100,12*3600)} 
    @md_triswim_vdot_recalc_hist = Resultimporter.normalize_histogram(md_triswim_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose[1..-1])

    md_tribike_vdots=ImportedRaceResult.md_only.pluck(:tribike_vdot).map(&:to_f)
    @md_tribike_vdot_hist = Resultimporter.normalize_histogram(md_tribike_vdots.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
    md_tribike_vdots_recalc=md_bike_times.collect{ |n| Resultimporter.calc_bdot(90000,n.to_f/100,12*3600)} 
    @md_tribike_vdot_recalc_hist = Resultimporter.normalize_histogram(md_tribike_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
    md_trirun_vdots=ImportedRaceResult.md_only.pluck(:trirun_vdot).map(&:to_f)
    @md_trirun_vdot_hist = Resultimporter.normalize_histogram(md_trirun_vdots.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
    md_trirun_vdots_recalc=md_run_times.collect{ |n| Resultimporter.calc_rdot(21100,n.to_f/100,12*3600)} 
    @md_trirun_vdot_recalc_hist = Resultimporter.normalize_histogram(md_trirun_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
    end

  # GET LD VDOT statistics
  def ld_vdot_stats
    bins=[0,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,64,66,68,70,72,74,76,78,80]
    #bins=[0,20,25,30,35,40,45,50,55,60,65,70,75,80,85]
    run42km_vdots_recalc=ImportedRaceResult.run42km_only.pluck(:total_time).collect{ |n| Resultimporter.calc_vdot(42195,n.to_f/100)} # in minutes
    @run42km_vdot_recalc_hist = Resultimporter.normalize_histogram(run42km_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    run21km_vdots_recalc=ImportedRaceResult.run21km_only.pluck(:total_time).collect{ |n| Resultimporter.calc_vdot(21100,n.to_f/100)} # in minutes
    @run21km_vdot_recalc_hist = Resultimporter.normalize_histogram(run21km_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
    ########################################################################################################
    # Full-Distance
    (ld_total_times, ld_swim_times, ld_bike_times, ld_run_times) = ImportedRaceResult.ld_only.pluck(:total_time, :swim_time, :bike_time, :run_time).transpose
    
    ld_tritotaltime_vdots=ImportedRaceResult.ld_only.pluck(:tritotaltime_vdot).map(&:to_f)
    @ld_tritotaltime_vdot_hist = Resultimporter.normalize_histogram(ld_tritotaltime_vdots.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
    ld_tritotaltime_vdots_recalc=ld_total_times.collect{ |n| Resultimporter.calc_tridot(3800, 180000, 42195, n.to_f/100)} 
    @ld_tritotaltime_vdot_recalc_hist = Resultimporter.normalize_histogram(ld_tritotaltime_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
    ld_triswim_vdots=ImportedRaceResult.ld_only.pluck(:triswim_vdot).map(&:to_f)
    @ld_triswim_vdot_hist = Resultimporter.normalize_histogram(ld_triswim_vdots.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
    ld_triswim_vdots_recalc=ld_swim_times.collect{ |n| Resultimporter.calc_sdot(3800,n.to_f/100,12*3600)} 
    @ld_triswim_vdot_recalc_hist = Resultimporter.normalize_histogram(ld_triswim_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose[1..-1])

    ld_tribike_vdots=ImportedRaceResult.ld_only.pluck(:tribike_vdot).map(&:to_f)
    @ld_tribike_vdot_hist = Resultimporter.normalize_histogram(ld_tribike_vdots.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
    ld_tribike_vdots_recalc=ld_bike_times.collect{ |n| Resultimporter.calc_bdot(180000,n.to_f/100,12*3600)} 
    @ld_tribike_vdot_recalc_hist = Resultimporter.normalize_histogram(ld_tribike_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
    ld_trirun_vdots=ImportedRaceResult.ld_only.pluck(:trirun_vdot).map(&:to_f)
    @ld_trirun_vdot_hist = Resultimporter.normalize_histogram(ld_trirun_vdots.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
    ld_trirun_vdots_recalc=ld_run_times.collect{ |n| Resultimporter.calc_rdot(42195,n.to_f/100,12*3600)} 
    @ld_trirun_vdot_recalc_hist = Resultimporter.normalize_histogram(ld_trirun_vdots_recalc.histogram(bins, :bin_boundary => :min).transpose[1..-1])
    
  end

  # GET /race_statistics
  def index
    @race_statistics = RaceStatistic.page(params[:page])
    @race_statistics_grid = initialize_grid(RaceStatistic,
        include: [:imported_race],
        name: 'grid',
        order: 'race_statistics.total_time_avg',
        order_direction: 'desc',
        enable_export_to_csv: true,
        csv_file_name: 'exported_race_statistics',
        per_page:20)
   

    gon.xy_vdot_marathon_table = [calc_vdot_marathon_table]
    gon.legend_vdot_marathon_table = ["VDOT"]

    #Collect the number of athletes in each vdot_level for the following RaceTypes
      #Vdot-Level  10k-Run   Half-Marathon   Marathon  Olympic   Halfdistance  Longdistance
      #15           3             65            0         0           0             0
      #20           116           290           141       0           39            0
      #25           919           2420          2827      0           898           26
      #30           2846          9764          9783      0           2309          855
      #35           4197          14849         14912     0           3388          2145
      #....
      #85           7             8             0         0           13            84  
    @race_type_descriptions=["10k-Run","Half-Marathon","Marathon","Olympic","Halfdistance","Longdistance"]
   
    @num_vdot_per_level_per_racetype = []
    @race_type_descriptions.each do | race_type_description |
      num_vdot_per_level = []
      num_vdot_per_level << RaceStatistic.includes(:imported_race).where(:imported_races => { race_type_id: RaceType.where("description=?",race_type_description).first.id}).pluck(:num_vdot_level1).sum.to_i
      num_vdot_per_level << RaceStatistic.includes(:imported_race).where(:imported_races => { race_type_id: RaceType.where("description=?",race_type_description).first.id}).pluck(:num_vdot_level2).sum.to_i
      num_vdot_per_level << RaceStatistic.includes(:imported_race).where(:imported_races => { race_type_id: RaceType.where("description=?",race_type_description).first.id}).pluck(:num_vdot_level3).sum.to_i
      num_vdot_per_level << RaceStatistic.includes(:imported_race).where(:imported_races => { race_type_id: RaceType.where("description=?",race_type_description).first.id}).pluck(:num_vdot_level4).sum.to_i
      num_vdot_per_level << RaceStatistic.includes(:imported_race).where(:imported_races => { race_type_id: RaceType.where("description=?",race_type_description).first.id}).pluck(:num_vdot_level5).sum.to_i
      num_vdot_per_level << RaceStatistic.includes(:imported_race).where(:imported_races => { race_type_id: RaceType.where("description=?",race_type_description).first.id}).pluck(:num_vdot_level6).sum.to_i
      num_vdot_per_level << RaceStatistic.includes(:imported_race).where(:imported_races => { race_type_id: RaceType.where("description=?",race_type_description).first.id}).pluck(:num_vdot_level7).sum.to_i
      num_vdot_per_level << RaceStatistic.includes(:imported_race).where(:imported_races => { race_type_id: RaceType.where("description=?",race_type_description).first.id}).pluck(:num_vdot_level8).sum.to_i
      num_vdot_per_level << RaceStatistic.includes(:imported_race).where(:imported_races => { race_type_id: RaceType.where("description=?",race_type_description).first.id}).pluck(:num_vdot_level9).sum.to_i
      num_vdot_per_level << RaceStatistic.includes(:imported_race).where(:imported_races => { race_type_id: RaceType.where("description=?",race_type_description).first.id}).pluck(:num_vdot_level10).sum.to_i
      num_vdot_per_level << RaceStatistic.includes(:imported_race).where(:imported_races => { race_type_id: RaceType.where("description=?",race_type_description).first.id}).pluck(:num_vdot_level11).sum.to_i
      num_vdot_per_level << RaceStatistic.includes(:imported_race).where(:imported_races => { race_type_id: RaceType.where("description=?",race_type_description).first.id}).pluck(:num_vdot_level12).sum.to_i
      num_vdot_per_level << RaceStatistic.includes(:imported_race).where(:imported_races => { race_type_id: RaceType.where("description=?",race_type_description).first.id}).pluck(:num_vdot_level13).sum.to_i
      num_vdot_per_level << RaceStatistic.includes(:imported_race).where(:imported_races => { race_type_id: RaceType.where("description=?",race_type_description).first.id}).pluck(:num_vdot_level14).sum.to_i
      num_vdot_per_level << RaceStatistic.includes(:imported_race).where(:imported_races => { race_type_id: RaceType.where("description=?",race_type_description).first.id}).pluck(:num_vdot_level15).sum.to_i
      @num_vdot_per_level_per_racetype << num_vdot_per_level 
    end
    xvals=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
    gon.xy_num_vdot_per_level_per_racetype=[]
    @num_vdot_per_level_per_racetype.each do | num_vdot_per_level | 
            xy_line = Resultanalyzer.map_xy_array(xvals, num_vdot_per_level)
            gon.xy_num_vdot_per_level_per_racetype << xy_line
    end
    gon.legend_race_type_descriptions = @race_type_descriptions
  end

  # GET /race_statistics/1
  def show
  end

  # GET /race_statistics/new
  def new
    @race_statistic = RaceStatistic.new
  end

  # GET /race_statistics/1/edit
  def edit
  end

  # POST /race_statistics
  def create
    @race_statistic = RaceStatistic.new(race_statistic_params)

    if @race_statistic.save
      redirect_to @race_statistic, notice: 'Race statistic was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /race_statistics/1
  def update
    if @race_statistic.update(race_statistic_params)
      redirect_to @race_statistic, notice: 'Race statistic was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /race_statistics/1
  def destroy
    @race_statistic.destroy
    redirect_to race_statistics_url, notice: 'Race statistic was successfully destroyed.'
  end


    ###########################################################
    # Calculate vdot values for following marathon times      #
    # 120min, 125min, ... 360min                              #
    # return [[120, vdot@100minutes], [105, ], ... [360, ] ]  #
    ###########################################################
    
    def calc_vdot_marathon_table
      vdots=[]
      (24..72).to_a.each do | idx |
        vdots << [idx.to_i*300/60, calc_vdot(42195, idx.to_i*300)]
      end
      vdots
    end

    ###########################################################
    #  Calculate Jack Daniels vdot                            #
    #  distance in meters as int eg. 42195 for Marathon #
    #  duration in seconds: eg: 3x3600 for 3:00:00            #
    ###########################################################
    def calc_vdot(distance, duration)
       checked_distance=nil
       #check if duration is not 0 to avoid div by 0 -> NaN
       if duration.nil?
          vdot=-1
       elsif duration > 0
           pvo2max=calc_pvo2max(duration)
           excel_duration=duration/(24*3600).to_f #xls time format = Minutes of a day: 23:59:00 = 1339,0 
           vdot=(-4.6+0.182258*(distance/excel_duration/1440)+0.000104*(distance/excel_duration/1440)**2)/pvo2max
          if vdot < 0 #happens if distance is too small or time too big
            vdot = -1 #
          end
        #duration == 0
        else
           vdot=-1
        end
        vdot
    end 

    ######################################################
    #  Calculate VO2Max in percent                        #
    #  duration in seconds: eg: 3x3600 for 3:00:00        #
    #######################################################
    def calc_pvo2max(duration)
       excel_duration=duration/(24*3600).to_f #xls time format = Minutes of a day: 23:59:00 = 1339,0
       pvo2max=0.8+0.1894393 * Math.exp(-0.012778*excel_duration*1440)+0.2989558* Math.exp(-0.1932605*duration*1440)
    end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_race_statistic
      @race_statistic = RaceStatistic.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def race_statistic_params
      params.require(:race_statistic).permit(:imported_race_id, :total_time_avg, :swim_time_avg, :bike_time_avg, :run_time_avg, :transition1_time_avg, :transition2_time_avg, :num_results, :num_dnf_results, :num_finisher_results)
    end

end
