class ResultAnalyzerPolicy < ApplicationPolicy

attr_reader :imported_race, :record

  def initialize(imported_race, record)
    @race_statistic = race_statistic
    @record = record
  end

  def index?
    true
  end

  def show?
    scope.where(:id => record.id).exists?
  end

  def scope
    Pundit.policy_scope!(race_statistic, record.class)
  end

  class Scope
    attr_reader :race_statistic, :scope

    def initialize(user, scope)
      @race_statistic = race_statistic
      @record = record
    end

    def resolve
      scope
    end
  end



end