class RaceStatistic < ActiveRecord::Base
  belongs_to :imported_race, dependent: :destroy

  scope :ld_only, -> { joins(:imported_race).where('imported_races.race_type_id = ?', RaceType.find_by_description("Longdistance").id)}
  scope :md_only, -> { joins(:imported_race).where('imported_races.race_type_id = ?', RaceType.find_by_description("Halfdistance").id)}
  scope :run42km_only, -> { joins(:imported_race).where('imported_races.race_type_id = ?', RaceType.find_by_description("Marathon").id)}
  scope :run21km_only, -> { joins(:imported_race).where('imported_races.race_type_id = ?', RaceType.find_by_description("Half-Marathon").id)}
  scope :run10km_only, -> { joins(:imported_race).where('imported_races.race_type_id = ?', RaceType.find_by_description("10k-Run").id)}


end
