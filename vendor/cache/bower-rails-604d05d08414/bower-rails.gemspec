# -*- encoding: utf-8 -*-
# stub: bower-rails 0.11.0 ruby lib

Gem::Specification.new do |s|
  s.name = "bower-rails"
  s.version = "0.11.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Ross Harrison"]
  s.date = "2013-12-12"
  s.description = "Rails integration for bower."
  s.email = "rtharrison86@gmail.com"
  s.files = ["MIT-LICENSE", "README.md", "lib/bower-rails", "lib/bower-rails.rb", "lib/bower-rails/dsl.rb", "lib/bower-rails/performer.rb", "lib/bower-rails/railtie.rb", "lib/bower-rails/version.rb", "lib/generators", "lib/generators/bower_rails", "lib/generators/bower_rails/initialize", "lib/generators/bower_rails/initialize/initialize_generator.rb", "lib/generators/bower_rails/initialize/templates", "lib/generators/bower_rails/initialize/templates/Bowerfile", "lib/generators/bower_rails/initialize/templates/bower.json", "lib/generators/bower_rails/initialize/templates/bower_rails.rb", "lib/tasks", "lib/tasks/bower.rake"]
  s.homepage = "https://github.com/rharriso/bower-rails"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.8"
  s.summary = "Bower for Rails"

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rspec>, [">= 0"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<coveralls>, [">= 0"])
    else
      s.add_dependency(%q<rspec>, [">= 0"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<coveralls>, [">= 0"])
    end
  else
    s.add_dependency(%q<rspec>, [">= 0"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<coveralls>, [">= 0"])
  end
end
