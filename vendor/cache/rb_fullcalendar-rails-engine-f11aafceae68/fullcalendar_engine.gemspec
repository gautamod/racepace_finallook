# -*- encoding: utf-8 -*-
# stub: fullcalendar_engine 1.1.5 ruby lib

Gem::Specification.new do |s|
  s.name = "fullcalendar_engine"
  s.version = "1.1.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Mohit Bansal", "Aditya Kapoor", "Shubham Gupta"]
  s.date = "2016-11-09"
  s.description = "Engine Implementation of jQuery Full Calendar"
  s.email = ["info@vinsol.com"]
  s.files = ["MIT-LICENSE", "Rakefile", "app/assets", "app/assets/images", "app/assets/images/fullcalendar_engine", "app/assets/javascripts", "app/assets/javascripts/fullcalendar_engine", "app/assets/javascripts/fullcalendar_engine/application.js", "app/assets/javascripts/fullcalendar_engine/events.js.erb", "app/assets/javascripts/fullcalendar_engine/fullcalendar.js", "app/assets/javascripts/fullcalendar_engine/gcal.js", "app/assets/javascripts/fullcalendar_engine/jquery-ui-1.10.3.custom.min.js", "app/assets/stylesheets", "app/assets/stylesheets/fullcalendar_engine", "app/assets/stylesheets/fullcalendar_engine/application.css", "app/assets/stylesheets/fullcalendar_engine/fullcalendar.css", "app/assets/stylesheets/fullcalendar_engine/images", "app/assets/stylesheets/fullcalendar_engine/images/animated-overlay.gif", "app/assets/stylesheets/fullcalendar_engine/images/ui-bg_diagonals-thick_18_b81900_40x40.png", "app/assets/stylesheets/fullcalendar_engine/images/ui-bg_diagonals-thick_20_666666_40x40.png", "app/assets/stylesheets/fullcalendar_engine/images/ui-bg_flat_10_000000_40x100.png", "app/assets/stylesheets/fullcalendar_engine/images/ui-bg_glass_100_f6f6f6_1x400.png", "app/assets/stylesheets/fullcalendar_engine/images/ui-bg_glass_100_fdf5ce_1x400.png", "app/assets/stylesheets/fullcalendar_engine/images/ui-bg_glass_65_ffffff_1x400.png", "app/assets/stylesheets/fullcalendar_engine/images/ui-bg_gloss-wave_35_f6a828_500x100.png", "app/assets/stylesheets/fullcalendar_engine/images/ui-bg_highlight-soft_100_eeeeee_1x100.png", "app/assets/stylesheets/fullcalendar_engine/images/ui-bg_highlight-soft_75_ffe45c_1x100.png", "app/assets/stylesheets/fullcalendar_engine/images/ui-icons_222222_256x240.png", "app/assets/stylesheets/fullcalendar_engine/images/ui-icons_228ef1_256x240.png", "app/assets/stylesheets/fullcalendar_engine/images/ui-icons_ef8c08_256x240.png", "app/assets/stylesheets/fullcalendar_engine/images/ui-icons_ffd27a_256x240.png", "app/assets/stylesheets/fullcalendar_engine/images/ui-icons_ffffff_256x240.png", "app/assets/stylesheets/fullcalendar_engine/jquery-ui-1.10.3.custom.css", "app/assets/stylesheets/fullcalendar_engine/style.css", "app/controllers", "app/controllers/fullcalendar_engine", "app/controllers/fullcalendar_engine/application_controller.rb", "app/controllers/fullcalendar_engine/events_controller.rb", "app/models", "app/models/fullcalendar_engine", "app/models/fullcalendar_engine/event.rb", "app/models/fullcalendar_engine/event_series.rb", "app/views", "app/views/fullcalendar_engine", "app/views/fullcalendar_engine/events", "app/views/fullcalendar_engine/events/_edit_form.html.erb", "app/views/fullcalendar_engine/events/_form.html.erb", "app/views/fullcalendar_engine/events/index.html.erb", "app/views/fullcalendar_engine/events/new.js.erb", "config/full-calendar.yml.dummy", "config/initializers", "config/initializers/configuration.rb", "config/initializers/constants.rb", "config/routes.rb", "db/migrate", "db/migrate/20131203105320_create_fullcalendar_engine_events.rb", "db/migrate/20131203105529_create_fullcalendar_engine_event_series.rb", "lib/fullcalendar_engine", "lib/fullcalendar_engine.rb", "lib/fullcalendar_engine/engine.rb", "lib/fullcalendar_engine/version.rb", "lib/generators", "lib/generators/fullcalendar_engine", "lib/generators/fullcalendar_engine/install", "lib/generators/fullcalendar_engine/install/install_generator.rb", "lib/tasks", "lib/tasks/fullcalendar_engine_tasks.rake", "test/controllers", "test/controllers/fullcalendar_engine", "test/controllers/fullcalendar_engine/events_controller_test.rb", "test/fixtures", "test/fixtures/fullcalendar_engine", "test/fixtures/fullcalendar_engine/event_series.yml", "test/fixtures/fullcalendar_engine/events.yml", "test/fullcalendar_engine_test.rb", "test/helpers", "test/helpers/fullcalendar_engine", "test/helpers/fullcalendar_engine/events_helper_test.rb", "test/integration", "test/integration/navigation_test.rb", "test/models", "test/models/fullcalendar_engine", "test/models/fullcalendar_engine/event_series_test.rb", "test/models/fullcalendar_engine/event_test.rb", "test/test_helper.rb"]
  s.homepage = "http://vinsol.com"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.8"
  s.summary = "Engine Implementation of jQuery Full Calendar"
  s.test_files = ["test/integration", "test/integration/navigation_test.rb", "test/test_helper.rb", "test/controllers", "test/controllers/fullcalendar_engine", "test/controllers/fullcalendar_engine/events_controller_test.rb", "test/helpers", "test/helpers/fullcalendar_engine", "test/helpers/fullcalendar_engine/events_helper_test.rb", "test/fixtures", "test/fixtures/fullcalendar_engine", "test/fixtures/fullcalendar_engine/event_series.yml", "test/fixtures/fullcalendar_engine/events.yml", "test/models", "test/models/fullcalendar_engine", "test/models/fullcalendar_engine/event_test.rb", "test/models/fullcalendar_engine/event_series_test.rb", "test/fullcalendar_engine_test.rb"]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rails>, [">= 4.0.0"])
    else
      s.add_dependency(%q<rails>, [">= 4.0.0"])
    end
  else
    s.add_dependency(%q<rails>, [">= 4.0.0"])
  end
end
