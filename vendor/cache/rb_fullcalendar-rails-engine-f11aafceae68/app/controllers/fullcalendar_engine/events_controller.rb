require_dependency 'fullcalendar_engine/application_controller'

module FullcalendarEngine
  class EventsController < ApplicationController

    layout FullcalendarEngine::Configuration['layout'] || 'application'

    before_filter :load_event, only: [:edit, :update, :destroy, :move, :resize]
    before_filter :determine_event_type, only: :create

    def create
      unless params[:trainingplan_id].nil?
        workout=Trainingplan.find_by_id(params[:trainingplan_id]).workout.create(
            title: "created by click in calendar",
            sport_type_id: SportType.where(description: "Run").first.id)
        params[:ev_id]=workout.id
        params[:ev_type]=workout.class.name
      end
      @event.update(ev_id: params[:ev_id], ev_type: params[:ev_type]) 

      if @event.save
        #render nothing: true
        redirect_to :back
      else
        render text: @event.errors.full_messages.to_sentence, status: 422
      end

    end

    def new
      respond_to do |format|
        format.js
      end
    end

    def get_events
      start_time = Time.at(params[:start].to_i).to_formatted_s(:db)
      end_time   = Time.at(params[:end].to_i).to_formatted_s(:db)
      #start_time = Time.at(Time.parse(params[:start]).to_i).to_formatted_s(:db)
      #end_time   = Time.at(Time.parse(params[:end]).to_i).to_formatted_s(:db)

      if current_user == nil
        my_trainingplans_to_show = [] #workouts which belongs to a trainingplan
        my_metrics_to_show = []  #workouts which belongs to current_user
        my_workouts_to_show = [] #workouts which belongs to current_user
      else
        my_trainingplans_to_show = current_user.trainingplans.where(show: true)
        my_metrics_to_show = current_user.metrics.all
        my_workouts_to_show = current_user.workouts.all
      end
      
      events = []
      
      ##############################################################
      # Metrics    
      my_metrics_to_show.each do |metric|
        @events = Event.where('
                  (starttime >= :start_time and endtime <= :end_time) or
                  (starttime >= :start_time and endtime > :end_time and starttime <= :end_time) or
                  (starttime <= :start_time and endtime >= :start_time and endtime <= :end_time) or
                  (starttime <= :start_time and endtime > :end_time)',
                  start_time: start_time, end_time: end_time).where(ev_type: metric.class.name).where(ev_id: metric.id)

        @events.each do |event|
          events << { id: event.id,
                    title: event.title,
                    description: event.description || '', 
                    start: event.starttime.iso8601,
                    end: event.endtime.iso8601,
                    allDay: event.all_day,
                    className: "weight-metric-event",
                    backgroundColor: '#1AB394', #fixme: don't hard code color here
                    borderColor: '#000000',
                    recurring: (event.event_series_id) ? true : false }
        end
      end      

      ##############################################################
      # Workouts (which belongs not to a trainingplan but which are uploaded by the user)    
      my_workouts_to_show.each do |workout|
        @events = Event.where('
                  (starttime >= :start_time and endtime <= :end_time) or
                  (starttime >= :start_time and endtime > :end_time and starttime <= :end_time) or
                  (starttime <= :start_time and endtime >= :start_time and endtime <= :end_time) or
                  (starttime <= :start_time and endtime > :end_time)',
                  start_time: start_time, end_time: end_time).where(ev_type: workout.class.name).where(ev_id: workout.id)

        @events.each do |event|
          events << { id: event.id,
                    title: event.title,
                    description: event.description || '', 
                    start: event.starttime.iso8601,
                    end: event.endtime.iso8601,
                    allDay: event.all_day,
                    className: "workout-event",
                    backgroundColor: '#10B394', #fixme: don't hard code color here
                    borderColor: '#000000',
                    recurring: (event.event_series_id) ? true : false }
        end
      end      
 
      ##############################################################
      # Trainingplans (contains some workouts)   
      my_trainingplans_to_show.each do |my_trainingplan|
        backgroundColor = my_trainingplan.color 
        my_trainingplan.workout.each do |workout|
          case workout.sport_type.description 
            when "Swim"
              className = "swim-workout-event"
            when "Bike"
              className = "bike-workout-event"
            when "Run"
              className = "run-workout-event"
            else
              className = "generic-workout-event"
            end
          
          @events = Event.where('
                    (starttime >= :start_time and endtime <= :end_time) or
                    (starttime >= :start_time and endtime > :end_time and starttime <= :end_time) or
                    (starttime <= :start_time and endtime >= :start_time and endtime <= :end_time) or
                    (starttime <= :start_time and endtime > :end_time)',
                    start_time: start_time, end_time: end_time).where(ev_type: workout.class.name).where(ev_id: workout.id)

          @events.each do |event|
            events << { id: event.id,
                      title: event.title,
                      description: event.description || '', 
                      start: event.starttime.iso8601,
                      end: event.endtime.iso8601,
                      allDay: event.all_day,
                      className: className,
                      backgroundColor: backgroundColor,
                      borderColor: '#000000',
                      recurring: (event.event_series_id) ? true : false }
          end #each event
        end  #each workout
      end #each trainingplan

      render json: events.to_json
    end

    def move
      if @event
        @event.starttime = make_time_from_minute_and_day_delta(@event.starttime)
        @event.endtime   = make_time_from_minute_and_day_delta(@event.endtime)
        @event.all_day   = params[:all_day]
        @event.save
      end
      render nothing: true
    end

    def resize
      if @event
        @event.endtime = make_time_from_minute_and_day_delta(@event.endtime)
        @event.save
      end    
      render nothing: true
    end

    def edit
      render json: { form: render_to_string(partial: 'edit_form') } 
    end

    def update
      case params[:event][:commit_button]
      when 'Update All Occurrence'
        @events = @event.event_series.events
        @event.update_events(@events, event_params)
      when 'Update All Following Occurrence'
        @events = @event.event_series.events.where('starttime > :start_time', 
                                                   start_time: @event.starttime.to_formatted_s(:db)).to_a
        @event.update_events(@events, event_params)
      else
        @event.attributes = event_params
        @event.save
      end
      render nothing: true
    end

    def destroy
      case params[:delete_all]
      when 'true'
        @event.event_series.destroy
      when 'future'
        @events = @event.event_series.events.where('starttime > :start_time',
                                                   start_time: @event.starttime.to_formatted_s(:db)).to_a
        @event.event_series.events.delete(@events)
      else
        @event.destroy
      end
      render nothing: true
    end

    private

    def load_event
      @event = Event.where(:id => params[:id]).first
      unless @event
        render json: { message: "Event Not Found.."}, status: 404 and return
      end
    end

    def event_params
      params.require(:event).permit('title', 'description', 'starttime', 'endtime', 'all_day', 'period', 'frequency', 'commit_button', 'workout_id')
    end

    def determine_event_type
      if params[:event][:period] == "Does not repeat"
        @event = Event.new(event_params)
      else
        @event = EventSeries.new(event_params)
      end
    end

    def make_time_from_minute_and_day_delta(event_time)
      params[:minute_delta].to_i.minutes.from_now((params[:day_delta].to_i).days.from_now(event_time))
    end
  end
end
