# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160329142454) do

  create_table "import_sources", force: :cascade do |t|
    t.string   "description", limit: 64
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "imported_race_results", force: :cascade do |t|
    t.integer  "imported_race_id"
    t.string   "start_number",              limit: 16
    t.string   "name",                      limit: 64
    t.string   "surname",                   limit: 64
    t.string   "gender",                    limit: 32
    t.date     "birthday"
    t.string   "profession",                limit: 64
    t.string   "club",                      limit: 64
    t.string   "country",                   limit: 64
    t.string   "city",                      limit: 64
    t.float    "latitude",                  limit: 12
    t.float    "longitude",                 limit: 12
    t.string   "division",                  limit: 32
    t.integer  "division_rank",             limit: 3
    t.integer  "division_num_participants", limit: 3
    t.integer  "overall_rank",              limit: 3
    t.integer  "overall_num_participants",  limit: 3
    t.integer  "total_time",                limit: 4
    t.integer  "swim_time",                 limit: 4
    t.integer  "bike_time",                 limit: 4
    t.integer  "run_time",                  limit: 4
    t.integer  "transition1_time",          limit: 4
    t.integer  "transition2_time",          limit: 4
    t.integer  "import_source_id"
    t.string   "import_source_url",         limit: 256
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "vdot",                      limit: 12
    t.float    "improved_vdot",             limit: 12
    t.float    "tritotaltime_vdot",         limit: 12
    t.float    "triswim_vdot",              limit: 12
    t.float    "tribike_vdot",              limit: 12
    t.float    "trirun_vdot",               limit: 12
    t.float    "transition1_vdot",          limit: 12
    t.float    "transition2_vdot",          limit: 12
    t.boolean  "finished",                              default: true
  end

  add_index "imported_race_results", ["import_source_id"], name: "index_imported_race_results_on_import_source_id"
  add_index "imported_race_results", ["imported_race_id"], name: "index_imported_race_results_on_imported_race_id"

  create_table "imported_races", force: :cascade do |t|
    t.string   "series",            limit: 64
    t.string   "name",              limit: 128, null: false
    t.string   "extended_name",     limit: 128
    t.string   "organizer",         limit: 128
    t.string   "country",           limit: 64
    t.string   "city",              limit: 64
    t.float    "latitude",          limit: 12
    t.float    "longitude",         limit: 12
    t.date     "date"
    t.integer  "sport_type_id"
    t.integer  "race_type_id"
    t.integer  "total_distance",    limit: 3
    t.integer  "swim_distance",     limit: 3
    t.integer  "bike_distance",     limit: 3
    t.integer  "run_distance",      limit: 3
    t.integer  "import_source_id"
    t.string   "import_source_url", limit: 256
    t.float    "distance"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "imported_races", ["import_source_id"], name: "index_imported_races_on_import_source_id"
  add_index "imported_races", ["race_type_id"], name: "index_imported_races_on_race_type_id"
  add_index "imported_races", ["sport_type_id"], name: "index_imported_races_on_sport_type_id"

  create_table "race_types", force: :cascade do |t|
    t.string   "description", limit: 64
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sport_types", force: :cascade do |t|
    t.string   "description", limit: 64
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
