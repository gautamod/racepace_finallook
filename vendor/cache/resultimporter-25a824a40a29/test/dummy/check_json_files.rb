# USAGE: ruby remap_json_files.rb
# Will read all files in db/seeds/*.json
# and remap/recreate new json files in db/seedsremap/

require "zlib"
require "json"
require "pp"
require "chronic_duration"
require 'progress_bar'

        #Crawler and Importer Version <3.x.x tables config
        #MyAthlete = Struct.new(:atname, :atsurname, :atgender, :Address, :atbirthday, :atprofession, :atclub, :atadcoun, :atadcity, :atadzip, :atadstreet, :atadnumber, :atadlongitude, :atadlatitude)
        #MyRace = Struct.new(:series, :name, :country, :city, :organizer, :sporttype, :distance, :swmdistance, :bikedistance, :rundistance, :url, :date, :rceventlistid, :parserid, :tableheader, :errormsg)
        #MyRaceresult = Struct.new(:Race, :name, :surname, :gender, :birthday, :profession, :club, :country, :city, :zip, :street, :number, :latitude, :longitude, :startno, :division, :divisionrank, :divisionparticipants, :overallrank, :overallrankparticipants, :overalltime, :swimtime, :biketime, :runtime, :t1time, :t2time, :link)
        
        #Crawler and Importer Version 3.x.x config
        MyImportedRaceV3 = Struct.new(:series, :name, :extended_name, :organizer, :country, :city, :latitude, :longitude, :date, :sport_type_id, :race_type_id, :total_distance, :swim_distance, :bike_distance, :run_distance, :import_source_id, :import_source_url)
        MyImportedRaceResultV3 = Struct.new(:Race, :name, :surname, :gender, :birthday, :profession, :club, :country, :city, :longitude, :latitude, :imported_race_id, :start_number, :division, :division_rank, :division_num_participants, :overall_rank, :overall_num_participants, :total_time, :swim_time, :bike_time, :run_time, :transition1_time, :transition2_time, :import_source_id, :import_source_url)


#######################################################################
# Create a filename including path from race infos                    #
# e.g. [racefilespath]/race__2014-11-hannover-2014__stman-9-km__no562.json   #
#######################################################################
def create_race_filename(racefilespath, rc, num_results)
   rc_date = rc.date.to_time.strftime("%Y-%m-%d")
   racefilespath +"race3__"+rc_date+"__"+rc.name.parameterize+"__"+rc.extended_name.parameterize+"__no"+num_results.to_s+".json"
end

##################################################################
# Import all racefiles.json incl. Athletes and Results from dir  #
##################################################################
def check_all_in_dir(racefilespath)
  #Loop through all .json files 
  num_race_results = Dir.glob(racefilespath+"race3_*.json").length
  if num_race_results < 1 
    puts "No files in #{racefilespath} found to import"
  else
    bar1= ProgressBar.new(num_race_results, :bar, :counter, :eta)
    Dir.glob(racefilespath+"race3_*.json").each do |filename|
      #puts " "
      #puts "Checking: #{filename.split('/')[-1]}"
      #puts "Checking #{num_race_results.to_s} files:"
      bar1.increment!
      #check_num_results_in_file(filename)
      check_if_race_file_exists_with_different_no_results(filename)
    end
  end
end

#############################################################################
# Check if race_[rcdate][rcname]_noxxx.json already exists in racefilespath #
# returncodes                                 #
# -2 if multiple files are found (should not happen)            #
# -1 if if no file is found                         #
# 0 if no results = 0                           #
# xxxx if file is found return number of results              #
#############################################################################
def check_if_race_file_exists_with_different_no_results(filename)
   
   filenames_with_same_name=Dir.glob(filename.split('__no')[0]+"__no*.json")
   #if filenames_with_same_name.length == 0
   #  #file doesn't exist in directory
   #  returncode = -1
   #elsif filenames_with_same_name.length == 1
    #one file found with same racename 
     #no_results_in_file = filenames_with_same_name[0].split('__no')[1].split('.json')[0].to_i
     #returncode = no_results_in_file
   if  filenames_with_same_name.length > 1
    puts "\nWARNING: having more than one .json file for one race:"#should never happen
    smallest_no_results=99999999
    biggest_no_results=0
    file_to_delete=""
    file_to_keep=""
    filenames_with_same_name.each do |fn|
      rrjsons_compressed = File.read(fn)
      rrjsons = Zlib.inflate(rrjsons_compressed)
      rrhash=JSON.parse(rrjsons)
      puts "WARNING: #{fn} has #{rrhash.length}"
      if rrhash.length < smallest_no_results
        smallest_no_results = rrhash.length
        file_to_delete=fn
      end
      if rrhash.length > biggest_no_results
        biggest_no_results = rrhash.length
        file_to_keep =fn
      end
    end
    puts "WARNING: deleting file with less results: #{file_to_delete}"
    FileUtils.rm(file_to_delete) 
    returncode = file_to_keep.split('__no')[1].split('.json')[0].to_i
  end
  [returncode,filenames_with_same_name]
end


#####################################################################
# Check if the number of results in file matches the __noxxxx value #
#####################################################################
def check_num_results_in_file(filename)
 rrjsons_compressed = File.read(filename)
 rrjsons = Zlib.inflate(rrjsons_compressed)
 #rrjsons = ActiveSupport::Gzip.uncompress(rrjsons_compressed)
     
  #Check if this is a parsable file
  if rrjsons.include?("Race")
    rrhash=JSON.parse(rrjsons).first #grep one entry to create the race
    #the race info in each record is redundant
    #create the race if it doesn't exist yet
    if rrhash != nil
      race_params=rrhash["Race"] #extract Race infos from hash
      #rc=ImportedRace.find_by(race_params)
      #rc=ImportedRace.find_by(name: rrhash["Race"]["name"], extended_name: rrhash["Race"]["extended_name"], date: rrhash["Race"]["date"])
      
        no_results_in_hash =JSON.parse(rrjsons).length
        no_results_in_file = filename.rpartition('__no').last.split('.json')[0].to_i
        if no_results_in_hash != no_results_in_file
          puts "INFO: #{filename.split('/')[-1]} #{no_results_in_hash.to_s} in hash vs. #{no_results_in_file.to_s} in file"
          exit
        end



      # if rc == nil 
      #   #race does not exist in db -> create it and import results
        
      #   rc=ImportedRace.create(race_params)
      #   no_race_results = JSON.parse(rrjsons).length

      #   bar= ProgressBar.new(no_race_results, :bar, :counter, :eta)
      #   #loop through all result hashes
      #   puts "Currently importing #{no_race_results.to_s} results"
      #   JSON.parse(rrjsons).each do |rrhash|
      #     bar.increment!
      #     if rrhash["start_number"].length < 16 #import only if start_number has a
      #     rc.imported_race_result.create(rrhash.except("Race"))
      #     end
      #   end #do |rrhash|
      
      # else
      #   #race exists already in db
      #   no_results_in_db = rc.imported_race_result.count
      #   no_results_in_file = filename.rpartition('__no').last.split('.json')[0].to_i
      #   if no_results_in_db == no_results_in_file
      #     puts "INFO: #{filename.split('/')[-1]} already in db with same number of results (#{no_results_in_db.to_s})"
      #   elsif no_results_in_db < no_results_in_file
      #     #more results in file than in db
      #     puts "WARNING: more results in file #{filename.split('/')[-1]} (#{no_results_in_file.to_s}) than in db(#{no_results_in_db.to_s})"
      #     idx=1
      #     no_race_results = JSON.parse(rrjsons).length
      #     #loop through all result hashes and import missing ones
      #     JSON.parse(rrjsons).each do |rrhash|
      #       puts "Importing missing race_result #{idx.to_s} of #{no_race_results.to_s}"
      #       idx+=1
      #       rc.imported_race_result.find_or_create_by(rrhash.except("Race"))
      #     end #do |rrhash|
      #   else #no_results_in_db > no_results_in_file
      #     puts "ERROR: more results in db(#{no_results_in_db.to_s}) than in file #{filename} (#{no_results_in_file.to_s})"
      #     exit
      #   end

 
    end #if rrhash == nil  
  else #rrjsons.include?("Race")
    #corrupted file
    puts "WARNING: file #{filename} doesn't contain Race-key"
  end #rrjsons.include?("Race")
end



###########################################################
# Remap content of one json file                          #
###########################################################
def remap_json_file(filename)
 rrjsons_compressed = File.read(filename)
 rrjsons = Zlib.inflate(rrjsons_compressed)
 #rrjsons = ActiveSupport::Gzip.uncompress(rrjsons_compressed)
     
  #Check if this is a parsable file
  if rrjsons.include?("Race")
   rrhash=JSON.parse(rrjsons).first #grep one entry to create the race
   #the race info in each record is redundant
   rc_v3=MyImportedRaceV3.new
   puts "=============================================="
     rc_v3.series =rrhash["Race"]["rcseries"].to_s
    if rrhash["Race"]["rcname"].include?("|")
      (name, extended_name)=rrhash["Race"]["rcname"].split('|',2)
    else
      name = rrhash["Race"]["rcname"].to_s
      extended_name = ""
    end
    rc_v3.name = name[0..63]
    rc_v3.extended_name = extended_name[0..63]
    rc_v3.organizer =rrhash["Race"]["rcorganizer"].to_s
    rc_v3.country =rrhash["Race"]["rccoun"].to_s
    rc_v3.city =rrhash["Race"]["rccity"].to_s
    rc_v3.latitude =rrhash["Race"]["latitude"].to_s
    rc_v3.longitude =rrhash["Race"]["longitude"].to_s
    rc_v3.date =rrhash["Race"]["rcdate"].to_s
    rc_v3.sport_type_id = "" #fixme rrhash["Race"]["rcsporttype"]
    rc_v3.race_type_id = "" #fixme rrhash["Race"]["rcdistance"]
    rc_v3.total_distance = "" #rrhash["Race"]["rcdistance"]
    rc_v3.swim_distance =rrhash["Race"]["rcswmdistance"].to_s
    rc_v3.bike_distance =rrhash["Race"]["rcbikedistance"].to_s
    rc_v3.run_distance =rrhash["Race"]["rcrundistance"].to_s
    rc_v3.import_source_id =rrhash["Race"]["parserid"].to_s
    rc_v3.import_source_url =rrhash["Race"]["rchtml"].to_s
    
    # unused informations:
    # rrhash["Race"]["hasresults"]
    # rrhash["Race"]["writeprotect"]
    # rrhash["Race"]["rceventlistid"]
    # rrhash["Race"]["nochallengers"]
    # rrhash["Race"]["locked"]
    # rrhash["Race"]["distance"] #fixme: migth be used by geocoder.gem
    # rrhash["Race"]["rctableheader"]
    # rrhash["Race"]["rcerrormsg"]


    if rrhash != nil
        #loop through all result hashes
        rrstructs=[]
        no_race_results = JSON.parse(rrjsons).length
        bar= ProgressBar.new(no_race_results, :bar, :counter, :eta)
        JSON.parse(rrjsons).each do |rrhash|
        #  puts "Converting race_result #{idx.to_s} of #{no_race_results.to_s}"
          bar.increment!
          #grep Athlete and Raceresult info if it exists
          if rrhash["Athlete"] != nil
              rr_v3=MyImportedRaceResultV3.new
              rr_v3.imported_race_id = "" #rc_r3.id
              rr_v3.start_number = rrhash["rrstartno"].to_s
              rr_v3.name = rrhash["Athlete"]["atname"].to_s
              rr_v3.surname = rrhash["Athlete"]["atsurname"].to_s
              rr_v3.gender = rrhash["Athlete"]["atgender"].to_s
              rr_v3.birthday = rrhash["Athlete"]["atbirthday"].to_s
              rr_v3.profession = rrhash["Athlete"]["atprofession"].to_s
              rr_v3.club = rrhash["Athlete"]["atclub"].to_s
              rr_v3.country = rrhash["Athlete"]["atadcoun"].to_s
              rr_v3.city = rrhash["Athlete"]["atadcity"].to_s
              rr_v3.latitude = rrhash["Athlete"]["atadlatitude"].to_s
              rr_v3.longitude = rrhash["Athlete"]["atadlatitude"].to_s
              rr_v3.division = rrhash["rrdivision"].to_s
              rr_v3.division_rank = rrhash["rrdivisionrank"].to_s
              rr_v3.division_num_participants = rrhash["rrdivisionparticipants"].to_s
              rr_v3.overall_rank = rrhash["rroverallrank"].to_s
              rr_v3.overall_num_participants = rrhash["rroverallrankparticipants"].to_s
              if rrhash["rroverall"]==nil
                 rrhash["rroverall"]="00:00:00"
	            end
              rr_v3.total_time = ((ChronicDuration.parse(rrhash["rroverall"]).to_i)*100).to_s #in milliseconds
              rr_v3.swim_time = rrhash["rrswim"].to_s
              rr_v3.bike_time = rrhash["rrbike"].to_s
              rr_v3.run_time = rrhash["rrrun"].to_s
              rr_v3.transition1_time = rrhash["rrt1"].to_s
              rr_v3.transition2_time = rrhash["rrt2"].to_s
              rr_v3.import_source_id = "" 
              rr_v3.import_source_url = rrhash["rrlink"].to_s
      
      	     rr_v3["Race"]=rc_v3 #insert race struct into rr
      	     rrstructs << rr_v3
          end  #if rrhash["Athlete"] != nil
        end #do |rrhash|
      rrjsons = rrstructs.to_json #convert array of structs into json-array
      num_results = rrstructs.length
      new_filename = create_race_filename("db/seed3/", rc_v3, num_results)
      write_jsons_to_jsonfile(rrjsons, new_filename, "db/seed3/")
    end #if rrhash != nil  
        
  else #rrjsons.include?("Race")
    puts rrjsons
    puts "WARNING: file #{filename} doesn't contain Race-key"
  end #rrjsons.include?("Race")
end


#######################################################################
# Write json string incl. Race, Athletes and Results to json file     #
#######################################################################
def write_jsons_to_jsonfile(rrjsons, filename, racefilespath)
    new_filename = racefilespath + filename.split("/")[-1] 
        puts "\nWriting: #{new_filename}"
        File.open(new_filename,"w") do |f|
          rrjsons_compressed_utf8 = Zlib.deflate(rrjsons).force_encoding('utf-8')
          f.write(rrjsons_compressed_utf8)
        end
end
 

#############################
# main                      #
#############################
check_all_in_dir("tmp/data/resultimporter/")

