require 'test_helper'

class ImportedRaceResultsControllerTest < ActionController::TestCase
  setup do
    @imported_race_result = imported_race_results(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:imported_race_results)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create imported_race_result" do
    assert_difference('ImportedRaceResult.count') do
      post :create, imported_race_result: { bike_time: @imported_race_result.bike_time, brithday: @imported_race_result.brithday, city: @imported_race_result.city, club: @imported_race_result.club, country: @imported_race_result.country, division: @imported_race_result.division, division_num_participants: @imported_race_result.division_num_participants, division_rank: @imported_race_result.division_rank, gender: @imported_race_result.gender, import_source_id: @imported_race_result.import_source_id, import_source_url: @imported_race_result.import_source_url, imported_race_id: @imported_race_result.imported_race_id, name: @imported_race_result.name, overall_num_participants: @imported_race_result.overall_num_participants, overall_rank: @imported_race_result.overall_rank, profession: @imported_race_result.profession, run_time: @imported_race_result.run_time, start_number: @imported_race_result.start_number, surname: @imported_race_result.surname, swim_time: @imported_race_result.swim_time, total_time: @imported_race_result.total_time, transition1_time: @imported_race_result.transition1_time, transition2_time: @imported_race_result.transition2_time }
    end

    assert_redirected_to imported_race_result_path(assigns(:imported_race_result))
  end

  test "should show imported_race_result" do
    get :show, id: @imported_race_result
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @imported_race_result
    assert_response :success
  end

  test "should update imported_race_result" do
    patch :update, id: @imported_race_result, imported_race_result: { bike_time: @imported_race_result.bike_time, brithday: @imported_race_result.brithday, city: @imported_race_result.city, club: @imported_race_result.club, country: @imported_race_result.country, division: @imported_race_result.division, division_num_participants: @imported_race_result.division_num_participants, division_rank: @imported_race_result.division_rank, gender: @imported_race_result.gender, import_source_id: @imported_race_result.import_source_id, import_source_url: @imported_race_result.import_source_url, imported_race_id: @imported_race_result.imported_race_id, name: @imported_race_result.name, overall_num_participants: @imported_race_result.overall_num_participants, overall_rank: @imported_race_result.overall_rank, profession: @imported_race_result.profession, run_time: @imported_race_result.run_time, start_number: @imported_race_result.start_number, surname: @imported_race_result.surname, swim_time: @imported_race_result.swim_time, total_time: @imported_race_result.total_time, transition1_time: @imported_race_result.transition1_time, transition2_time: @imported_race_result.transition2_time }
    assert_redirected_to imported_race_result_path(assigns(:imported_race_result))
  end

  test "should destroy imported_race_result" do
    assert_difference('ImportedRaceResult.count', -1) do
      delete :destroy, id: @imported_race_result
    end

    assert_redirected_to imported_race_results_path
  end
end
