require 'test_helper'

class ImportSourcesControllerTest < ActionController::TestCase
  setup do
    @import_source = import_sources(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:import_sources)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create import_source" do
    assert_difference('ImportSource.count') do
      post :create, import_source: { description: @import_source.description }
    end

    assert_redirected_to import_source_path(assigns(:import_source))
  end

  test "should show import_source" do
    get :show, id: @import_source
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @import_source
    assert_response :success
  end

  test "should update import_source" do
    patch :update, id: @import_source, import_source: { description: @import_source.description }
    assert_redirected_to import_source_path(assigns(:import_source))
  end

  test "should destroy import_source" do
    assert_difference('ImportSource.count', -1) do
      delete :destroy, id: @import_source
    end

    assert_redirected_to import_sources_path
  end
end
