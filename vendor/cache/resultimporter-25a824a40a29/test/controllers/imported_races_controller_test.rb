require 'test_helper'

class ImportedRacesControllerTest < ActionController::TestCase
  setup do
    @imported_race = imported_races(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:imported_races)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create imported_race" do
    assert_difference('ImportedRace.count') do
      post :create, imported_race: { bike_distance: @imported_race.bike_distance, city: @imported_race.city, country: @imported_race.country, date: @imported_race.date, extended_name: @imported_race.extended_name, import_source_id: @imported_race.import_source_id, import_source_url: @imported_race.import_source_url, latitdude: @imported_race.latitdude, longitude: @imported_race.longitude, name: @imported_race.name, organizer: @imported_race.organizer, race_type_id: @imported_race.race_type_id, run_distance: @imported_race.run_distance, series: @imported_race.series, sport_type_id: @imported_race.sport_type_id, swim_distance: @imported_race.swim_distance, total_distance: @imported_race.total_distance }
    end

    assert_redirected_to imported_race_path(assigns(:imported_race))
  end

  test "should show imported_race" do
    get :show, id: @imported_race
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @imported_race
    assert_response :success
  end

  test "should update imported_race" do
    patch :update, id: @imported_race, imported_race: { bike_distance: @imported_race.bike_distance, city: @imported_race.city, country: @imported_race.country, date: @imported_race.date, extended_name: @imported_race.extended_name, import_source_id: @imported_race.import_source_id, import_source_url: @imported_race.import_source_url, latitdude: @imported_race.latitdude, longitude: @imported_race.longitude, name: @imported_race.name, organizer: @imported_race.organizer, race_type_id: @imported_race.race_type_id, run_distance: @imported_race.run_distance, series: @imported_race.series, sport_type_id: @imported_race.sport_type_id, swim_distance: @imported_race.swim_distance, total_distance: @imported_race.total_distance }
    assert_redirected_to imported_race_path(assigns(:imported_race))
  end

  test "should destroy imported_race" do
    assert_difference('ImportedRace.count', -1) do
      delete :destroy, id: @imported_race
    end

    assert_redirected_to imported_races_path
  end
end
