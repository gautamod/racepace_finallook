module RaceImportExport
extend self

require 'progress_bar'


#######################################################################
# Create a filename including path from race infos                    #
# e.g. [racefilespath]/race__2014-11-hannover-2014__stman-9-km__no562.json   #
#######################################################################
def create_race_filename(racefilespath, rc, num_results)
   rc_date = rc.date.to_time.strftime("%Y-%m-%d")
   racefilespath +"race3__"+rc_date+"__"+rc.name.parameterize+"__"+rc.extended_name.parameterize+"__no"+num_results.to_s+".json"
end


#############################################################################
# Check if race_[rcdate][rcname]_noxxx.json already exists in racefilespath #
# returncodes                                 #
# -2 if multiple files are found (should not happen)            #
# -1 if if no file is found                         #
# 0 if no results = 0                           #
# xxxx if file is found return number of results              #
#############################################################################
def check_if_race_file_exists(racefilespath, rc)
   hasresults=0 # set to 0, will not be used in this case
   filename=Crawlertools.create_race_filename(racefilespath, rc, 0)
   filenames_with_same_name=Dir.glob(filename.split('__no')[0]+"__no*.json")
   if filenames_with_same_name.length == 0
     #file doesn't exist in directory
     returncode = -1
   elsif filenames_with_same_name.length == 1
    #one file found with same racename 
    no_results_in_file = filenames_with_same_name[0].split('__no')[1].split('.json')[0].to_i
     returncode = no_results_in_file
   else # > 1
    puts "\nWARNING: having more than one .json file for one race:"#should never happen
    smallest_no_results=99999999
    biggest_no_results=0
    file_to_delete=""
    file_to_keep=""
    filenames_with_same_name.each do |fn|
      rrjsons_compressed = File.read(fn)
      rrjsons = Zlib.inflate(rrjsons_compressed)
      rrhash=JSON.parse(rrjsons)
      puts "WARNING: #{fn} has #{rrhash.length}"
      if rrhash.length < smallest_no_results
        smallest_no_results = rrhash.length
        file_to_delete=fn
      end
      if rrhash.length > biggest_no_results
        biggest_no_results = rrhash.length
        file_to_keep =fn
      end
    end
    puts "WARNING: deleting file with less results: #{file_to_delete}"
    FileUtils.rm(file_to_delete) 
    returncode = file_to_keep.split('__no')[1].split('.json')[0].to_i
  end
  [returncode,filenames_with_same_name]
end

##################################################################
# Import all racefiles.json incl. Athletes and Results from dir  #
##################################################################
def import_all_from_dir(racefilespath)
  #Loop through all .json files 
  num_race_results = Dir.glob(racefilespath+"race3_*.json").length
  if num_race_results < 1 
    puts "No files in #{racefilespath} found to import"
  else
    bar1= ProgressBar.new(num_race_results, :bar, :counter, :eta)
    Dir.glob(racefilespath+"race3_*.json").each do |filename|
      puts " "
      puts "Importing: #{filename.split('/')[-1]}"
      puts "Importing #{num_race_results.to_s} files:"
      bar1.increment!
      import_race_from_file(filename)
    end
  end
end

#############################################################
# Import one race incl. Athletes and Results from json file #
#############################################################
def import_race_from_file(filename)
 rrjsons_compressed = File.read(filename)
 rrjsons = Zlib.inflate(rrjsons_compressed)
 #rrjsons = ActiveSupport::Gzip.uncompress(rrjsons_compressed)
     
  #Check if this is a parsable file
  rrhash=JSON.parse(rrjsons).first #grep one entry to create the race
    
  #this check allows importing jsons which indicates the race table name as "Race" or "imported_race"  
  if rrhash.keys.include?("imported_race")
    race_table_name="imported_race"
  elsif rrhash.keys.include?("Race")
    race_table_name="Race"
  else
    race_table_name="not_supported"
  end
  
  if race_table_name!="not_supported" #rrjsons.include?("Race")
    #the race info in each record is redundant
    #create the race if it doesn't exist yet
    if rrhash != nil
      #limit length of url to 255
      rrhash["import_source_url"]=rrhash["import_source_url"].to_s[0..254]
      rrhash[race_table_name]["import_source_url"]=rrhash[race_table_name]["import_source_url"].to_s[0..254]
      race_params=rrhash[race_table_name] #extract Race infos from hash
      rc=ImportedRace.find_by(name: rrhash[race_table_name]["name"], extended_name: rrhash[race_table_name]["extended_name"], date: rrhash[race_table_name]["date"])

      if rc == nil 
        #race does not exist in db -> create it and import results
        
        rc=ImportedRace.create(race_params)
        no_race_results = JSON.parse(rrjsons).length

        bar= ProgressBar.new(no_race_results, :bar, :counter, :eta)
        #loop through all result hashes
        puts "Currently importing #{no_race_results.to_s} results"

        #use batch processing to save mem (huge races e.g. berlin marathon > 30.000 results)
        JSON.parse(rrjsons).in_groups_of(1000, false) do |rrhash_batch|
          rrhash_batch.each do |rrhash|
            bar.increment!
            if rrhash["start_number"].to_s.length < 16 #import only if start_number has a
      		    rc.imported_race_result.create(rrhash.except(race_table_name))
      	    end
          end #do |rrhash|
        end
      
      else
        #race exists already in db
        no_results_in_db = rc.imported_race_result.count
        no_results_in_file = filename.rpartition('__no').last.split('.json')[0].to_i
        if no_results_in_db == no_results_in_file
          puts "INFO: #{filename.split('/')[-1]} already in db with same number of results (#{no_results_in_db.to_s})"
        elsif no_results_in_db < no_results_in_file
          #more results in file than in db
          puts "WARNING: more results in file #{filename.split('/')[-1]} (#{no_results_in_file.to_s}) than in db(#{no_results_in_db.to_s})"
          idx=1
          no_race_results = JSON.parse(rrjsons).length
          #loop through all result hashes and import missing ones
          JSON.parse(rrjsons).each do |rrhash|
            puts "Importing missing race_result #{idx.to_s} of #{no_race_results.to_s}"
            idx+=1
            rc.imported_race_result.find_or_create_by(rrhash.except(race_table_name))
          end #do |rrhash|
        else #no_results_in_db > no_results_in_file
          puts "Warning: more results in db(#{no_results_in_db.to_s}) than in file #{filename} (#{no_results_in_file.to_s})"
	  puts "Warning: delete current race in database (2nd import run required!)"
	  rc.destroy
        end

      end 

    end #if rrhash == nil  
  else #rrjsons.include?("Race")
    #corrupted file
    puts "WARNING: file #{filename} doesn't contain json-key Race or imported_race"
  end #rrjsons.include?("Race")
end

#######################################################################
# Write json string incl. Race, Athletes and Results to json file     #
#######################################################################
def write_jsons_to_jsonfile(rrjsons, rc, racefilespath)
    no_new_results=JSON.parse(rrjsons).size
    new_filename = create_race_filename(racefilespath, rc.rcname, rc.rcdate.to_time, no_new_results)
    filenames_with_same_name=Dir.glob(new_filename.split('__no')[0]+"__no*.json")
    if filenames_with_same_name.length == 0
        #file doesn't exist in directory -> go and export it
        puts "Writing: #{new_filename}"
        File.open(new_filename,"w") do |f|
          rrjsons_compressed_utf8 = Zlib.deflate(rrjsons).force_encoding('utf-8')
          f.write(rrjsons_compressed_utf8)
        end
    elsif filenames_with_same_name.length == 1
        #one file found with same racename (but possibly with different no results)
        no_results_in_existing_file = filenames_with_same_name[0].split('__no')[1].split('.json')[0].to_i
        if no_results_in_existing_file == no_results_in_db
          #File exists with same number of results -> nothing to do
          print(".")
        elsif no_results_in_existing_file < no_new_results
          #File exists but have more new results -> delete old one and export new one
          puts "\nUpdating: #{new_filename}"
          FileUtils.rm(filenames_with_same_name[0])
          File.open(new_filename,"w") do |f|
            rrjsons_compressed_utf8 = Zlib.deflate(rrjsons).force_encoding('utf-8')
            f.write(rrjsons_compressed_utf8)
          end
        else
          #File exisits but have fewer new results than in existing file
          puts "\nWARNING: more results in file(#{no_results_in_file.to_s}) than currently fetched(#{no_new_results.to_s}) #{new_filename}"
        end
    else # > 1
      puts "\nWARNING: having more than one .json file for one race:"#should never happen
      filenames_with_same_name.each do |fn|
      puts "-------: #{fn}"
      end
    end
    new_filename
end
 
##########################################################################
# Export all races incl. Athletes and Results from database into dir     #
##########################################################################
def export_all_to_dir(racefilespath)
  #Loop through all races in the database
  num_races = ImportedRace.all.count
  bar1= ProgressBar.new(num_races, :bar, :counter, :eta)
  ImportedRace.all.each do |rc|
     export_race_from_db_to_file(rc, racefilespath)
     bar1.increment!
  end
end


###############################################################
# Export one race incl. Athletes and Results to json file     #
###############################################################
def export_race_from_db_to_file(rc, racefilespath)
      #Check if hasresults number is correct
      no_raceresults_in_db = ImportedRaceResult.where("imported_race_id = ?",rc.id).count
      
      #Create a filename base on race infos
      FileUtils.mkdir_p(racefilespath+"export/") unless File.directory?(racefilespath+"export/")
      new_filename = create_race_filename(racefilespath+"export/", rc, no_raceresults_in_db)
      #Check if the race has already be exported (ignore the number of results)
       filenames_with_same_name=Dir.glob(new_filename.split('__no')[0]+"__no*.json")
       if filenames_with_same_name.length == 0
        #file doesn't exist in directory -> go and export it
        puts "\nExporting: #{new_filename}"
        do_export(rc,new_filename)
       elsif filenames_with_same_name.length == 1
        #one file found with same racename (but possibly with different no results)
          #File exists -> delete old one and export new one
          puts "\nUpdating: #{new_filename}"
          FileUtils.rm(filenames_with_same_name[0])
          do_export(rc,new_filename) 
       else # > 1
        puts "\nERROR: having more than one .json file for one race:"#should never happen
        filenames_with_same_name.each do |fn|
        puts "ERROR: #{fn}"
        exit   
     end
  end
end

def do_export(rc, filename)
  if ImportedRaceResult.where("imported_race_id = ?",rc.id).count == 0
    #if the race has no results then export only the race information
    rrjsons=ImportedRace.where("id = ?",rc.id).to_json(except: [:id, :created_at, :updated_at], root: "Race")
  else
    #rrjsons=Raceresult.where("Race_id = ?",rc.id).to_json(include: { Athlete: {only: [:atname, :atsurname, :atclub]}, Race: {only: [:rcname, :rcdate, :hasresults]} } , except: [:id,
    rrjsons=ImportedRaceResult.where("imported_race_id = ?",rc.id).to_json(include: 
      { imported_race: {except: [:id, :created_at, :updated_at]} } , 
      except: [:id, :created_at, :updated_at], root: false)
  end
  File.open(filename,"w") do |f|
      rrjsons_compressed_utf8 = Zlib.deflate(rrjsons).force_encoding('utf-8')
      f.write(rrjsons_compressed_utf8)
  end
end




end #module
