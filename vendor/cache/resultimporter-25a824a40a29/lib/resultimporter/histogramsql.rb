module Histogramsql
extend self
##
# Get an array with counts of records for each range of values.
# The +table+ argument is the name of the database table to query and
# +value+ is an SQL statement for the value to calculate for each record.
#
def hist(table, expr, options = {})
	options[:min] ||= minimum_value(table, expr, options)
	options[:max] ||= maximum_value(table, expr, options)
	if options[:max] == 0
		options[:max] =1
	end
	options[:bands] ||= 500
	if options[:bands] == 0
		options[:bands] =1
	end
	options[:conditions] ||= nil
	inc = (options[:max].to_f - options[:min].to_f) / options[:bands].to_f
	res = ActiveRecord::Base.connection.execute(
	"SELECT band, COUNT(*) AS count FROM (" +
	"SELECT FLOOR(" +
	"(#{expr} - #{options[:min].to_f}) / #{inc}" +
	") AS band FROM #{table}" +
	((c = options[:conditions]) ? " WHERE #{c}" : "") +
	") bands " +
	"GROUP BY band ORDER BY band"
	)

    if res.first.nil? #strange variant to check if result is empty
    	bins=[0]
    	res=[0]
    else
		i=1;
    	res=([0] * options[:bands]).tap do |bands|
		case adapter = ActiveRecord::Base.connection.adapter_name
			when "PostgreSQL"
				[inc * i, res.each{ |i| bands[i["band"].to_i] = i["count"].to_i }]
			when "Mysql2"
				begin
					[inc * i, res.each{ |i| bands[i[0]] = i[1] }]
				rescue
					#raise StandardError, "Error res=nil, maybe to few or no data"
					[inc * i,0]
				end
			else
				raise StandardError, "Database adapter '#{adapter}' not supported"
			end
		end

		#build bin array
		bins=[]
		i=1
		res.each do |histval|
			bins << inc * i + options[:min].to_f
			i=i+1
		end
	end
	[bins,res]
end

##########################################
#normalized version of hist              #
##########################################
def hist_norm(table, expr, options = {})
	(bins,res)=hist(table, expr, options = {})
	res_normed=[]
	total = res.inject{|sum,x| sum + x}
	res.each do |histval|
		res_normed << (histval / total.to_f)
    end
    [bins,res_normed]
end



##
# Get the minimum value from the table.
#
def minimum_value(table, expr, options = {})
 min_val=min_or_max_value(table, expr, false, options)
 if min_val.nil?
 	min_val=0
 end
 min_val
end

##
# Get the maximum value from the table.
#
def maximum_value(table, expr,options = {})
max_val=min_or_max_value(table, expr, true, options)
if max_val.nil?
 	max_val=1
 end
 max_val
end

private # -----------------------------------------------------------------
##
# Get the min or max value from the table.
#
def min_or_max_value(table, expr, max = false,options = {})
	options[:conditions] ||= nil
	res = ActiveRecord::Base.connection.execute(
	"SELECT #{max ? 'MAX' : 'MIN'}(#{expr}) FROM #{table}"+ ((c = options[:conditions]) ? " WHERE #{c}" : "")
	)
	case adapter = ActiveRecord::Base.connection.adapter_name
		when "PostgreSQL"
		res.first.first[1].to_f
		when "Mysql2"
		res.first[0]
	else
		raise StandardError, "Database adapter '#{adapter}' not supported"
	end
end

end
