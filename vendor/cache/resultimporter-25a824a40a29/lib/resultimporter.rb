require "resultimporter/engine"
require "geocoder"
require 'best_in_place'
require 'wice_grid'
require 'histogram'
require 'ratyrate'
require 'gchart'
require 'mysql2'
require 'resultimporter/import_export_race'
require 'resultimporter/histogramsql'
require 'chronic_duration'
require 'configatron'
require 'progress_bar'
require 'secondbase'


Dir["tasks/**/*.rake"].each { |ext| load ext } if defined?(Rake)
#load 'tasks/resultimporter.rake' if defined?(Rake)


module Resultimporter

 def self.sayhello
    "Hello"
 end


    ##########################################################
    #  Check races times according to distance if the race   #
    #  has been finished and set finished flag accordingly   #
    #  Examples/Rules:                                       #
    #  total_time for a LD race < 7h30m -> finished=false    #
    #  bike_time for a LD race < 4h00m -> finished=false     #
    #  run_time for a LD race < 2h30m -> finished=false      #
    ##########################################################
    def self.set_triathlon_finished_flag
      races= ImportedRace.where("sport_type_id = ?",SportType.where("description=?","Triathlon").first.id)
      num=races.length
      idx=0
      races.each do |rc|
        num_dnf=0 #number of did-not-finished
        idx+=1
        unless rc.race_type.nil?
          if rc.race_type.description == "Longdistance" #this is an full-distance triathlon (LD)
            #loop through all the results of this race
            #use batch processing to save mem (huge races e.g. berlin marathon > 30.000 results)
            rc.imported_race_result.find_in_batches(batch_size: 1000) do |rr_batch|
              rr_batch.each do |rr|
                if rr.total_time < 7.5*3600*100 #less than 7.5hrs
                  rr.update(finished: false)
                  num_dnf+=1
                elsif rr.bike_time < 4.0*3600*100 #less than 4hrs
                  rr.update(finished: false)
                  num_dnf+=1
                elsif rr.run_time < 2.5*3600*100 #less than 2.5hrs     
                  rr.update(finished: false)
                  num_dnf+=1
                end

                # if the race was not finished set all vdot's to 0
                if rr.finished == false
                  rr.update(tritotaltime_vdot: 0, triswim_vdot: 0, tribike_vdot: 0, trirun_vdot: 0, transition1_vdot: 0, transition2_vdot: 0)
                end  

              end
            end 
          
          elsif rc.race_type.description == "Halfdistance" #this is a half-distance triathlon (MD)    
            #use batch processing to save mem (huge races e.g. berlin marathon > 30.000 results)
            rc.imported_race_result.find_in_batches(batch_size: 1000) do |rr_batch|
              rr_batch.each do |rr|
                if rr.total_time < 3.5*3600*100 #less than 3.5hrs
                  rr.update(finished: false)
                  num_dnf+=1
                elsif rr.bike_time < 1.5*3600*100 #less than 1.5hrs
                  rr.update(finished: false)
                  num_dnf+=1
                elsif rr.run_time < 1.0*3600*100 #less than 1.0hrs     
                  rr.update(finished: false)
                  num_dnf+=1
                end

                # if the race was not finished set all vdot's to 0
                if rr.finished == false
                  rr.update(tritotaltime_vdot: 0, triswim_vdot: 0, tribike_vdot: 0, trirun_vdot: 0, transition1_vdot: 0, transition2_vdot: 0)
                end  
             
              end
            end      
          end 

               
        end #unless
        num_results=rc.imported_race_result.count
        puts "Race #{idx}/#{num}: #{num_dnf.to_s} of #{num_results.to_s} results set to DNF: #{(num_dnf/num_results.to_f*100).round.to_s}% (#{rc.name})"
      end #races.each do
    end


    ###########################################################
    #  Calculate Jack Daniels vdot                            #
    #  distance in meters as int eg. 42195 for Marathon #
    #  duration in seconds: eg: 3x3600 for 3:00:00            #
    ###########################################################
    def self.calc_vdot(distance, duration)
       checked_distance=nil
       #check if duration is not 0 to avoid div by 0 -> NaN
       if duration.nil?
          vdot=-1
       elsif duration > 0
           pvo2max=calc_pvo2max(duration)
         excel_duration=duration/(24*3600).to_f #xls time format = Minutes of a day: 23:59:00 = 1339,0 
          vdot=(-4.6+0.182258*(distance/excel_duration/1440)+0.000104*(distance/excel_duration/1440)**2)/pvo2max
          if vdot < 0 #happens if distance is too small or time too big
            vdot = -1 #
          elsif vdot > 100 #happen if time is too short
          	vdot = -2
          end
        #duration == 0
        else
           vdot=-1
        end
        vdot
    end 

    ######################################################
    #  Calculate VO2Max in percent                        #
    #  duration in seconds: eg: 3x3600 for 3:00:00        #
    #######################################################
    def self.calc_pvo2max(duration)
       excel_duration=duration/(24*3600).to_f #xls time format = Minutes of a day: 23:59:00 = 1339,0
       pvo2max=0.8+0.1894393 * Math.exp(-0.012778*excel_duration*1440)+0.2989558* Math.exp(-0.1932605*duration*1440)
    end


    ###########################################################
    #  Calculate Triathlon total-time VDOT -> tdot            #
    #  vdot is for run's and tdot is for triathlon            #
    #  distance in meters as int eg. 42195 for LD             #
    #  swim*4 + bike/3 + run + 1000 + 1000                    #
    #  duration in sec: 3x3600 for discipline 3:00:00         #
    #  total_duration in sec: 10x3600 for total_tri 10:00:00  #
    ###########################################################
    def self.calc_tdot(distance, duration, total_duration)
       checked_distance=nil
       #check if duration is not 0 to avoid div by 0 -> NaN
       if duration.nil?
          tdot=-1
       elsif duration > 0
          tri_pvo2max=tri_calc_pvo2max(total_duration)
          excel_duration=duration/(24*3600).to_f #xls time format = Minutes of a day: 23:59:00 = 1339,0 
          tdot=(-4.6+0.182258*(distance/excel_duration/1440)+0.000104*(distance/excel_duration/1440)**2)/tri_pvo2max
          if tdot < 0 #happens if distance is too small or time too big
            tdot = -1 #
          elsif tdot > 100 #happen if time is too short
            tdot = -2
          end
        #duration == 0
        else
           tdot=-1
        end
        tdot
    end 

    ###########################################################
    #  Calculate Triathlon Total-Time VDOT -> tridot          #
    #  swim_distance in meters as int eg. 3800 for LD         #
    #  bike_distance in meters as int eg. 180000 for LD       #
    #  run_distance in meters as int eg. 42195 for LD         #
    #  total_time in sec: eg. 12x3600                         #
    ###########################################################
    def self.calc_tridot(swim_distance, bike_distance, run_distance, total_time)
      #use bike_distance get b_val correction-factor
      case bike_distance.to_i # in meters 
      when 0 .. 30000  # 0 to (20000+40000)/2  must be sprint-distance
          b_val =  3.25 #fixme: value not yet checked
      when 30000 .. 65000 # (20000+40000)/2 to (40000+90000)/2 must be olympic-distance
          b_val =  3.10 #fixme: value not yet checked
      when 65000 .. 135000 #(40000+90000)/2 to (90000+180000)/2 must be half-distance
          b_val =  -20.078*total_time/24/3600.to_f*total_time/24/3600.to_f + 13.537*total_time/24/3600.to_f + 0.8801 # was 2.95
      when 135000 .. 200000  #(90000+180000)/2 to 200000 must be full-distance
          b_val = -8.6556*total_time/24/3600.to_f*total_time/24/3600.to_f + 10.824*total_time/24/3600.to_f - 0.3819  # was 2.8
      else 
          b_val = -8.6556*total_time/24/3600.to_f*total_time/24/3600.to_f + 10.824*total_time/24/3600.to_f - 0.3819  # was 2.8
      end  
     Resultimporter.calc_tdot(swim_distance*get_sdot_correction_value(swim_distance)+1000+bike_distance/b_val+1000+run_distance*get_rdot_correction_value(run_distance),total_time.to_f,total_time)
    end 

    ###########################################################
    #  Calculate Triathlon Swim VDOT -> sdot                  #
    #  distance in meters as int eg. 3800 for LD             #
    #  duration in sec: 1x3600 for discipline 1:00:00         #
    #  total_duration in sec: 10x3600 for total_tri 10:00:00  #
    ###########################################################
    def self.calc_sdot(swim_distance, swim_time , total_time)
       Resultimporter.calc_tdot(swim_distance*get_sdot_correction_value(swim_distance), swim_time, total_time)
    end 

    ###########################################################
    #  Get correction_value for sdot depending on distance    #
    #  swim_distance in meters as int eg. 3800 for LD         #
    ###########################################################
    def self.get_sdot_correction_value(swim_distance)
     case swim_distance.to_i # in meters 
      when 0 .. 750    # 0 to (500+1000)/2  must be sprint-distance
          s_val =  3.9 #fixme: value not yet checked
      when 750 .. 1700 # (500+1000)/2 to (1500+1900)/2 must be olympic-distance
          s_val =  3.7 #fixme: value not yet checked
      when 1700 .. 3150 #(1500+1900)/2 to (2500+3800)/2 must be half-distance
          s_val =  3.5
      when 3150 .. 10000  #(2500+3800)/2 to 10000 must be full-distance
          s_val =  3.3
      else 
          s_val =  3.3
      end  
      return s_val
    end 

    ###########################################################
    #  Calculate Triathlon Bike VDOT -> bdot                  #
    #  distance in meters as int eg. 180000 for LD             #
    #  bike_time in sec: 6x3600 for discipline 6:00:00         #
    #  total_duration in sec: 10x3600 for total_tri 10:00:00  #
    ###########################################################
    def self.calc_bdot(bike_distance, bike_time , total_time)
      Resultimporter.calc_tdot(bike_distance/get_bdot_correction_value(bike_distance,bike_time), bike_time, total_time)
    end 

    ###########################################################
    #  Get correction_value for bdot depending on distance    #
    #  bike_distance in meters as int eg. 180000 for LD       #
    #  bike_time in sec: 6x3600 for discipline 6:00:00        #
    ###########################################################
    def self.get_bdot_correction_value(bike_distance, bike_time)
     case bike_distance.to_i # in meters 
      when 0 .. 30000  # 0 to (20000+40000)/2  must be sprint-distance
          b_val =  3.25 #fixme: value not yet checked
      when 30000 .. 65000 # (20000+40000)/2 to (40000+90000)/2 must be olympic-distance
          b_val =  3.10 #fixme: value not yet checked
      when 65000 .. 135000 #(40000+90000)/2 to (90000+180000)/2 must be half-distance
          b_val =  -106.51*bike_time/24/3600.to_f*bike_time/24/3600.to_f + 35.02*bike_time/24/3600.to_f + 0.2902 # was 2.95
      when 135000 .. 200000  #(90000+180000)/2 to 200000 must be full-distance
          b_val = -45.445*bike_time/24/3600.to_f*bike_time/24/3600.to_f + 28.064*bike_time/24/3600.to_f - 1.3156  # was 2.8
      else 
          b_val =  -45.445*bike_time/24/3600.to_f*bike_time/24/3600.to_f + 28.064*bike_time/24/3600.to_f - 1.3156  # was 2.8
      end  
      return b_val
    end 

    ###########################################################
    #  Calculate Triathlon Run VDOT -> rdot                  #
    #  distance in meters as int eg. 42195 for LD             #
    #  duration in sec: 4x3600 for discipline 4:00:00         #
    #  total_duration in sec: 10x3600 for total_tri 10:00:00  #
    ###########################################################
    def self.calc_rdot(run_distance, run_time , total_time)
       Resultimporter.calc_tdot(run_distance*get_rdot_correction_value(run_distance), run_time, total_time)
    end 

    ###########################################################
    #  Get correction_value for rdot depending on distance    #
    #  run_distance in meters as int eg. 42195 for LD         #
    ###########################################################
    def self.get_rdot_correction_value(run_distance)
      case run_distance.to_i # in meters 
      when 0 .. 7500  # 0 to (5000+10000)/2  must be sprint-distance
          r_val =  1.0 #fixme: value not yet checked
      when 7500 .. 15550 # (5000+10000)/2 to (10000+21100)/2 must be olympic-distance
          r_val =  1.0 #fixme: value not yet checked
      when 15550 .. 31647 #(10000+21100)/2 to (21100+42195)/2 must be half-distance
          r_val =  1.0
      when 31647 .. 50000  #(21100+42195)/2 to 50000 must be full-distance
          r_val =  1.15
      else 
          r_val =  1.15
      end  
      return r_val
    end 

    ###########################################################
    #  Calculate Triathlon Transtion1 VDOT -> t1dot           #
    #  t1 duration in sec: 4x60 for discipline 0:40:00        #
    #  total_time in sec: 10x3600 for total_tri 10:00:00  #
    ###########################################################
    def self.calc_t1dot(t1_distance, t1_time , total_time)
       Resultimporter.calc_tdot(t1_distance*get_t1_correction_value(t1_time, total_time), t1_time, total_time)
    end 

    ###########################################################
    #  Get correction_value for t1dot depending on distance    #
    #  total_time in sec: 10x3600 for total_tri 10:00:00  #
    ###########################################################
    def self.get_t1_correction_value(t1_time, total_time)
      case total_time.to_i # in sec
      when 0 .. 7*3600+45*60 # 0 to 7h45 should be less than a full-distance e.g. half-distance
          t1_val = -11539*t1_time/24/3600.to_f*t1_time/24/3600.to_f + 235.46*t1_time/24/3600.to_f + 0.13861
      when 7*3600+45*60 .. 20*3600 # 7h45 to 20h might be a full-distance
          t1_val =  -7856.1*t1_time/24/3600.to_f*t1_time/24/3600.to_f + 243.27*t1_time/24/3600.to_f + 0.1885
      else 
          t1_val =  -7856.1*t1_time/24/3600.to_f*t1_time/24/3600.to_f + 243.27*t1_time/24/3600.to_f + 0.1885
      end  
      return t1_val
    end 
    
     ###########################################################
    #  Calculate Triathlon Transtion2 VDOT -> t1dot           #
    #  t2 duration in sec: 4x60 for discipline 0:40:00        #
    #  total_time in sec: 10x3600 for total_tri 10:00:00  #
    ###########################################################
    def self.calc_t2dot(t2_distance, t2_time , total_time)
       Resultimporter.calc_tdot(t2_distance*get_t2_correction_value(t2_time, total_time), t2_time, total_time)
    end 

    ###########################################################
    #  Get correction_value for t2dot depending on distance    #
    #  total_time in sec: 10x3600 for total_tri 10:00:00  #
    ###########################################################
    def self.get_t2_correction_value(t2_time, total_time)
      case total_time.to_i # in sec
      when 0 .. 7*3600+45*60 # 0 to 7h45 should be less than a full-distance e.g. half-distance
          t2_val = -11205*t2_time/24/3600.to_f*t2_time/24/3600.to_f + 223.63*t2_time/24/3600.to_f + 0.0981
      when 7*3600+45*60 .. 20*3600 # 7h45 to 20h might be a full-distance
          t2_val =  -7575.2*t2_time/24/3600.to_f*t2_time/24/3600.to_f + 246.54*t2_time/24/3600.to_f+ 0.0983
      else 
          t2_val =  -7575.2*t2_time/24/3600.to_f*t2_time/24/3600.to_f + 246.54*t2_time/24/3600.to_f+ 0.0983
      end  
      return t2_val
    end 
    

    ######################################################
    #  Calculate VO2Max in percent                       #
    #  duration in seconds: eg: 3x3600 for 3:00:00       #
    ######################################################
    def self.tri_calc_pvo2max(duration)
       excel_duration=duration/(24*3600).to_f #xls time format = Minutes of a day: 23:59:00 = 1339,0
       pvo2max=0.8+0.1894393 * Math.exp(-0.012778*excel_duration*1440)+0.2989558* Math.exp(-0.1932605*duration*1440)
    end

    


    ######################################################
    #  Calculate vdot for triathlon races                #
    ######################################################
    def self.calc_vdot_triathlon
      races= ImportedRace.where("sport_type_id = ?",SportType.where("description=?","Triathlon").first.id)
      num=races.length
      idx=0
      races.each do |rc|
      idx+=1
        unless rc.race_type.nil?
          if rc.race_type.description == "Longdistance" #this is an full-distance triathlon (LD)
            #loop through all the results of this race
            #use batch processing to save mem (huge races e.g. berlin marathon > 30.000 results)
            rc.imported_race_result.find_in_batches(batch_size: 1000) do |rr_batch|
              rr_batch.each do |rr|
                if rr.finished #only calc vdots if race has been finished
                  sw_vd = Resultimporter.calc_sdot(3800,rr.swim_time/100,rr.total_time/100)
                  t1_vd = Resultimporter.calc_t1dot(1000,rr.transition1_time/100,rr.total_time/100) #fixme: 1700 might be more accurate
                  bk_vd = Resultimporter.calc_bdot(180000,rr.bike_time/100,rr.total_time/100)
                  t2_vd = Resultimporter.calc_t2dot(1000,rr.transition2_time/100,rr.total_time/100) #fixme: 1600 might be more accurate
                  rn_vd = Resultimporter.calc_rdot(42195,rr.run_time/100,rr.total_time/100) 
                  tt_vd = Resultimporter.calc_tridot(3800,180000,42195,rr.total_time/100)
                  rr.update(tritotaltime_vdot: tt_vd, triswim_vdot: sw_vd, tribike_vdot: bk_vd, trirun_vdot: rn_vd, transition1_vdot: t1_vd, transition2_vdot: t2_vd)
                else
                  rr.update(tritotaltime_vdot: 0, triswim_vdot: 0, tribike_vdot: 0, trirun_vdot: 0, transition1_vdot: 0, transition2_vdot: 0)
                end
              end
            end
          
          elsif rc.race_type.description == "Halfdistance" #this is a half-distance triathlon (MD)    
            #use batch processing to save mem (huge races e.g. berlin marathon > 30.000 results)
            rc.imported_race_result.find_in_batches(batch_size: 1000) do |rr_batch|
              rr_batch.each do |rr|
                if rr.finished #only update vdots if race has been finished
                  sw_vd = Resultimporter.calc_sdot(1900,rr.swim_time/100,rr.total_time/100)
                  t1_vd = Resultimporter.calc_t1dot(1000,rr.transition1_time/100,rr.total_time/100)
                  bk_vd = Resultimporter.calc_bdot(90000,rr.bike_time/100,rr.total_time/100)
                  t2_vd = Resultimporter.calc_t2dot(1000,rr.transition2_time/100,rr.total_time/100) #fixme: 900 might be more accurate
                  rn_vd = Resultimporter.calc_rdot(21100,rr.run_time/100,rr.total_time/100)
                  tt_vd = Resultimporter.calc_tridot(1900, 90000, 21100,rr.total_time/100)
                  rr.update(tritotaltime_vdot: tt_vd, triswim_vdot: sw_vd, tribike_vdot: bk_vd, trirun_vdot: rn_vd, transition1_vdot: t1_vd, transition2_vdot: t2_vd)
                else
                  rr.update(tritotaltime_vdot: 0, triswim_vdot: 0, tribike_vdot: 0, trirun_vdot: 0, transition1_vdot: 0, transition2_vdot: 0)
                end
              end
            end         
          else
            puts "INFO: not calulating vdot for race_type #{rc.race_type.description} #{rc.name}"
          end
          puts "(#{idx}/#{num}) VDOT calcualted for #{rc.name}"
        end  
      end
    end

    ####################################################
    #  Calculate vdot for run races                    #
    ####################################################
    def self.calc_vdot_running
    #find all run races which have a total_distance>0
    races= ImportedRace.where("sport_type_id = ? and total_distance > 0",SportType.where("description=?","Run").first.id)
    num=races.length
    idx=0
    races.each do |rc|
      idx+=1
      #loop through all the results of this race
      #use batch processing to save mem (huge races e.g. berlin marathon > 30.000 results)
      rc.imported_race_result.find_in_batches(batch_size: 1000) do |rr_batch|
        rr_batch.each do |rr|
          rr.update(vdot: Resultimporter.calc_vdot(rc.total_distance,rr.total_time.to_f/100))
        end
      end
    
      vdot_std=rc.imported_race_result.pluck(:vdot).standard_deviation.round(2)
      vdot_avg=rc.imported_race_result.pluck(:vdot).mean.round(2)
      total_time_vdot_stat="#{vdot_avg.round(1).to_s.rjust(4,' ')}(#{vdot_std.round(0).to_s.rjust(2,' ')})"
      puts "(#{idx}/#{num}) MEAN(STD) | #{total_time_vdot_stat}  #{rc.name}"
      end
    end
  
  ###################################################
  # Normalize Histogramm data                       #
  # in: [[0, 10], [5, 230], [10, 560], [15, 330]]   #
  # out:[[0, 1], [5, 20], [10, 50], [15, 29]]       #
  ###################################################
  def self.normalize_histogram(hist_bins_and_freqs)
    freqs = []
    bins = []
    hist_bins_and_freqs.each do |bin, freq|
        bins << bin
        freqs << freq
    end
    norm_hist_bins_and_freqs = []
    freqs.each_with_index do |freq, idx|
        norm_hist_bins_and_freqs <<  [bins[idx], (freq/freqs.sum*100).round(1)]
    end
    return norm_hist_bins_and_freqs
  end

end
