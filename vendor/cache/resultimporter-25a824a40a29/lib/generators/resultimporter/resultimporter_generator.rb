class ResultimporterGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)


	def copy_testjsonresult_file
	    copy_file "race3__2015-08-23__40-sengbach-talsperrenlauf__halbmarathon-21-1km__no228.json", configatron.resultcrawler_path + "race3__2015-08-23__40-sengbach-talsperrenlauf__halbmarathon-21-1km__no228.json"
	    copy_file "race3__2015-08-23__7-sportscheck-stadtlauf-berlin-2015__21km-lauf__no2684.json", configatron.resultcrawler_path + "race3__2015-08-23__7-sportscheck-stadtlauf-berlin-2015__21km-lauf__no2684.json"
	    copy_file "race3__2015-08-23__gochness-triathlon-2015__jedermanndistanz-0-5-20-5__no308.json", configatron.resultcrawler_path + "race3__2015-08-23__gochness-triathlon-2015__jedermanndistanz-0-5-20-5__no308.json"
	 end

#  def run_migrations
#    rake "db:migrate"
#  end


end
