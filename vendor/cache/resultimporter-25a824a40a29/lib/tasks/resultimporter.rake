namespace :resultimporter do
  desc "Import json files into imported_races and imported_race_results tables"
  task import_races: :environment do
   RaceImportExport.import_all_from_dir(configatron.resultcrawler_path)
   puts "import_all_races_from_dir DONE"
  end

  desc "Export imported_races and imported_race_results tables(database content) into json files"
  task export_races: :environment do
    RaceImportExport.export_all_to_dir(configatron.resultcrawler_path)
    puts "export_all_races_to_dir DONE"
  end

  desc "Check if Triathlon race has been finished. Set finished flag and set vdots=0 if needed"
  task set_triathlon_finished_flag: :environment do
    Resultimporter.set_triathlon_finished_flag
    puts "setting triathlon finished flag DONE"
  end

  desc "Calculate vdot values for each imported_race_result and fill vdot columns"
  task calc_vdots: :environment do
    Resultimporter.calc_vdot_triathlon
    Resultimporter.calc_vdot_running
    puts "calculating vdot values DONE"
  end
 
  desc "Calculate triathlon vdot values for each imported_race_result and fill vdot columns"
  task calc_vdots_triathlon: :environment do
    Resultimporter.calc_vdot_triathlon
    puts "calculating triathlon vdot values DONE"
  end

 desc "Calculate running vdot values for each imported_race_result and fill vdot columns"
  task calc_vdots_running: :environment do
    Resultimporter.calc_vdot_running
    puts "calculating running vdot values DONE"
 end
   

  desc "Try to automatically fill sport_type, race_type, total_distance columns"
  task autofill_race_properties: :environment do
   
    #Matching Marathon runs by extended name
    ImportedRace.where("extended_name LIKE ?","Marathon").each do |rc| #will match "Marathon" and "marathon"
      rc.update(sport_type_id: SportType.where("description = ?","Run").first.id)
      rc.update(race_type_id: RaceType.where("description = ?","Marathon").first.id)
      if rc.total_distance.to_i == 0 #if not already set
        rc.update(total_distance: 42195) #defaulting a Marathon race to 42195m
      end
      puts "Set sport_type: #{rc.sport_type.description}, race_type: #{rc.race_type.description}, total_distance:#{rc.total_distance.to_s} for #{rc.name}|#{rc.extended_name} "
     end

    #Matching Marathon and Half-Marathon runs by name and total_time_avg
    ImportedRace.where("name LIKE ?","%marathon%").each do |rc| #will match "Marathon" and "marathon"
      rs=RaceStatistic.where("imported_race_id = ?", rc.id).first
      case rs.total_time_avg.to_f/100 
        when 3.5*3600 .. 4.75*3600 
          rc.update(sport_type_id: SportType.where("description = ?","Run").first.id)
          rc.update(race_type_id: RaceType.where("description = ?","Marathon").first.id)
          if rc.total_distance.to_i == 0 #if not already set
            rc.update(total_distance: 42195) #defaulting a Marathon race to 42195m
          end
          puts "Set sport_type: #{rc.sport_type.description}, race_type: #{rc.race_type.description}, total_distance:#{rc.total_distance.to_s} for #{rc.name}|#{rc.extended_name} "
        when 1.5*3600 .. 2.5*3600 
          rc.update(sport_type_id: SportType.where("description = ?","Run").first.id)
          rc.update(race_type_id: RaceType.where("description = ?","Half-Marathon").first.id)
          if rc.total_distance.to_i == 0 #if not already set
            rc.update(total_distance: 21100) #defaulting a Marathon race to 42195m
          end
          puts "Set sport_type: #{rc.sport_type.description}, race_type: #{rc.race_type.description}, total_distance:#{rc.total_distance.to_s} for #{rc.name}|#{rc.extended_name} "
      end
    end    
   
    #Matching Half-Marathon runs by extended name
    ImportedRace.where("(extended_name LIKE 'Half marathon') or (extended_name LIKE 'Halfmarathon')").each do |rc| #will match "Half Marathon" and "halfmarathon" etc.
      rc.update(sport_type_id: SportType.where("description = ?","Run").first.id)
      rc.update(race_type_id: RaceType.where("description = ?","Half-Marathon").first.id)
      if rc.total_distance.to_i == 0 #if not already set
        rc.update(total_distance: 21100) #defaulting a Marathon race to 42195m
      end
      puts "Set sport_type: #{rc.sport_type.description}, race_type: #{rc.race_type.description}, total_distance:#{rc.total_distance.to_s} for #{rc.name}|#{rc.extended_name} "
     end  
   
    #Matching 10km runs by extended name and total_time_avg
    ImportedRace.where("extended_name LIKE '%10km%'").each do |rc|
      rs=RaceStatistic.where("imported_race_id = ?", rc.id).first
      case rs.total_time_avg.to_f/100 
        when 0.5*3600 .. 1.5*3600 
          rc.update(sport_type_id: SportType.where("description = ?","Run").first.id)
          rc.update(race_type_id: RaceType.where("description = ?","10k-Run").first.id)
          if rc.total_distance.to_i == 0 #if not already set
            rc.update(total_distance: 10000) #defaulting a Marathon race to 10000m
          end
          puts "Set sport_type: #{rc.sport_type.description}, race_type: #{rc.race_type.description}, total_distance:#{rc.total_distance.to_s} for #{rc.name}|#{rc.extended_name} "
      end
    end
   
    
    RaceStatistic.all.each do |rs|
      if (rs.swim_time_avg.to_f > 0 and rs.bike_time_avg.to_f > 0 and rs.run_time_avg.to_f > 0) then #this must be a triathlon
        rc=ImportedRace.find_by_id(rs.imported_race_id)
        rc.update(sport_type_id: SportType.where("description = ?","Triathlon").first.id)
        case rs.total_time_avg.to_f/100 
          when 9.5*3600 .. 12*3600 
            rc.update(race_type_id: RaceType.where("description = ?","Longdistance").first.id)
            puts "Set sport_type: #{rc.sport_type.description}, race_type: #{rc.race_type.description} for #{rc.name}|#{rc.extended_name} "
          when 4*3600 .. 6*3600 
            rc.update(race_type_id: RaceType.where("description = ?","Halfdistance").first.id)
            puts "Set sport_type: #{rc.sport_type.description}, race_type: #{rc.race_type.description} for #{rc.name}|#{rc.extended_name} "
          #fixme evaluate limit for other distances such as Olympic
          #when 4*3600 .. 6*3600 
          #  rc.update(race_type_id: RaceType.where("description = ?","Halfdistance").first.id)
          #  puts "Setting race_type for #{rc.name} to Halfdistance"
        end

      end
    end

    ImportedRace.where(organizer: "World Triathlon Corporation").each do | rc |
      import_source_id=ImportSource.where(description: "Ironman").first.id
      rc.update(import_source_id: import_source_id)
      puts "Setting import_source_id for #{rc.name} to #{import_source_id.to_s} (Ironman)"
    end


  end
  


end
