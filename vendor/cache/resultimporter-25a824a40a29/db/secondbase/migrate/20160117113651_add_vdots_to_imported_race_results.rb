class AddVdotsToImportedRaceResults < ActiveRecord::Migration
  def change
    add_column :imported_race_results, :vdot, :float ,limit: 12
    add_column :imported_race_results, :improved_vdot, :float ,limit: 12
    add_column :imported_race_results, :tritotaltime_vdot, :float ,limit: 12
    add_column :imported_race_results, :triswim_vdot, :float ,limit: 12
    add_column :imported_race_results, :tribike_vdot, :float ,limit: 12
    add_column :imported_race_results, :trirun_vdot, :float ,limit: 12
    add_column :imported_race_results, :transition1_vdot, :float ,limit: 12
    add_column :imported_race_results, :transition2_vdot, :float ,limit: 12
  end
end
