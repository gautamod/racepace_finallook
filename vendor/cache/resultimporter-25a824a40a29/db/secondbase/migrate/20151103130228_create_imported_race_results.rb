class CreateImportedRaceResults < ActiveRecord::Migration
  def change
    create_table :imported_race_results do |t|
      t.references :imported_race, index: true
      t.string :start_number, limit: 16
      t.string :name, limit: 64
      t.string :surname, limit: 64
      t.string :gender, limit: 32
      t.date :birthday
      t.string :profession, limit: 64
      t.string :club, limit: 64
      t.string :country, limit: 64
      t.string :city, limit: 64
      t.float :latitude, limit: 12
      t.float :longitude, limit: 12
      t.string :division, limit: 32
      t.integer :division_rank, limit: 3
      t.integer :division_num_participants, limit: 3
      t.integer :overall_rank, limit: 3
      t.integer :overall_num_participants, limit: 3
      t.integer :total_time, limit: 4 #in milliseconds
      t.integer :swim_time, limit: 4 #in milliseconds
      t.integer :bike_time, limit: 4 #in milliseconds
      t.integer :run_time, limit: 4 #in milliseconds
      t.integer :transition1_time, limit: 4 #in milliseconds
      t.integer :transition2_time, limit: 4 #in milliseconds
      t.references :import_source, index: true
      t.string :import_source_url, limit: 256

      t.timestamps
    end
  end
end
