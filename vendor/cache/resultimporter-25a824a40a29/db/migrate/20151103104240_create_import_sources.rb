class CreateImportSources < ActiveRecord::Migration
  def change
    create_table :import_sources do |t|
      t.string :description, limit: 64

      t.timestamps
    end
  end
end
