class CreateSportTypes < ActiveRecord::Migration
  def change
    create_table :sport_types do |t|
      t.string :description, limit: 64

      t.timestamps
    end
  end
end
