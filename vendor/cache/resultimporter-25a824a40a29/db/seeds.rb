# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.find_or_create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Enumerations

# %w{ admin moderator member }.each do |x|
#   Role.enumeration_model_updates_permitted = true
#   Role.new(name: x).save!
#   Role.enumeration_model_updates_permitted = false
# end

# %w{ pending accepted }.each do |x|
#   FriendshipStatus.enumeration_model_updates_permitted = true
#   FriendshipStatus.new(name: x).save!
#   FriendshipStatus.enumeration_model_updates_permitted = false
# end

# State.new(name: '-').save!

# MetroArea.new(name: '-', state_id: 1, country_id: 0).save!

# Test users for development and testing

# def create_user(name, role = :member)
#   attributes = {
#     login:                 name,
#     login_slug:            name,
#     email:                 "#{name}@localhost.local",
#     password:              name,
#     password_confirmation: name,
#     birthday:              '2000-01-01'
#   }
  
#   user = User.create!(attributes)
#   user.role = Role[role]
#   user.activate
# end

# if %w{ development test }.include?(Rails.env)
#   create_user('testadmin', :admin)
#   (1..9).each do |i|
#     create_user("testuser#{i}")
#   end
# end

puts "Seeding SportTypes"
SportType.find_or_create_by({ description: "Run" })
SportType.find_or_create_by({ description: "Inline" })
SportType.find_or_create_by({ description: "Mountain Bike" })
SportType.find_or_create_by({ description: "Cycle Cross" })
SportType.find_or_create_by({ description: "BMX" })
SportType.find_or_create_by({ description: "Bike" })
SportType.find_or_create_by({ description: "Bike Tour" })
SportType.find_or_create_by({ description: "Skiing" })
SportType.find_or_create_by({ description: "Cross-Country Skiing" })
SportType.find_or_create_by({ description: "Speed Skating" })
SportType.find_or_create_by({ description: "Swim" })
SportType.find_or_create_by({ description: "Water Skiing" })
SportType.find_or_create_by({ description: "Wakeboard" })
SportType.find_or_create_by({ description: "Rowing" })
SportType.find_or_create_by({ description: "Sailing" })
SportType.find_or_create_by({ description: "Biathlon" })
SportType.find_or_create_by({ description: "Triathlon" })
SportType.find_or_create_by({ description: "Duathlon" })
SportType.find_or_create_by({ description: "Gigathlon" })
SportType.find_or_create_by({ description: "Aquathlon" })
SportType.find_or_create_by({ description: "Walking" })
SportType.find_or_create_by({ description: "Athletics" })
SportType.find_or_create_by({ description: "Motorsports" })
SportType.find_or_create_by({ description: "Rowing"})
SportType.find_or_create_by({ description: "Other" })
SportType.find_or_create_by({ description: "Unknown" })

puts "Seeding RaceTypes"
RaceType.find_or_create_by({ description: "Marathon" })
RaceType.find_or_create_by({ description: "Half-Marathon" })
RaceType.find_or_create_by({ description: "10k-Run" })
RaceType.find_or_create_by({ description: "Ultratriathlon" })
RaceType.find_or_create_by({ description: "Ultraman" })
RaceType.find_or_create_by({ description: "Ironman" })
RaceType.find_or_create_by({ description: "Longdistance" })
RaceType.find_or_create_by({ description: "Halfdistance" })
RaceType.find_or_create_by({ description: "Triple Olympic" })
RaceType.find_or_create_by({ description: "Double Olympic" })
RaceType.find_or_create_by({ description: "Half-Ironman" })
RaceType.find_or_create_by({ description: "Olympic" })
RaceType.find_or_create_by({ description: "5150" })
RaceType.find_or_create_by({ description: "Sprint" })

puts "Seeding ImportSources"
ImportSource.find_or_create_by({ description: "Mikatiming" })
ImportSource.find_or_create_by({ description: "Ironman" })
ImportSource.find_or_create_by({ description: "MyRaceResult" })
