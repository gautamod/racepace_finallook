Rails.application.routes.draw do
  
  resources :imported_race_results
  resources :imported_races
  resources :import_sources
  resources :race_types
  resources :sport_types
  resources :races
  
end
