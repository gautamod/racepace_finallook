class ImportedRacesController < ApplicationController
  skip_before_action :authenticate_user!
  
  skip_after_action :verify_authorized
  skip_after_action :verify_policy_scoped

  before_action :set_imported_race, only: [:show, :edit, :update, :destroy]


  require 'histogram/array'
  require 'gchart'


  # GET /imported_races
  def index
    @imported_races = ImportedRace.all
     #added for geocoding 
     if params[:search].present?
      @imported_races = ImportedRace.near(params[:search], 50, :order => :distance)
     else
      @imported_races = ImportedRace.all
     end

    #@races = Race.page(params[:page])
    #@races = Race.all
    @imported_races_table_grid = initialize_grid(ImportedRace,
        #include: [:users, :results],
        order: 'imported_races.id',
        order_direction: 'desc',
        enable_export_to_csv: true,
        csv_file_name: 'exported_races',
        per_page:10)
    
  end

   #######################################################################
  # Search for races                                                    #
  #######################################################################
 def search
   @imported_races = ImportedRace.search(params[:q]).page(params[:page]).records
    @imported_races_table_grid = initialize_grid(ImportedRace,
        name: 'grid3',
        #include: [:users, :results],
        order: 'imported_races.id',
        order_direction: 'desc',
        enable_export_to_csv: true,
        csv_file_name: 'exported_races',
        per_page:10)

    render action: "index"
end

 # GET /races/1
  # GET /races/1.json
  def show
    #commontator_thread_show(@race)
     #@athletes = Athlete.page(params[:page])
    # @raceresult_items=@race.Raceresult.page(params[:page]) #.paginate(page: 1)
    if params[:page]=="" #bugfix for empty page_no for first page
      params[:page]=1
    end
    @imported_race_result_items=@imported_race.imported_race_result.paginate(:page => params[:page], :per_page => 25)

    @agegroup_count_hash=ImportedRaceResult.where("imported_race_id=?",@imported_race.id).group("division").count
    (@ag_noathletes,@ag_labels) = data_and_labels_from_hash(@agegroup_count_hash)
    @agegroup_bar = Gchart.bar(:data => @ag_noathletes,:labels => @ag_labels, :title => "Agegroups", :vAxis => { title: "Participants"})

    @genders_count_hash=ImportedRaceResult.where("imported_race_id=?",@imported_race.id).group("gender").count
    (gender_noathletes,gender_labels) = data_and_labels_from_hash(@genders_count_hash)
    @gender_pie = Gchart.pie(:data => gender_noathletes, :labels=>["Female: "+@genders_count_hash["F"].to_s,"Male: "+@genders_count_hash["M"].to_s], :title => "Gender")
  
    #find max and min value over all rroverall values for this race
    max_val = Histogramsql.maximum_value("imported_race_results", "total_time", {bands: 6,conditions: "imported_race_id = #{@imported_race.id}"})
    min_val = Histogramsql.minimum_value("imported_race_results", "total_time", {bands: 6,conditions: "imported_race_id = #{@imported_race.id}"})
    inc=inc_in_sec(max_val) #300, 600, 1800 depending on max_val
    max=(max_val/inc).floor*inc+inc #round up to nearest e.g 5min value e.g. 5h25
    min=(min_val/inc).floor*inc     #round down to nearest e.g 5min value e.g. 2h35
    bands=(max-min)/inc #generate bands with 10min time difference
    (bins, @all_rroverall_histvals) = Histogramsql.hist("imported_race_results", "total_time", {max: (max_val/300).floor*300, min: (min_val/300).floor*300, bands: bands,conditions: "imported_race_id = #{@imported_race.id}"})
      
    #get histogram values for each division/agegroup
    @ag_rroverall_histvals_hash={}
    

    @ag_labels.each do |ag_label| #eg "40"
      (bins, histvals) = Histogramsql.hist("imported_race_results", "total_time", {max: max, min: min, bands: bands,conditions: "imported_race_id = #{@imported_race.id} and division='#{ag_label}'"})      
      @ag_rroverall_histvals_hash = @ag_rroverall_histvals_hash.merge(ag_label => histvals) # create a hash for each agegroup {"35"=>[histvals],"40"=>[histvals], ...}
    end
    @ag_rroverall_histvals_hash.delete(nil) #fix for a very strange problem 
    
        #result_ary=Raceresult.where("Race_id=?",@race.id).pluck(:rroverall)
    #no_results=Raceresult.where("Race_id=?",@race.id).count
    #if no_results > 0
    # seconds_ary=[]
    # result_ary.each do |time_str|
    #if time_str != nil and time_str.size > 5
    #        seconds_ary << Time.parse(time_str)
    #end
    #     end
    #  result_hist_wbins_badmapping = seconds_ary.histogram((no_results/50).to_i)
    #  (bins, freqs) = result_hist_wbins_badmapping
      

      #max_val = Histogramsql.maximum_value("raceresults", "TIME_TO_SEC(rroverall)", {bands: 6,conditions: "Race_id = #{@race.id}"})
      #min_val = Histogramsql.minimum_value("raceresults", "TIME_TO_SEC(rroverall)", {bands: 6,conditions: "Race_id = #{@race.id}"})
      #bands=(max_val-min_val)/600
      #(bins, histvals) = Histogramsql.bands("raceresults", "TIME_TO_SEC(rroverall)", {max: (max_val/300).floor*300, min: (min_val/300).floor*300, bands: bands,conditions: "Race_id = #{@race.id}"})
      #(bins, female_histvals) = Histogramsql.bands("raceresults", "TIME_TO_SEC(rroverall)", {max: (max_val/300).floor*300, min: (min_val/300).floor*300, bands: bands,conditions: "Race_id = #{@race.id}"})
      
      #@result_hist_wbins = Histogramsql.msec_bands("raceresults", "TIME_TO_SEC(rroverall)", {bands: 30,conditions: "Race_id = #{@race.id}"})
      @all_rroverall_hist_wbins=[]
      i=0
      bins.each do |bin|
        @all_rroverall_hist_wbins << [bin*1000,@all_rroverall_histvals[i]] 
        i=i+1
      end
    
      @ag_rroverall_hist_wbins_hash={} # [ [bins][histvals ak30],[bins][histvals ak35],... ]
 
      temp_histvals=[]
         @ag_rroverall_histvals_hash.keys.each do |key|
          temp_hist_wbins=[]
          i=0
          temp_histvals = @ag_rroverall_histvals_hash[key]
          bins.each do |bin|
           temp_hist_wbins << [bin*1000,temp_histvals[i]]
           i=i+1
           end
        @ag_rroverall_hist_wbins_hash = @ag_rroverall_hist_wbins_hash.merge(key => temp_hist_wbins)
       end

     

      @time_increments=[]
      bins.each do |bin|
        @time_increments << Time.at(bin).utc.strftime("%H:%M")
      end
    
    @female_result_hist_wbins=@result_hist_wbins #fixme: is just a copy for test

    # else
    # @result_hist_wbins = [0,0]
    #end
    #agegroup_count_hash=Raceresult.where("Race_id=?",@race.id).group("rrdivision").count
    #gc_data_labels = data_and_labels_from_hash(agegroup_count_hash)
    
   
    #@rroverall_hist_bar = Gchart.bar(:data => histvals,:labels => bins, :title => "Agegroups", :vAxis => { title: "Participants"})


 #  lc = GoogleChart::LineChart.new("200x200", "My Results", false)
 #  lc.data "Rankings", ranks, green


  # puts lc.to_url # Or @line_graph = lc.to_url
  end


  # GET /imported_races/new
  def new
    @imported_race = ImportedRace.new
  end

  # GET /imported_races/1/edit
  def edit
  end

  # POST /imported_races
  def create
    @imported_race = ImportedRace.new(imported_race_params)

    if @imported_race.save
      redirect_to @imported_race, notice: 'Imported race was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /imported_races/1
  def update
    if @imported_race.update(imported_race_params)
      redirect_to @imported_race, notice: 'Imported race was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /imported_races/1
  def destroy
    @imported_race.destroy
    redirect_to imported_races_url, notice: 'Imported race was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_imported_race
      @imported_race = ImportedRace.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def imported_race_params
      params.require(:imported_race).permit(:series, :name, :extended_name, :organizer, :country, :city, :latitdude, :longitude, :date, :sport_type_id, :race_type_id, :total_distance, :swim_distance, :bike_distance, :run_distance, :import_source_id, :import_source_url)
    end

    def inc_in_sec(max_time_in_sec)
      if max_time_in_sec < 4400 #90min
        inc_in_sec=300 #5min
      elsif max_time_in_sec < 3*3600
        inc_in_sec=600 #10min
      elsif max_time_in_sec < 4*3600+1800
        inc_in_sec=900 #15min
      elsif max_time_in_sec < 6*3600
        inc_in_sec=1200 #20min
      elsif max_time_in_sec < 12*3600
        inc_in_sec=1800 #30min
      elsif max_time_in_sec < 24*3600
        inc_in_sec=3600 #1h
      else
        inc_in_sec=7200 #2h
      end
      inc_in_sec
    end

    #######################################################################
    # Extract a data_array and labels_array from a hash                   #
    #######################################################################
    def data_and_labels_from_hash(group_count_hash)
      data_array=[]
      labels_array=[]
      if group_count_hash.size > 0
              group_count_hash.keys.each do |key|
                 data_array << group_count_hash[key]
                 labels_array << key
              end
      else
                 data_array << 1
                 labels_array << "empty"
      end
      [data_array,labels_array]
    end

end
