class ImportedRaceResultsController < ApplicationController
  skip_before_action :authenticate_user!
  
  skip_after_action :verify_authorized
  skip_after_action :verify_policy_scoped

  before_action :set_imported_race_result, only: [:show, :edit, :update, :destroy]

  # GET /imported_race_results
  def index
    #@imported_race_results = ImportedRaceResult.all
      #@imported_race_results = Raceresult.all
    @imported_race_results = ImportedRaceResult.page(params[:page])
    @imported_race_results_table_grid = initialize_grid(ImportedRaceResult,
        name: 'grid2',
        order: 'imported_race_results.id',
        order_direction: 'desc',
        enable_export_to_csv: true,
        csv_file_name: 'exported_imported_race_results',
        per_page:10)
    #@imported_race_results_grouped_by_vdot=[] #fixme: maybe group_by is much more efficient
    #(0..13).to_a.each do |idx|
    # from=idx*5
    # to=(idx+1)*5
    # @imported_race_results_grouped_by_vdot << [from,to,ImportedRaceResult.where("rrvdot > ? and rrvdot <= ?", from, to).count]
    #end
  end

  def search
   @imported_race_results = ImportedRaceResult.search(params[:q]).page(params[:page]).records
    @imported_race_results_table_grid = initialize_grid(ImportedRaceResult,
        name: 'grid2',
        #include: [:users, :results],
        order: 'imported_race_results.id',
        order_direction: 'desc',
        enable_export_to_csv: true,
        csv_file_name: 'exported_imported_race_results',
        per_page:10)

    render action: "index"
 end

  # GET /imported_race_results/1
  def show
  end

  # GET /imported_race_results/new
  def new
    @imported_race_result = ImportedRaceResult.new
  end

  # GET /imported_race_results/1/edit
  def edit
  end

  # POST /imported_race_results
  def create
    @imported_race_result = ImportedRaceResult.new(imported_race_result_params)

    if @imported_race_result.save
      redirect_to @imported_race_result, notice: 'Imported race result was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /imported_race_results/1
  def update
    if @imported_race_result.update(imported_race_result_params)
      redirect_to @imported_race_result, notice: 'Imported race result was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /imported_race_results/1
  def destroy
    @imported_race_result.destroy
    redirect_to imported_race_results_url, notice: 'Imported race result was successfully destroyed.'
  end

  def allocate #this will assign this raceresult to the current_user.id
     #@raceresult.update(user: current_user.id)
     @raceresult = ImportedRaceResult.where("id = ?",params[:id]).first
     if @raceresult.user_id == nil
      @raceresult.update(user_id: current_user.id)
      flash[:success] = "This raceresult is successfully assigned to you!"
    elsif @raceresult.user_id == current_user.id
      flash[:info] = "This raceresult is already assigned to you."
    else
      flash[:error] = "This raceresult is already assigned to #{User.find_by_id(@raceresult.user_id).name}!"
    end  
     redirect_to :back
  end

  def unallocate #this will assign this raceresult to the current_user.id
     #@raceresult.update(user: current_user.id)
     @raceresult = ImportedRaceResult.where("id = ?",params[:id]).first
      @raceresult.update(user_id: nil)
      flash[:success] = "This raceresult is successfully released/unallocated!"
      redirect_to :back
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_imported_race_result
      @imported_race_result = ImportedRaceResult.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def imported_race_result_params
      params.require(:imported_race_result).permit(:imported_race_id, :name, :surname, :gender, :brithday, :profession, :club, :country, :city, :start_number, :division, :division_rank, :division_num_participants, :overall_rank, :overall_num_participants, :total_time, :swim_time, :bike_time, :run_time, :transition1_time, :transition2_time, :import_source_id, :import_source_url, :comment)
    end
end
