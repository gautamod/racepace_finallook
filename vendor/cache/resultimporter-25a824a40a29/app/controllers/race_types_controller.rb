class RaceTypesController < ApplicationController
  before_action :set_race_type, only: [:show, :edit, :update, :destroy]

  # GET /race_types
  def index
    @race_types = RaceType.all
  end

  # GET /race_types/1
  def show
  end

  # GET /race_types/new
  def new
    @race_type = RaceType.new
  end

  # GET /race_types/1/edit
  def edit
  end

  # POST /race_types
  def create
    @race_type = RaceType.new(race_type_params)

    if @race_type.save
      redirect_to @race_type, notice: 'Race type was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /race_types/1
  def update
    if @race_type.update(race_type_params)
      redirect_to @race_type, notice: 'Race type was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /race_types/1
  def destroy
    @race_type.destroy
    redirect_to race_types_url, notice: 'Race type was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_race_type
      @race_type = RaceType.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def race_type_params
      params.require(:race_type).permit(:description)
    end
end
