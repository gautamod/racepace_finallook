class ImportSourcesController < ApplicationController
  before_action :set_import_source, only: [:show, :edit, :update, :destroy]

  # GET /import_sources
  def index
    @import_sources = ImportSource.all
  end

  # GET /import_sources/1
  def show
  end

  # GET /import_sources/new
  def new
    @import_source = ImportSource.new
  end

  # GET /import_sources/1/edit
  def edit
  end

  # POST /import_sources
  def create
    @import_source = ImportSource.new(import_source_params)

    if @import_source.save
      redirect_to @import_source, notice: 'Import source was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /import_sources/1
  def update
    if @import_source.update(import_source_params)
      redirect_to @import_source, notice: 'Import source was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /import_sources/1
  def destroy
    @import_source.destroy
    redirect_to import_sources_url, notice: 'Import source was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_import_source
      @import_source = ImportSource.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def import_source_params
      params.require(:import_source).permit(:description)
    end
end
