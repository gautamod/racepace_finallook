class SportTypesController < ApplicationController
  before_action :set_sport_type, only: [:show, :edit, :update, :destroy]

  # GET /sport_types
  def index
    @sport_types = SportType.all
  end

  # GET /sport_types/1
  def show
  end

  # GET /sport_types/new
  def new
    @sport_type = SportType.new
  end

  # GET /sport_types/1/edit
  def edit
  end

  # POST /sport_types
  def create
    @sport_type = SportType.new(sport_type_params)

    if @sport_type.save
      redirect_to @sport_type, notice: 'Sport type was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /sport_types/1
  def update
    if @sport_type.update(sport_type_params)
      redirect_to @sport_type, notice: 'Sport type was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /sport_types/1
  def destroy
    @sport_type.destroy
    redirect_to sport_types_url, notice: 'Sport type was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sport_type
      @sport_type = SportType.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def sport_type_params
      params.require(:sport_type).permit(:description)
    end
end
