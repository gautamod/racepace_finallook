class ImportedRacePolicy < ApplicationPolicy

attr_reader :imported_race, :record

  def initialize(imported_race, record)
    @imported_race = imported_race
    @record = record
  end

  def index?
    true
  end

  def show?
    scope.where(:id => record.id).exists?
  end

  def scope
    Pundit.policy_scope!(imported_race, record.class)
  end

  class Scope
    attr_reader :imported_race, :scope

    def initialize(user, scope)
      @imported_race = imported_race
      @scope = scope
    end

    def resolve
      scope
    end
  end



end
