class ImportedRaceResult < SecondBase::Base

  acts_as_commentable
  acts_as_votable
  resourcify

  include PublicActivity::Common

  belongs_to :imported_race
  belongs_to :import_source

  validates :start_number, length: {maximum: 16}
  validates :name, length: {maximum: 64}
  validates :surname, length: {maximum: 64}
  validates :gender, length: {maximum: 32}
  validates :profession, length: {maximum: 64}
  validates :club, length: {maximum: 64}
  validates :country, length: {maximum: 64}
  validates :city, length: {maximum: 64}
  validates :division, length: {maximum: 64}

  scope :ld_only, -> { joins(:imported_race).where('imported_races.race_type_id = ?', RaceType.find_by_description("Longdistance").id)}
  scope :md_only, -> { joins(:imported_race).where('imported_races.race_type_id = ?', RaceType.find_by_description("Halfdistance").id)}
  scope :run42km_only, -> { joins(:imported_race).where('imported_races.race_type_id = ?', RaceType.find_by_description("Marathon").id)}
  scope :run21km_only, -> { joins(:imported_race).where('imported_races.race_type_id = ?', RaceType.find_by_description("Half-Marathon").id)}
  scope :run10km_only, -> { joins(:imported_race).where('imported_races.race_type_id = ?', RaceType.find_by_description("10k-Run").id)}

  searchable do
    text :name
    text :surname
    text :club
  end
end
