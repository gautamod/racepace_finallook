class ImportedRace < SecondBase::Base

  acts_as_commentable

  belongs_to :sport_type
  belongs_to :race_type
  belongs_to :import_source

  has_many :imported_race_result, dependent: :destroy
  has_many :race_statistic, dependent: :destroy

  geocoded_by :city     # can also be an IP address
  after_validation :geocode, :if => :city_changed? # auto-fetch coordinates
 
  validates :series, length: {maximum: 64}
  validates :name, length: {maximum: 128}
  validates :extended_name, length: {maximum: 128}
  validates :organizer, length: {maximum: 128}
  validates :country, length: {maximum: 64}
  validates :city, length: {maximum: 64}
  
  searchable do
    text :series
    text :name
    text :extended_name
    text :organizer
    text :country
    text :city
  end
  
end

