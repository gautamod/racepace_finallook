# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment',  __FILE__)

if Rails.configuration.try(:xx_do_profiling)
  use Rack::RubyProf,
    path: ::File.expand_path('tmp/profile'),
    printers: {
      RubyProf::FlatPrinterWithLineNumbers => 'flat.txt',
      RubyProf::GraphPrinter               => 'graph.txt',
      RubyProf::GraphHtmlPrinter           => 'graph.html',
      RubyProf::CallStackPrinter           => 'call_stack.html',
      RubyProf::CallTreePrinter            => 'call_tree.txt',
    }
end

run Rails.application
