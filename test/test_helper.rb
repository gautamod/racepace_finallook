ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

# SimpleCov
require 'simplecov'
SimpleCov.start 'rails' if ENV['COVERAGE']

# Improved Minitest output
Minitest::Reporters.use!(
  Minitest::Reporters::ProgressReporter.new,
  ENV,
  Minitest.backtrace_filter
)

# Mocha
require 'mocha/mini_test'

# Capybara
require "capybara/rails"
require "capybara/webkit"
require "minitest/rails/capybara"

# Use WebKit driver for Capybara
Capybara.javascript_driver = :webkit

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
end

class ActionController::TestCase
  include Devise::TestHelpers
  include Warden::Test::Helpers
end

class ActionDispatch::IntegrationTest
  include Capybara::DSL
end

# Capybara uses two threads to run client and server. Use a single connection but protect
# access to it by having each thread "check out" the connection when they are using it.
# See: https://gist.github.com/mperham/3049152
class ActiveRecord::Base
  mattr_accessor :shared_connection
  @@shared_connection = nil

  def self.connection
    @@shared_connection || ConnectionPool::Wrapper.new(:size => 1) { retrieve_connection }
  end
end
ActiveRecord::Base.shared_connection = ActiveRecord::Base.connection

