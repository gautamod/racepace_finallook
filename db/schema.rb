# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160831161439) do

  create_table "activities", force: :cascade do |t|
    t.integer  "trackable_id",   limit: 4
    t.string   "trackable_type", limit: 255
    t.integer  "owner_id",       limit: 4
    t.string   "owner_type",     limit: 255
    t.string   "key",            limit: 255
    t.text     "parameters",     limit: 65535
    t.integer  "recipient_id",   limit: 4
    t.string   "recipient_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "commentable_id",   limit: 4
    t.string   "commentable_type", limit: 255
    t.string   "title",            limit: 255
    t.text     "body",             limit: 65535
    t.string   "subject",          limit: 255
    t.integer  "user_id",          limit: 4,     null: false
    t.integer  "parent_id",        limit: 4
    t.integer  "lft",              limit: 4
    t.integer  "rgt",              limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["commentable_id", "commentable_type"], name: "index_comments_on_commentable_id_and_commentable_type", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "follows", force: :cascade do |t|
    t.integer  "followable_id",   limit: 4,                   null: false
    t.string   "followable_type", limit: 255,                 null: false
    t.integer  "follower_id",     limit: 4,                   null: false
    t.string   "follower_type",   limit: 255,                 null: false
    t.boolean  "blocked",                     default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "follows", ["followable_id", "followable_type"], name: "fk_followables", using: :btree
  add_index "follows", ["follower_id", "follower_type"], name: "fk_follows", using: :btree

  create_table "fullcalendar_engine_event_series", force: :cascade do |t|
    t.integer  "frequency",  limit: 4,   default: 1
    t.string   "period",     limit: 255, default: "monthly"
    t.datetime "starttime"
    t.datetime "endtime"
    t.boolean  "all_day",                default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ev_id",      limit: 4
    t.string   "ev_type",    limit: 255
  end

  add_index "fullcalendar_engine_event_series", ["ev_type", "ev_id"], name: "index_fullcalendar_engine_event_series_on_ev_type_and_ev_id", using: :btree

  create_table "fullcalendar_engine_events", force: :cascade do |t|
    t.string   "title",           limit: 255
    t.datetime "starttime"
    t.datetime "endtime"
    t.boolean  "all_day",                       default: false
    t.text     "description",     limit: 65535
    t.integer  "event_series_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ev_id",           limit: 4
    t.string   "ev_type",         limit: 255
  end

  add_index "fullcalendar_engine_events", ["ev_type", "ev_id"], name: "index_fullcalendar_engine_events_on_ev_type_and_ev_id", using: :btree
  add_index "fullcalendar_engine_events", ["event_series_id"], name: "index_fullcalendar_engine_events_on_event_series_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.string   "name",        limit: 256,   null: false
    t.string   "description", limit: 16384
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "import_sources", force: :cascade do |t|
    t.string   "description", limit: 64
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "laps", force: :cascade do |t|
    t.float    "lptotaltimeseconds", limit: 24
    t.float    "lpdistancemeters",   limit: 24
    t.float    "lpavgheartratebpm",  limit: 24
    t.float    "lpmaxheartratebpm",  limit: 24
    t.float    "lpavgbikecadence",   limit: 24
    t.float    "lpmaxbikecadence",   limit: 24
    t.float    "lpavgspeed",         limit: 24
    t.float    "lpmaxspeed",         limit: 24
    t.float    "lpavgwatts",         limit: 24
    t.float    "lpmaxwatts",         limit: 24
    t.float    "lpcalories",         limit: 24
    t.string   "lpintensity",        limit: 255
    t.string   "lptriggermethod",    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "workout_id",         limit: 4
  end

  add_index "laps", ["workout_id"], name: "index_laps_on_workout_id", using: :btree

  create_table "metrics", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.integer  "weight",          limit: 4
    t.float    "sleep_hours",     limit: 24
    t.integer  "sleep_quality",   limit: 4
    t.float    "rest_hr",         limit: 24
    t.float    "max_hr",          limit: 24
    t.float    "bmi",             limit: 24
    t.integer  "overall_feeling", limit: 4
    t.string   "description",     limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "metrics", ["user_id"], name: "index_metrics_on_user_id", using: :btree

  create_table "mqtt_messages", force: :cascade do |t|
    t.string   "message",         limit: 255
    t.string   "topic",           limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.float    "latitude",        limit: 24
    t.float    "longitude",       limit: 24
    t.integer  "user_id",         limit: 4
    t.integer  "virtual_race_id", limit: 4
    t.datetime "timestamp"
  end

  add_index "mqtt_messages", ["user_id"], name: "index_mqtt_messages_on_user_id", using: :btree
  add_index "mqtt_messages", ["virtual_race_id"], name: "index_mqtt_messages_on_virtual_race_id", using: :btree

  create_table "planedunits", force: :cascade do |t|
    t.integer  "trainingplan_id", limit: 4
    t.integer  "pusporttypeid",   limit: 4
    t.datetime "pudate"
    t.integer  "puunitpoolid",    limit: 4
    t.integer  "pudistance",      limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "planedunits", ["trainingplan_id"], name: "index_planedunits_on_trainingplan_id", using: :btree

  create_table "preferences", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "locale",     limit: 5
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "preferences", ["user_id"], name: "index_preferences_on_user_id", using: :btree

  create_table "race_statistics", force: :cascade do |t|
    t.integer  "imported_race_id",      limit: 4
    t.float    "total_time_avg",        limit: 24
    t.float    "swim_time_avg",         limit: 24
    t.float    "bike_time_avg",         limit: 24
    t.float    "run_time_avg",          limit: 24
    t.float    "transition1_time_avg",  limit: 24
    t.float    "transition2_time_avg",  limit: 24
    t.integer  "num_results",           limit: 4
    t.integer  "num_dnf_results",       limit: 4
    t.integer  "num_finisher_results",  limit: 4
    t.float    "vdot_avg",              limit: 24
    t.float    "improved_vdot_avg",     limit: 24
    t.float    "tritotaltime_vdot_avg", limit: 24
    t.float    "triswim_vdot_avg",      limit: 24
    t.float    "tribike_vdot_avg",      limit: 24
    t.float    "trirun_vdot_avg",       limit: 24
    t.float    "transition1_vdot_avg",  limit: 24
    t.float    "transition2_vdot_avg",  limit: 24
    t.float    "vdot_std",              limit: 24
    t.float    "improved_vdot_std",     limit: 24
    t.float    "tritotaltime_vdot_std", limit: 24
    t.float    "triswim_vdot_std",      limit: 24
    t.float    "tribike_vdot_std",      limit: 24
    t.float    "trirun_vdot_std",       limit: 24
    t.float    "transition1_vdot_std",  limit: 24
    t.float    "transition2_vdot_std",  limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "num_vdot_level1",       limit: 4
    t.integer  "num_vdot_level2",       limit: 4
    t.integer  "num_vdot_level3",       limit: 4
    t.integer  "num_vdot_level4",       limit: 4
    t.integer  "num_vdot_level5",       limit: 4
    t.integer  "num_vdot_level6",       limit: 4
    t.integer  "num_vdot_level7",       limit: 4
    t.integer  "num_vdot_level8",       limit: 4
    t.integer  "num_vdot_level9",       limit: 4
    t.integer  "num_vdot_level10",      limit: 4
    t.integer  "num_vdot_level11",      limit: 4
    t.integer  "num_vdot_level12",      limit: 4
    t.integer  "num_vdot_level13",      limit: 4
    t.integer  "num_vdot_level14",      limit: 4
    t.integer  "num_vdot_level15",      limit: 4
  end

  add_index "race_statistics", ["imported_race_id"], name: "index_race_statistics_on_imported_race_id", using: :btree

  create_table "race_types", force: :cascade do |t|
    t.string   "description", limit: 64
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "resource_id",   limit: 4
    t.string   "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "runpaces", force: :cascade do |t|
    t.string   "rpname",     limit: 255
    t.integer  "rpfast",     limit: 4
    t.integer  "rpslow",     limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "sport_types", force: :cascade do |t|
    t.string   "description", limit: 64
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trackpoints", force: :cascade do |t|
    t.integer  "track_id",           limit: 4
    t.time     "tptime"
    t.float    "tplatitudedegrees",  limit: 24
    t.float    "tplongitudedegrees", limit: 24
    t.float    "tpaltitudemeters",   limit: 24
    t.float    "tpdistancemeters",   limit: 24
    t.float    "tpspeed",            limit: 24
    t.float    "tpheartratebpm",     limit: 24
    t.float    "tpcadence",          limit: 24
    t.float    "tpwatts",            limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "tptimeseconds",      limit: 4
  end

  add_index "trackpoints", ["track_id"], name: "index_trackpoints_on_track_id", using: :btree

  create_table "tracks", force: :cascade do |t|
    t.integer  "lap_id",     limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tracks", ["lap_id"], name: "index_tracks_on_lap_id", using: :btree

  create_table "trainingplans", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.datetime "tpracedate"
    t.time     "tpracetime"
    t.integer  "tpunitsperweek", limit: 4
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.boolean  "show",                     default: true
    t.string   "color",          limit: 7, default: "#1AB394"
  end

  add_index "trainingplans", ["user_id"], name: "index_trainingplans_on_user_id", using: :btree

  create_table "unitpools", force: :cascade do |t|
    t.string   "upname",          limit: 255
    t.integer  "upnoiv",          limit: 4
    t.integer  "upivpace",        limit: 4
    t.integer  "upivdistance",    limit: 4
    t.integer  "upivhr",          limit: 4
    t.integer  "uppausedistance", limit: 4
    t.integer  "uppausepace",     limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "",   null: false
    t.string   "encrypted_password",     limit: 255, default: "",   null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.integer  "failed_attempts",        limit: 4,   default: 0,    null: false
    t.string   "unlock_token",           limit: 255
    t.datetime "locked_at"
    t.datetime "deactivated_at"
    t.string   "uid",                    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name",       limit: 255
    t.string   "avatar_content_type",    limit: 255
    t.integer  "avatar_file_size",       limit: 4
    t.datetime "avatar_updated_at"
    t.string   "username",               limit: 255
    t.string   "firstname",              limit: 255
    t.string   "lastname",               limit: 255
    t.string   "gender",                 limit: 255
    t.date     "date_of_birth"
    t.string   "country",                limit: 255
    t.string   "state",                  limit: 255
    t.string   "city",                   limit: 255
    t.boolean  "receive_email",                      default: true
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["uid"], name: "index_users_on_uid", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  create_table "virtual_races", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "votes", force: :cascade do |t|
    t.integer  "votable_id",   limit: 4
    t.string   "votable_type", limit: 255
    t.integer  "voter_id",     limit: 4
    t.string   "voter_type",   limit: 255
    t.boolean  "vote_flag"
    t.string   "vote_scope",   limit: 255
    t.integer  "vote_weight",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree

  create_table "workouts", force: :cascade do |t|
    t.integer  "sport_type_id",             limit: 4
    t.integer  "trainingplan_id",           limit: 4
    t.string   "title",                     limit: 255
    t.string   "description",               limit: 255
    t.integer  "planed_duration",           limit: 4
    t.integer  "completed_duration",        limit: 4
    t.integer  "planed_distance",           limit: 4
    t.integer  "completed_distance",        limit: 4
    t.integer  "planed_avg_pace",           limit: 4
    t.integer  "completed_avg_pace",        limit: 4
    t.float    "planed_avg_speed",          limit: 24
    t.float    "completed_avg_speed",       limit: 24
    t.integer  "planed_calories",           limit: 4
    t.integer  "completed_calories",        limit: 4
    t.float    "planed_elevation_gain",     limit: 24
    t.float    "completed_elevation_gain",  limit: 24
    t.float    "completed_elevation_high",  limit: 24
    t.float    "completed_elevation_low",   limit: 24
    t.float    "planed_tss",                limit: 24
    t.float    "completed_tss",             limit: 24
    t.float    "planed_if",                 limit: 24
    t.float    "completed_if",              limit: 24
    t.float    "planed_avg_hr",             limit: 24
    t.float    "completed_avg_hr",          limit: 24
    t.float    "completed_min_hr",          limit: 24
    t.float    "completed_max_hr",          limit: 24
    t.float    "planed_avg_power",          limit: 24
    t.float    "completed_avg_power",       limit: 24
    t.float    "completed_min_power",       limit: 24
    t.float    "completed_max_power",       limit: 24
    t.string   "autor",                     limit: 255
    t.string   "target",                    limit: 255
    t.integer  "days_before_race",          limit: 4
    t.datetime "planed_datetime"
    t.string   "completed_datetime",        limit: 255
    t.integer  "athlete_id",                limit: 4
    t.integer  "coach_id",                  limit: 4
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "workout_file_file_name",    limit: 255
    t.string   "workout_file_content_type", limit: 255
    t.integer  "workout_file_file_size",    limit: 4
    t.datetime "workout_file_updated_at"
    t.integer  "user_id",                   limit: 4
  end

  add_index "workouts", ["sport_type_id"], name: "index_workouts_on_sport_type_id", using: :btree
  add_index "workouts", ["trainingplan_id"], name: "index_workouts_on_trainingplan_id", using: :btree
  add_index "workouts", ["user_id"], name: "index_workouts_on_user_id", using: :btree

  create_table "zones", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "description",   limit: 255
    t.string   "variant",       limit: 255
    t.string   "calc_base",     limit: 255
    t.string   "method",        limit: 255
    t.integer  "sport_type_id", limit: 4
    t.integer  "user_id",       limit: 4
    t.integer  "low",           limit: 4
    t.integer  "high",          limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "zones", ["sport_type_id"], name: "index_zones_on_sport_type_id", using: :btree
  add_index "zones", ["user_id"], name: "index_zones_on_user_id", using: :btree

  add_foreign_key "laps", "workouts"
  add_foreign_key "metrics", "users"
  add_foreign_key "mqtt_messages", "users"
  add_foreign_key "mqtt_messages", "virtual_races"
  add_foreign_key "planedunits", "trainingplans"
  add_foreign_key "preferences", "users"
  add_foreign_key "workouts", "users"
  add_foreign_key "zones", "sport_types"
  add_foreign_key "zones", "users"
end
