# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

seed_password = '1seed2pass3'

user_superadmin = User.create!(email: "super@localhost.local", password: seed_password, confirmed_at: Time.now, confirmation_sent_at: Time.now)
user_superadmin.add_role :super_admin
user_superadmin.add_role :admin

if %w{ development test }.include?(Rails.env)
  user_admin = User.create!(email: "admin@localhost.local", password: seed_password, confirmed_at: Time.now, confirmation_sent_at: Time.now)
  user_admin.add_role :admin
  
  (1..9).each do |i|
    user_user = User.create!(email: "user#{i}@localhost.local", password: seed_password, confirmed_at: Time.now, confirmation_sent_at: Time.now)
  end
end

SportType.find_or_create_by({ description: "Run" })
SportType.find_or_create_by({ description: "Inline" })
SportType.find_or_create_by({ description: "Mountain Bike" })
SportType.find_or_create_by({ description: "Cycle Cross" })
SportType.find_or_create_by({ description: "BMX" })
SportType.find_or_create_by({ description: "Bike" })
SportType.find_or_create_by({ description: "Bike Tour" })
SportType.find_or_create_by({ description: "Skiing" })
SportType.find_or_create_by({ description: "Cross-Country Skiing" })
SportType.find_or_create_by({ description: "Speed Skating" })
SportType.find_or_create_by({ description: "Swim" })
SportType.find_or_create_by({ description: "Water Skiing" })
SportType.find_or_create_by({ description: "Wakeboard" })
SportType.find_or_create_by({ description: "Rowing" })
SportType.find_or_create_by({ description: "Sailing" })
SportType.find_or_create_by({ description: "Biathlon" })
SportType.find_or_create_by({ description: "Triathlon" })
SportType.find_or_create_by({ description: "Duathlon" })
SportType.find_or_create_by({ description: "Gigathlon" })
SportType.find_or_create_by({ description: "Aquathlon" })
SportType.find_or_create_by({ description: "Walking" })
SportType.find_or_create_by({ description: "Athletics" })
SportType.find_or_create_by({ description: "Motorsports" })
SportType.find_or_create_by({ description: "Rowing"})
SportType.find_or_create_by({ description: "Other" })
SportType.find_or_create_by({ description: "Unknown" })

RaceType.find_or_create_by({ description: "Marathon" })
RaceType.find_or_create_by({ description: "Half-Marathon" })
RaceType.find_or_create_by({ description: "10k-Run" })
RaceType.find_or_create_by({ description: "Ultratriathlon" })
RaceType.find_or_create_by({ description: "Ultraman" })
RaceType.find_or_create_by({ description: "Ironman" })
RaceType.find_or_create_by({ description: "Longdistance" })
RaceType.find_or_create_by({ description: "Halfdistance" })
RaceType.find_or_create_by({ description: "Triple Olympic" })
RaceType.find_or_create_by({ description: "Double Olympic" })
RaceType.find_or_create_by({ description: "Half-Ironman" })
RaceType.find_or_create_by({ description: "Olympic" })
RaceType.find_or_create_by({ description: "5150" })
RaceType.find_or_create_by({ description: "Sprint" })

ImportSource.find_or_create_by({ description: "Mikatiming" })
ImportSource.find_or_create_by({ description: "Ironman" })
ImportSource.find_or_create_by({ description: "MyRaceResult" })

