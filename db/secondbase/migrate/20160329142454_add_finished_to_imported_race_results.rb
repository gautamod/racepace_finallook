class AddFinishedToImportedRaceResults < ActiveRecord::Migration
  def change
	add_column :imported_race_results, :finished, :boolean, :default => true
  end
end
