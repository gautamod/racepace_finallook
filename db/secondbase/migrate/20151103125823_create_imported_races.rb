class CreateImportedRaces < ActiveRecord::Migration
  def change
    create_table :imported_races do |t|
      t.string :series, limit: 64
      t.string :name, limit: 128, null: false
      t.string :extended_name, limit: 128
      t.string :organizer, limit: 128
      t.string :country, limit: 64
      t.string :city, limit: 64
      t.float :latitude, limit: 12
      t.float :longitude, limit: 12
      t.date :date
      t.references :sport_type, index: true
      t.references :race_type, index: true
      t.integer :total_distance, limit: 3 #in meters
      t.integer :swim_distance, limit: 3 #in meters
      t.integer :bike_distance, limit: 3 #in meters
      t.integer :run_distance, limit: 3 #in meters
      t.references :import_source, index: true
      t.string :import_source_url, limit: 256
      t.float :distance

      t.timestamps
    end
  end
end
