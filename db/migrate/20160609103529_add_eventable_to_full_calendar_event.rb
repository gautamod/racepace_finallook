class AddEventableToFullCalendarEvent < ActiveRecord::Migration
  def change
   add_reference :fullcalendar_engine_events, :ev, polymorphic: true, index: true
   add_reference :fullcalendar_engine_event_series, :ev, polymorphic: true, index: true
  end
end
