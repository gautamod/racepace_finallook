class CreateTrainingplans < ActiveRecord::Migration
  def change
    create_table :trainingplans do |t|
      t.references :user, index: true
      t.datetime :tpracedate
      t.time :tpracetime
      t.integer :tpunitsperweek

      t.timestamps null: false
    end
  end
end
