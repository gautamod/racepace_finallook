class CreateVirtualRaces < ActiveRecord::Migration
  def change
    create_table :virtual_races do |t|

      t.timestamps null: false
    end
  end
end
