class AddTptimesecondsToTrackpoint < ActiveRecord::Migration
  def change
    add_column :trackpoints, :tptimeseconds, :integer
  end
end
