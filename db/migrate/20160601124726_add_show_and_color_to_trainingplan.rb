class AddShowAndColorToTrainingplan < ActiveRecord::Migration
  def change
    add_column :trainingplans, :show, :boolean, :default => true
    add_column :trainingplans, :color, :string, limit: 7, :default => "#1AB394"
  end
end
