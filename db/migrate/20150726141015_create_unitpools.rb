class CreateUnitpools < ActiveRecord::Migration
  def change
    create_table :unitpools do |t|
      t.string :upname
      t.integer :upnoiv
      t.integer :upivpace
      t.integer :upivdistance
      t.integer :upivhr
      t.integer :uppausedistance
      t.integer :uppausepace

      t.timestamps null: false
    end
  end
end
