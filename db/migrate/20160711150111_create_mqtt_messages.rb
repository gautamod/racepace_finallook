class CreateMqttMessages < ActiveRecord::Migration
  def change
    create_table :mqtt_messages do |t|
      t.string :message
      t.string :topic

      t.timestamps null: false
    end
  end
end
