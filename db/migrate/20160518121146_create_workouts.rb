class CreateWorkouts < ActiveRecord::Migration
  def change
    create_table :workouts do |t|
      t.references :sport_type, index: true
      t.references :trainingplan, index: true
      t.string :title
      t.string :description
      t.integer :planed_duration
      t.integer :completed_duration
      t.integer :planed_distance
      t.integer :completed_distance
      t.integer :planed_avg_pace
      t.integer :completed_avg_pace
      t.float :planed_avg_speed
      t.float :completed_avg_speed
      t.integer :planed_calories
      t.integer :completed_calories
      t.float :planed_elevation_gain
      t.float :completed_elevation_gain
      t.float :completed_elevation_high
      t.float :completed_elevation_low
      t.float :planed_tss
      t.float :completed_tss
      t.float :planed_if
      t.float :completed_if
      t.float :planed_avg_hr
      t.float :completed_avg_hr
      t.float :completed_min_hr
      t.float :completed_max_hr
      t.float :planed_avg_power
      t.float :completed_avg_power
      t.float :completed_min_power
      t.float :completed_max_power
      t.string :autor
      t.string :target
      t.integer :days_before_race
      t.datetime :planed_datetime
      t.string :completed_datetime
      t.integer :athlete_id
      t.integer :coach_id

      t.timestamps null: false
    end
  end
end
