class CreateMetrics < ActiveRecord::Migration
  def change
    create_table :metrics do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :weight
      t.float :sleep_hours
      t.integer :sleep_quality
      t.float :rest_hr
      t.float :max_hr
      t.float :bmi
      t.integer :overall_feeling
      t.string :description

      t.timestamps null: false
    end
  end
end
