class CreateZones < ActiveRecord::Migration
  def change
    create_table :zones do |t|
      t.string :name
      t.string :description
      t.string :variant
      t.string :calc_base
      t.string :method
      t.references :sport_type, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.integer :low
      t.integer :high

      t.timestamps null: false
    end
  end
end
