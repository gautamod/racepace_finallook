class AddLatitudeAndLongitudeToMqttMessage < ActiveRecord::Migration
  def change
    add_column :mqtt_messages, :latitude, :float
    add_column :mqtt_messages, :longitude, :float
  end
end
