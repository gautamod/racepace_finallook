class CreatePlanedunits < ActiveRecord::Migration
  def change
    create_table :planedunits do |t|
      t.references :trainingplan, index: true
      t.integer :pusporttypeid
      t.datetime :pudate
      t.integer :puunitpoolid
      t.integer :pudistance

      t.timestamps null: false
    end
    add_foreign_key :planedunits, :trainingplans
  end
end
