class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.references :lap, index: true

      t.timestamps
    end
  end
end
