class CreatePreferences < ActiveRecord::Migration
  def change
    create_table :preferences do |t|
      t.references :user, index: true
      t.string :locale, limit: 5

      t.timestamps null: false
    end
    add_foreign_key :preferences, :users
  end
end
