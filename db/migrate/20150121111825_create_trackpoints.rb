class CreateTrackpoints < ActiveRecord::Migration
  def change
    create_table :trackpoints do |t|
      t.references :track, index: true
      t.time :tptime
      t.float :tplatitudedegrees
      t.float :tplongitudedegrees
      t.float :tpaltitudemeters
      t.float :tpdistancemeters
      t.float :tpspeed
      t.float :tpheartratebpm
      t.float :tpcadence
      t.float :tpwatts

      t.timestamps
    end
  end
end
