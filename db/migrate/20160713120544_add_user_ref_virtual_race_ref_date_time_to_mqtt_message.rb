class AddUserRefVirtualRaceRefDateTimeToMqttMessage < ActiveRecord::Migration
  def change
    add_reference :mqtt_messages, :user, index: true, foreign_key: true
    add_reference :mqtt_messages, :virtual_race, index: true, foreign_key: true
    add_column :mqtt_messages, :timestamp, :datetime
  end
end
