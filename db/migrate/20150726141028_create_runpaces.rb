class CreateRunpaces < ActiveRecord::Migration
  def change
    create_table :runpaces do |t|
      t.string :rpname
      t.integer :rpfast
      t.integer :rpslow

      t.timestamps null: false
    end
  end
end
