class CreateLaps < ActiveRecord::Migration
  def change
    create_table :laps do |t|
      #t.references :activity, index: true
      t.float :lptotaltimeseconds
      t.float :lpdistancemeters
      t.float :lpavgheartratebpm
      t.float :lpmaxheartratebpm
      t.float :lpavgbikecadence
      t.float :lpmaxbikecadence
      t.float :lpavgspeed
      t.float :lpmaxspeed
      t.float :lpavgwatts
      t.float :lpmaxwatts
      t.float :lpcalories
      t.string :lpintensity
      t.string :lptriggermethod

      t.timestamps
    end
  end
end
