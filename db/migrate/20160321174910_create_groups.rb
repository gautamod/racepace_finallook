class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :type, limit: 256
      t.string :name, limit: 256, null: false
      t.string :description, limit: 16384

      t.timestamps null: false
    end
  end
end
