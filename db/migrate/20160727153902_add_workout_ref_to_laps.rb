class AddWorkoutRefToLaps < ActiveRecord::Migration
  def change
    add_reference :laps, :workout, index: true, foreign_key: true
  end
end
