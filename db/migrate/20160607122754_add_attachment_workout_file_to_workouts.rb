class AddAttachmentWorkoutFileToWorkouts < ActiveRecord::Migration
  def self.up
    change_table :workouts do |t|
      t.attachment :workout_file
    end
  end

  def self.down
    remove_attachment :workouts, :workout_file
  end
end
