#!/bin/bash

API_USER=   # fill in username
API_PASS=   # fill in password

BASEURL=http://localhost:3000/api/v1/groups

VERB=$1
URL=$BASEURL/$2
DATA=$3

curl -X $VERB -v -H "Content-Type: application/json" -H "X-Api-User: $API_USER" -H "X-Api-Password: $API_PASS" --data "$DATA" $URL

