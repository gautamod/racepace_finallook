namespace :cache do
  task :clear do
    on roles :app do
      within release_path do
        execute :rake, 'cache:clear'
      end
    end
  end
end

