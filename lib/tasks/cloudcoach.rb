require "cloudcoach/engine"
require "cloudcoach/greif"
require "cloudcoach/weling_tri_ld_18h_weekend"

module Cloudcoach

 def self.sayhello
    "Hello"
 end

 def self.create_greif_plan(athlete_id, coach_id, raceduration, racedate, unitsperweek) 
    Greif.create_greif_marathon_plan(athlete_id, coach_id, raceduration, racedate, unitsperweek) 
 end

 def self.create_weling_tri_ld_18h_weekend_plan(athlete_id, coach_id, raceduration, racedate)
    WelingTriLD18hWeekend.create_weling_tri_ld_18h_weekend_plan(athlete_id, coach_id, raceduration, racedate)
 end

end
