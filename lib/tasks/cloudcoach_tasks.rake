 desc "Create Greif Trainingplan"
	task create_greif_trainingplan: :environment do
	  athlete_id = 1
	  coach_id = 2
	  raceduration=3*3600 + 55*60 # 3h55 in seconds
	  racedate=DateTime.now+8.weeks
	  unitsperweek=4 #fixme: plan only for 4 units per week implemented
	  Cloudcoach.create_greif_plan(athlete_id, coach_id, raceduration, racedate, unitsperweek) #create a plan for Athlete with ID 1
	  
	  athlete_id = 2
	  coach_id = 1
	  raceduration=3*3600 + 45*60 # 3h45 in seconds
	  racedate=DateTime.now+8.weeks
	  unitsperweek=4 #fixme: plan only for 4 units per week implemented
	  Cloudcoach.create_greif_plan(athlete_id, coach_id, raceduration, racedate, unitsperweek) #create a plan for Athlete with ID 1
	  puts "create_greif_trainingsplan DONE"
    end


desc "Create Weling Triathlon Long Distance Trainingplan"
	task create_weling_tri_ld_18h_weekend_plan: :environment do
    Cloudcoach.create_weling_tri_ld_18h_weekend_plan(1,2,3600*12,DateTime.now+6.weeks)
     puts "create_weling_tri_ld_18h_weekend_plan DONE"
  end
