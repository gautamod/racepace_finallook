require 'progress_bar'

namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    create_sport_types #fill database with sport_type definitions
    import_sample_race_results #import sample race result files into db
    populate_users #create some users
    allocate_race_results #allcate existing race results to corresponding user
    create_relations #create some relations (follow) between users
    create_groups #will create groups/teams gem
    #create_virtual_races #will create some virtual_races
    #create_greif_trainingplan #create a Greif Trainingplan
    #create_weling_tri_ld_18h_weekend_plan
    #create_mqtt_messages #create some mqtt messages to simulate a mobile sending virtualrace data
    #upload_tcx_files #Upload some TCX files
    
  end
 

  
  #fill database with sport_type definitions
  task create_sport_types: :environment do
    create_sport_types
  end
   
  #cp sample json files from db/seeds/ and import into db
  task import_sample_race_results: :environment do
    import_sample_race_results
  end
    
  task populate_users: :environment do
    populate_users
  end
  
  task allocate_race_results: :environment do
    allocate_race_results
  end

  task create_relations: :environment do
    create_relations
  end

  task create_groups: :environment do
     create_groups
  end

  task create_greif_trainingplan: :environment do
    create_greif_trainingplan
  end

  task import_races: :environment do
    import_all_races_from_dir
  end

  task export_races: :environment do
    export_all_races_to_dir
  end
 
  task create_mqtt_messages: :environment do
    create_mqtt_messages
  end

  task create_virtual_races: :environment do
    create_virtual_races
  end

  task upload_tcx_files: :environment do
    upload_tcx_files
  end

end

#fill database with sport_type definitions
def create_sport_types
  Resultimporter::Engine.load_seed
end

#import sample race result files into db
def import_sample_race_results
    RaceImportExport.import_all_from_dir('db/seed/sample_race_result_files/')
end

def populate_users
  #create n random user which are in ImportedRaceResult
  #pick from the first 1000 records or the existing number of records
  record_range_limit = [1000,ImportedRaceResult.count].min #for performance improvement
  10.times do |n|
    rr=ImportedRaceResult.limit(1).offset(rand(record_range_limit)).first #Random result
    email = "#{rr.surname.to_s}.#{rr.name.to_s}@racespace.de".gsub(' ','').downcase
    password  = "password"
    if User.where(email: email).count == 0
      User.create!(email: email,password: password,confirmed_at: Time.now, confirmation_sent_at: Time.now)
    end
  end
  
  #create n known users
  sample_users=[["Nico","Weling"],["Jan","Frodeno"],["Sebastian","Kienle"],["Timo","Bracht"],["Arne","Gabius"]]
  sample_users.each do |surname,name|
    email = "#{surname}.#{name}@racespace.de".gsub(' ','').downcase
    password  = "password"
    if User.where(email: email).count == 0
      User.create!(email: email,password: password,confirmed_at: Time.now, confirmation_sent_at: Time.now)
    end
  end
  puts "make_users DONE"
end

#allcate existing race results to corresponding user
def allocate_race_results
  User.all.each do |user|
    raceresults=ImportedRaceResult.search {fulltext user.email.split('@')[0].gsub('.',' ') }.results
    raceresults.each do |raceresult|
      user.add_role(:racer, raceresult)
      puts "Allocating raceresult from athlete #{raceresult.surname} #{raceresult.name} to user #{user.email}"
    end
  end
end

#create some relations (follow) between users
def create_relations 
  users = User.all
  current_user  = User.find_by_email("nico.weling@racespace.de")
  followed_users = users[2..50]
  followers      = users[3..40]
  followed_users.each do |followed|
    if followed.id != current_user.id
      unless current_user.following?(followed)
        relationship = current_user.follow(followed)
        relationship.create_activity :create, owner: current_user, recipient: followed
        puts "User #{current_user.email} follow #{followed.email}"
     else
        puts "User #{current_user.email} already following #{followed.email}"
      end
    end
  end
  followers.each do |follower| 
    if follower.id != current_user.id
      unless follower.following?(current_user)
        relationship = follower.follow(current_user) 
        relationship.create_activity :create, owner: follower, recipient: current_user
        puts "User #{follower.email} follow #{current_user.email}"
      else
        puts "User #{follower.email} already following #{current_user.email}"
      end
    end
  end
end

def create_groups
   # #DeamTeam Groups for different VDOT levels and add Athletes to them
   # (1..16).to_a.each do |idx|
   #    group=Group.create(:name => "DreamTeam-sub"+(idx*5).to_s)
   #    from=idx*5
   #    to=(idx+1)*5
   #    Raceresult.where("rrvdot > ? and rrvdot <= ?", from, to).each do |rr|
   #       group.add(Athlete.find_by_id(rr.Athlete_id), as: 'athlete')
   #    end
   # end

  #First club
  group = Group.find_or_create_by(:name => "Triathlon Team Eupen")
  ImportedRaceResult.where("club LIKE '%tri%team%eupen%'").pluck(:name,:surname).each do | name, surname |
    email = "#{surname}.#{name}@racespace.de".gsub(' ','').downcase
    password  = "password"
    if User.where(email: email).count == 0 #create user and add to the group
      user=User.create!(email: email,password: password,confirmed_at: Time.now, confirmation_sent_at: Time.now)
    end
    User.find_by_email(email).add_role :athlete, group
  end
  User.find_by_email("nico.weling@racespace.de").add_role :coach, Group.find_or_create_by(:name => "Triathlon Team Eupen") #is an 'athlete' and a 'coach'
  
   
  #Second club
  group = Group.find_or_create_by(:name => "Triathlon Team Indeland")
  ImportedRaceResult.where("club LIKE '%indeland%'").pluck(:name,:surname).each do | name, surname |
    email = "#{surname}.#{name}@racespace.de".gsub(' ','').downcase
    password  = "password"
    if User.where(email: email).count == 0 #create user and add to the group
      user=User.create!(email: email,password: password,confirmed_at: Time.now, confirmation_sent_at: Time.now)
    end
    User.find_by_email(email).add_role :athlete, group
  end

  #Third club
  group = Group.find_or_create_by(:name => "Ute Mückel Sebamed Triathon Team")
  ImportedRaceResult.where("club LIKE '%ute%mückel%sebamed%'").pluck(:name,:surname).each do | name, surname |
    email = "#{surname}.#{name}@racespace.de".gsub(' ','').downcase
    password  = "password"
    if User.where(email: email).count == 0 #create user and add to the group
      user=User.create!(email: email,password: password,confirmed_at: Time.now, confirmation_sent_at: Time.now)
    end
    User.find_by_email(email).add_role :athlete, group
  end
  
  #Forth club
  group = Group.find_or_create_by(:name => "DLC Aachen")
  ImportedRaceResult.where("club LIKE '%dlc%aachen%'").pluck(:name,:surname).each do | name, surname |
    email = "#{surname}.#{name}@racespace.de".gsub(' ','').downcase
    password  = "password"
    if User.where(email: email).count == 0 #create user and add to the group
      user=User.create!(email: email,password: password,confirmed_at: Time.now, confirmation_sent_at: Time.now)
    end
    User.find_by_email(email).add_role :athlete, group
  end

  puts "create_teams DONE"
end


#create a Greif Marathon Trainingplan for some users
def create_greif_trainingplan
  10.times do |n| #create for 10 random users
    user = User.limit(1).offset(rand(User.count)).first
    coach_id = 1 #could be done nicer
    raceduration=rand(2..4)*3600 + rand(0..59)*60 # 3h55 in seconds
    racedate=DateTime.now+rand(1..8).weeks
    unitsperweek=4 #fixme: plan only for 4 units per week implemented
    coach_id = 1 #could be done nicer
    athlete_id = user.id
    Greif.create_greif_marathon_plan(athlete_id, coach_id, raceduration, racedate, unitsperweek)
  end

  user = User.where(email: "nico.weling@racespace.de").first
  unless user.nil?
    raceduration=2*3600 + 45*60 # 2h45 in seconds
    racedate=DateTime.now+8.weeks
    unitsperweek=4 #fixme: plan only for 4 units per week implemented
    coach_id = 1 #could be done nicer
    athlete_id = user.id
    Greif.create_greif_marathon_plan(athlete_id, coach_id, raceduration, racedate, unitsperweek)
   end

end

#Create Weling Triathlon Long Distance Trainingplan
def create_weling_tri_ld_18h_weekend_plan
  WelingTriLD18hWeekend.create_weling_tri_ld_18h_weekend_plan(1,2,3600*12,DateTime.now+6.weeks)
  puts "create_weling_tri_ld_18h_weekend_plan DONE"
end

#Create some virtual races
def create_virtual_races
  (0..4).to_a.each do |i|
    VirtualRace.create
  end
  puts "create_virtual_races DONE"
end

#create some mqtt messages to simulate a mobile sending virtualrace data
def create_mqtt_messages 
  mqtt_client = MQTT::Client.connect('localhost')
  user = User.where(email: "nico.weling@racespace.de").first
  (0..20).to_a.each do |i|
    msg = {"timestamp" => DateTime.now+i.second, "user_id" => user.id, "virtual_race_id" => 1, "latitude" => 50.629203+i/100.to_f*2, "longitude" => 6.033711+i/100.to_f*3}.to_json
    mqtt_client.publish('virtualrace1', msg)
  end

  user = User.where(email: "jan.frodeno@racespace.de").first
  (0..20).to_a.each do |i|
    msg = {"timestamp" => DateTime.now+i.second, "user_id" => user.id, "virtual_race_id" => 1, "latitude" => 50.629203+i/100.to_f*3, "longitude" => 6.033711+i/100.to_f*2}.to_json
    mqtt_client.publish('virtualrace1', msg)
  end

  mqtt_client.disconnect()
  puts "create_mqtt_messages DONE"
end


#Upload some tcx filex
def upload_tcx_files
  user = User.where(email: "nico.weling@racespace.de").first
  tcx_filenames=["db/seed/20160427-122049-1-1328-ANTFS-4-0.tcx",
             "db/seed/activity_444924928.tcx",
             "db/seed/activity_444924933.tcx"
              ]
  tcx_filenames.each do | tcx_filename |
    #workout=user.workouts.create(workout_file: File.open("db/seed/20160427-122049-1-1328-ANTFS-4-0.tcx", "r"))
    workout=user.workouts.create(workout_file: File.open(tcx_filename, "r"))
    if workout.save
      workout_file_with_path=Paperclip.io_adapters.for(workout.workout_file).path;
      tcx_json = Tcxtools.parse_tcx_file(workout_file_with_path);
      tcx_hash = JSON.parse(tcx_json);
      workout.update(completed_datetime: tcx_hash["activity_datetimeid"])
        tcx_hash['laps'].each do | lap_hash |
          lap=workout.laps.create(lap_hash.except('track'))
          track=Track.create(lap_id: lap.id)
          trackpoints=lap_hash['track']['trackpoints']
          bar= ProgressBar.new(trackpoints.count, :bar, :counter, :eta)
          trackpoints.each do |trackpoint| #just the first 1000 points
            track.trackpoints.create(trackpoint)
            bar.increment!
          end
        end
    else
      puts 'Failed to upload TCX file'
    end
  end
  puts 'TCX files successfully uploaded'
end


# def import_all_races_from_dir
#    RaceImportExport.import_all_from_dir("db/seeds/")
#    puts "import_all_races_from_dir DONE"
# end

# def export_all_races_to_dir
#    RaceImportExport.export_all_to_dir("db/seeds/")
#    puts "export_all_races_to_dir DONE"
# end

# def import_all_races_from_sample_dir #seed has been replace by import export
#    RaceImportExport.import_all_from_dir("db/sample_data/")
#    puts "import_all_races_from_sample_dir"
# end



# def make_athletes #Need to run after seed_athletes to keep id1... free
#     at1=Athlete.create!(atname: "Sebastian", atsurname: "Kienle",
#     atgender: "m")
#     at1.add_badge(1)
#     at1.save
    
#     at2=Athlete.create!(atname: "Timo", atsurname: "Bracht",
#     atgender: "m")
#     at2.add_badge(2)
#     at2.save
#     puts "make_athletes DONE"
# end

# def seed_mikatiming_races
#     load(Rails.root.join("db","sample_data","races_2014.rb"))
#     puts "seed_mikatiming_races DONE"
# end


# def seed_raceresults
#     load(Rails.root.join("db","sample_data","raceresults_rcid-1.rb"))
#     load(Rails.root.join("db","sample_data","raceresults_rcid-2.rb"))
#     load(Rails.root.join("db","sample_data","raceresults_rcid-3.rb"))
#     load(Rails.root.join("db","sample_data","raceresults_rcid-4.rb"))
#     load(Rails.root.join("db","sample_data","raceresults_rcid-5.rb"))
#     puts "seed_raceresults DONE"
# end
    

# #update rccity for two mikatiming races
# #the ironman race have fetched this info automatically
# def update_rccity_info
#  id1=Race.where("hasresults > 100").first.id
#  Race.find_by_id(id1).update(rccity: "Heidelberg") #langitude and longitude auto-set by google-api
#  id2=Race.where("hasresults > 100").second.id
#  Race.find_by_id(id2).update(rccity: "Hannover") #langitude and longitude auto-set by google-api
# end

# def calc_vdots
#     id1=Race.where("hasresults > 100").first.id
#     Vdothelper.update_vdot_column(id1, 21100) #distance in meters
#     id2=Race.where("hasresults > 100").second.id
#     Vdothelper.update_vdot_column(id2, 9000) #distance in meters
#     puts "calc_vdots DONE"
# end

# def make_microposts
#   #users = User.all(limit: 6)
#   users = User.all
#   3.times do
#     content = Faker::Lorem.sentence(2)
#     users.each { |user| user.microposts.create!(content: content) }
#   end
#     puts "make_microposts DONE"
# end

# def simulate_challenge
#   id1=Race.where("hasresults > 400").first.id
#   Simulatechallenge.simulate(id1,15) # race_id:1, no_participants:15
#   puts "simulate_challenge DONE"
# end


# #will create an activity with laps, tacks etc. Garmin-style
# def create_activity 
#   # me=User.first
#   # ac=me.activities.create #fixme  activity table is destroyed by merit gem!!!
#   # me.activities.last                       #show last activity
#   # lp=Lap.create(activity_id: 2, lpcalories: 1234)
#   # me.activities.last.laps.last             #show last lap
#   # Track.create(lap_id: me.activities.last.laps.last.id)
#   # puts "create_activity DONE"
# end

# def create_single_calendar_event
#   FullcalendarEngine::Event.create({ 
#     :title => 'D-Day', 
#     :description => 'Today is a good day to start with training', 
#     :starttime => Time.current, 
#     :endtime => Time.current + 10.minute
#   })
#   puts "create_single_calendar_event DONE"
# end

# def create_calendar_event_series
#   FullcalendarEngine::EventSeries.create({ 
#     :title => 'Wakeup heartrate', 
#     :description => 'Measure your heartrate after wakeup', 
#     :starttime => Time.current,
#     :endtime => Time.current + 10.minute, 
#     :period => 'weekly', 
#     :frequency => '4'
#    })
#   puts "create_calendar_event_series DONE"
# end