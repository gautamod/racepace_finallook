require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  
  fixtures :users, :imported_race_results

  before(:each) do
    Warden.test_mode!
  end
  
  after(:each) do
    Warden.test_reset!
  end

  before(:each) do
    request.env["HTTP_REFERER"] = "http://localhost:3000/imported_race_results/search?utf8=%E2%9C%93&query=nico+weling" #needed for redirect_to :back
  end

  it 'should not allow to allocate_race_result when not logged in' do
    get :allocate_race_result, id: 1
    
    expect(response).to redirect_to user_session_path
    expect(flash[:alert]).not_to be_nil
  end

  it 'should allow to allocate_race_result when logged in' do
    @user1 = users(:user1)
    sign_in @user1
    @imported_race_result1 = imported_race_results(:imported_race_result1)
    
    expect(@user1.roles.where(name: "racer").count).to eq(0)
    get :allocate_race_result, id: @imported_race_result1.id
    expect(flash[:success]).not_to be_nil
    expect(@user1.roles.where(name: "racer").count).to eq(1)
    #expect(assigns(:preference)).not_to be_nil
  end

  it 'should allow to unallocate_race_result an allocated result when logged in' do
    @user1 = users(:user1)
    sign_in @user1
    @imported_race_result1 = imported_race_results(:imported_race_result1)
    
    expect(@user1.roles.where(name: "racer").count).to eq(0)
    get :allocate_race_result, id: @imported_race_result1.id
    expect(flash[:success]).not_to be_nil
    expect(@user1.roles.where(name: "racer").count).to eq(1)
    
    get :unallocate_race_result, id: @imported_race_result1.id
    expect(flash[:success]).not_to be_nil
    expect(@user1.roles.where(name: "racer").count).to eq(0)
    #expect(assigns(:preference)).not_to be_nil
  end

  it 'should not allow to unallocate_race_result an result which is not allocated when logged in' do
    @user1 = users(:user1)
    sign_in @user1
    @imported_race_result1 = imported_race_results(:imported_race_result1)
    
    #expect(@user1.roles.where(name: "racer").count).to eq(0)
    #get :allocate_race_result, id: @imported_race_result1.id
    #expect(flash[:success]).not_to be_nil
    #expect(@user1.roles.where(name: "racer").count).to eq(1)
    
    get :unallocate_race_result, id: @imported_race_result1.id
    expect(flash[:success]).not_to be_nil
    expect(@user1.roles.where(name: "racer").count).to eq(0)
    #expect(assigns(:preference)).not_to be_nil
  end


end
