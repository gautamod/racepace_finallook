require 'rails_helper'

RSpec.describe Api::V1::ImportedRaceResultsController, type: :controller do

  fixtures :users

  before(:each) do
    Warden.test_mode!
  end
  
  after(:each) do
    Warden.test_reset!
  end

end
