require 'rails_helper'

RSpec.describe Api::V1::GroupsController, type: :controller do

  fixtures :users, :groups

  before(:each) do
    Warden.test_mode!
  end
  
  after(:each) do
    Warden.test_reset!
  end

  it 'should show index when not authenticated' do
    get :index, format: :json
    
    expect(response).to be_success
    expect(assigns(:groups)).not_to be_nil
  end
  
  it 'should show group when not authenticated' do
    get :show, id: 1, format: :json
    
    expect(response).to be_success
    expect(assigns(:group)).not_to be_nil
  end
  
  it 'should not show group which does not exist' do
    get :show, id: 9999, format: :json
    
    expect(response).to be_not_found
  end
  
  it 'should update group when authenticated' do
    @user1 = users(:user1)
    sign_in @user1
    
    @group = Group.where(id: 1).first
    expect(@group.name).not_to eq('newgroupname')
    
    patch :update, id: @group.id, group: { name: 'newgroupname' }, format: :json
    
    expect(response).to be_success
    expect(assigns(:group)).not_to be_nil

    @group = Group.where(id: 1).first
    expect(@group.name).to eq('newgroupname')
  end
  
  it 'should not update group when not authenticated' do
    @group = Group.where(id: 1).first
    expect(@group.name).not_to eq('newgroupname')
    
    patch :update, id: @group.id, group: { name: 'newgroupname' }, format: :json
    
    expect(response).to be_unauthorized

    @group = Group.where(id: 1).first
    expect(@group.name).not_to eq('newgroupname')
  end
  
  it 'should delete group when authenticated' do
    @user1 = users(:user1)
    sign_in @user1
    
    @groups = Group.where(id: 1)
    expect(@groups.length).to eq(1)
    
    delete :destroy, id: @groups.first.id, format: :json
    
    expect(response).to be_success

    @groups = Group.where(id: 1)
    expect(@groups.length).to eq(0)
  end

  it 'should not delete group when not authenticated' do
    @groups = Group.where(id: 1)
    expect(@groups.length).to eq(1)
    
    delete :destroy, id: @groups.first.id, format: :json
    
    expect(response).to be_unauthorized

    @groups = Group.where(id: 1)
    expect(@groups.length).to eq(1)
  end
  
  it 'should create group when authenticated' do
    @user1 = users(:user1)
    sign_in @user1
    
    post :create, group: { name: 'newgroupname', description: 'newgroupdesc' }, format: :json
    
    expect(response).to be_created
    
    @groups = Group.where(name: 'newgroupname')
    expect(@groups.length).to eq(1)
  end
  
  it 'should not create group when not authenticated' do
    post :create, group: { name: 'newgroupname', description: 'newgroupdesc' }, format: :json
    
    expect(response).to be_unauthorized
    
    @groups = Group.where(name: 'newgroupname')
    expect(@groups.length).to eq(0)
  end

end
