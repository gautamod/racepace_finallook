require 'rails_helper'

RSpec.describe Api::V1::PreferencesController, type: :controller do

  fixtures :users, :preferences

  before(:each) do
    Warden.test_mode!
  end
  
  after(:each) do
    Warden.test_reset!
  end

  it 'should not show preference when not authenticated' do
    get :show, id: 1
    
    expect(response).to be_unauthorized
  end

  it 'should show own preference when logged in' do
    @user1 = users(:user1)
    sign_in @user1
    
    get :show, id: @user1.id, format: :json
    
    expect(response).to be_success
    expect(assigns(:preference)).not_to be_nil
  end
  
  it 'should not show other preference when logged in' do
    @user1 = users(:user1)
    @user2 = users(:user2)
    sign_in @user2
    
    get :show, id: @user1.id, format: :json
    
    expect(response).to be_unauthorized
  end

  it 'should show own preference when authenticated' do
    @user1 = users(:user1)
    
    @request.headers['X-Api-User']     = @user1.email
    @request.headers['X-Api-Password'] = 'user1pass'
    
    get :show, id: @user1.id, format: :json
    
    expect(response).to be_success
    expect(assigns(:preference)).not_to be_nil
  end
  
  it 'should not show own preference when credentials are wrong' do
    @user1 = users(:user1)
    
    @request.headers['X-Api-User']     = @user1.email
    @request.headers['X-Api-Password'] = 'wrongpass'
    
    get :show, id: @user1.id, format: :json
    
    expect(response).to be_unauthorized
  end
  
  it 'should not show other preference when authenticated' do
    @user1 = users(:user1)
    @user2 = users(:user2)
    
    @request.headers['X-Api-User']     = @user2.email
    @request.headers['X-Api-Password'] = 'user2pass'
    
    get :show, id: @user1.id, format: :json
    
    expect(response).to be_unauthorized
  end
  
  it 'should not update preference when not authenticated' do
    @user1 = users(:user1)
    
    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).not_to eq('xx')
    
    patch :update, id: @preference.id, preference: { locale: 'xx' }, format: :json
    
    expect(response).to be_unauthorized

    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).not_to eq('xx')
  end

  it 'should update own preference when logged in' do
    @user1 = users(:user1)
    sign_in @user1
    
    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).not_to eq('xx')
    
    patch :update, id: @preference.id, preference: { locale: 'xx' }, format: :json
    
    expect(response).to be_success
    expect(assigns(:preference)).not_to be_nil

    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).to eq('xx')
  end

  it 'should update own preference when authenticated' do
    @user1 = users(:user1)
    
    @request.headers['X-Api-User']     = @user1.email
    @request.headers['X-Api-Password'] = 'user1pass'
    
    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).not_to eq('xx')
    
    patch :update, id: @preference.id, preference: { locale: 'xx' }, format: :json
    
    expect(response).to be_success
    expect(assigns(:preference)).not_to be_nil

    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).to eq('xx')
  end

  it 'should not update own preference when credentials are wrong' do
    @user1 = users(:user1)
    
    @request.headers['X-Api-User']     = @user1.email
    @request.headers['X-Api-Password'] = 'wrongpass'
    
    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).not_to eq('xx')
    
    patch :update, id: @preference.id, preference: { locale: 'xx' }, format: :json
    
    expect(response).to be_unauthorized

    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).not_to eq('xx')
  end

  it 'should not update other preference when authenticated' do
    @user1 = users(:user1)
    @user2 = users(:user2)
    
    @request.headers['X-Api-User']     = @user2.email
    @request.headers['X-Api-Password'] = 'user2pass'
    
    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).not_to eq('xx')
    
    patch :update, id: @preference.id, preference: { locale: 'xx' }, format: :json
    
    expect(response).to be_unauthorized

    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).not_to eq('xx')
  end
  
end
