require 'rails_helper'



RSpec.describe ImportedRaceResultsController do

 fixtures :users, :imported_race_results, :imported_races

  before(:each) do
    Warden.test_mode!
  end
  
  after(:each) do
    Warden.test_reset!
  end


  let(:result1) { imported_race_results(:imported_race_result1) }
  let(:race1) { imported_races(:imported_race1) }
  
   #RaceImportExport.import_race_from_file('db/seed/sample_race_result_files/race3__2013-10-12__ironman-world-championship____no2120.json')
  #before(:each) do
    load "#{Rails.root}/db/seed/weling_race_results.seeds.rb" 
    load "#{Rails.root}/db/seed/weling_races.seeds.rb" 
  #end  

  before { @result = result1 }
  subject { @result }
 
  it { should respond_to(:id) }
  it { should respond_to(:name) }
  puts "Number of imported_race_results: #{ImportedRaceResult.count.to_s}"
  puts "Number of imported_races: #{ImportedRace.count.to_s}"
  
  describe "allocate" do
  	let(:user) { users(:arnegabius) }
  	before do
  		puts user.email
  	end
	#raceresults=ImportedRaceResult.search {fulltext user.email.split('@')[0].gsub('.',' ') }.results
    #raceresults.each do |raceresult|
    #  user.add_role(:racer, raceresult)
    #  puts "Allocating raceresult from athlete #{raceresult.surname} #{raceresult.name} to user #{user.email}"
    #end
  end

end
