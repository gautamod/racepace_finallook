require 'rails_helper'

RSpec.describe PreferencesController, type: :controller do

  fixtures :users, :preferences

  before(:each) do
    Warden.test_mode!
  end
  
  after(:each) do
    Warden.test_reset!
  end

  it 'should not show preference when not logged in' do
    get :show, id: 1
    
    expect(response).to redirect_to user_session_path
    expect(flash[:alert]).not_to be_nil
  end

  it 'should show own preference when logged in' do
    @user1 = users(:user1)
    sign_in @user1
    
    get :show, id: @user1.id
    
    expect(response).to be_success
    expect(assigns(:preference)).not_to be_nil
  end
  
  it 'should not show other preference when logged in' do
    @user1 = users(:user1)
    @user2 = users(:user2)
    sign_in @user2
    
    get :show, id: @user1.id
    
    expect(response).to redirect_to root_path
    expect(flash[:alert]).not_to be_nil
  end

  it 'should not update preference when not logged in' do
    @user1 = users(:user1)
    
    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).not_to eq('xx')
    
    patch :update, id: @preference.id, preference: { locale: 'xx' }
    
    expect(response).to redirect_to user_session_path
    expect(flash[:alert]).not_to be_nil

    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).not_to eq('xx')
  end

  it 'should update own preference when logged in' do
    @user1 = users(:user1)
    sign_in @user1
    
    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).not_to eq('xx')
    
    patch :update, id: @preference.id, preference: { locale: 'xx' }
    
    expect(response).to redirect_to preference_path
    expect(flash[:success]).not_to be_nil

    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).to eq('xx')
  end

  it 'should not update other preference when logged in' do
    @user1 = users(:user1)
    @user2 = users(:user2)
    sign_in @user2
    
    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).not_to eq('xx')
    
    patch :update, id: @preference.id, preference: { locale: 'xx' }
    
    expect(response).to redirect_to root_path
    expect(flash[:alert]).not_to be_nil

    @preference = Preference.where(user_id: @user1.id).first
    expect(@preference.locale).not_to eq('xx')
  end
  
end
