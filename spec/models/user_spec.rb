require 'rails_helper'

RSpec.describe User do

  fixtures :users

  before { @user = users(:user1) }
  subject { @user }

  before(:each) do
    Warden.test_mode!
  end
  
  after(:each) do
    Warden.test_reset!
  end

  it { should respond_to(:id) }
  it { should respond_to(:email) }
  it { should respond_to(:encrypted_password) }
  it { should respond_to(:reset_password_token) }
  it { should respond_to(:reset_password_sent_at) }
  it { should respond_to(:remember_created_at) }
  it { should respond_to(:sign_in_count) }
  it { should respond_to(:current_sign_in_at) }
  it { should respond_to(:last_sign_in_at) }
  it { should respond_to(:current_sign_in_ip) }
  it { should respond_to(:last_sign_in_ip) }
  it { should respond_to(:confirmation_token) }
  it { should respond_to(:confirmed_at) }
  it { should respond_to(:confirmation_sent_at) }
  it { should respond_to(:unconfirmed_email) }
  it { should respond_to(:failed_attempts) }
  it { should respond_to(:unlock_token) }
  it { should respond_to(:locked_at) }
  it { should respond_to(:deactivated_at) }
  it { should respond_to(:uid) }
  it { should respond_to(:created_at) }
  it { should respond_to(:updated_at) }
  it { should respond_to(:avatar_file_name) }
  it { should respond_to(:avatar_content_type) }
  it { should respond_to(:avatar_file_size) }
  it { should respond_to(:avatar_updated_at) }
  it { should respond_to(:followers) }
  it { should respond_to(:following?) }
  it { should respond_to(:follow) }
  it { should respond_to(:stop_following) }
  
  describe "following" do
    let(:other_user) { users(:user2) }
    before do
      @user.save
      @user.follow(other_user)
    end

    #it { should be_following(other_user) }
    #its(:following_by_type) { should include(other_user) }
  
    describe "followed users" do
    subject { other_user }
    its(:followers) { should include(@user) }
    end

      describe "and stop_following" do
      before { @user.stop_following(other_user) }

      it { should_not be_following(other_user) }
      its(:followers) { should_not include(other_user) }
    end

  end
end
