require 'rails_helper'

RSpec.describe User do

  fixtures :users

  before { @user = users(:user1) }
  subject { @user }

  before(:each) do
    Warden.test_mode!
  end
  
  after(:each) do
    Warden.test_reset!
  end

  it { should respond_to(:id) }


end
