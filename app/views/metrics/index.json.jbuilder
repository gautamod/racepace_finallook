json.array!(@metrics) do |metric|
  json.extract! metric, :id, :user_id, :weight, :sleep_hours, :sleep_quality, :rest_hr, :max_hr, :bmi, :overall_feeling, :description
  json.url metric_url(metric, format: :json)
end
