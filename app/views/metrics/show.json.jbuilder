json.extract! @metric, :id, :user_id, :weight, :sleep_hours, :sleep_quality, :rest_hr, :max_hr, :bmi, :overall_feeling, :description, :created_at, :updated_at
