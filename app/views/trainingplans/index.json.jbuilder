json.array!(@trainingplans) do |trainingplan|
  json.extract! trainingplan, :Athlete_id, :tpracedate, :tpracetime, :tpunitsperweek
  json.url trainingplan_url(trainingplan, format: :json)
end