class ImportedRaceResultSerializer < ActiveModel::Serializer

  attributes :id, :name, :surname, :gender, :club
  attributes :total_time, :swim_time, :bike_time, :run_time
  
  link :self do
    href Rails.application.routes.url_helpers.api_v1_imported_race_result_path(object.id)
  end
  
end