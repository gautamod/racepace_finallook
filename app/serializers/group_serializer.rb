class GroupSerializer < ActiveModel::Serializer

  attributes :name, :description
  
  link :self do
    href Rails.application.routes.url_helpers.api_v1_group_path(object.id)
  end
  
end
