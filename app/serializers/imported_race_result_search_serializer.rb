class ImportedRaceResultSearchSerializer < ActiveModel::Serializer

  attributes :id, :name, :surname, :gender, :club, :imported_race_name, :imported_race_date

  link :self do
    href Rails.application.routes.url_helpers.api_v1_imported_race_result_path(object.id)
  end
  
  def imported_race_name
  	object.imported_race.name
  end
  
  def imported_race_date
  	object.imported_race.date
  end

end