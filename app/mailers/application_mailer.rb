class ApplicationMailer < ActionMailer::Base
  default from: ENV['MAILER_SMTP_SENDER']
  layout 'mailer'
end
