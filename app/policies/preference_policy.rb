class PreferencePolicy < ApplicationPolicy
  def permitted_attributes
    %i{ locale }
  end

  def show?
    user && (record.user_id == user.id)
  end

  def update?
    show?
  end
end
