class GroupPolicy < ApplicationPolicy
  def permitted_attributes
    %i{ name description }
  end

  def index?
    true
  end

  def show?
    true
  end

  def new?
    user
  end

  def edit?
    user
  end

  def create?
    user
  end

  def update?
    user
  end

  def destroy?
    user
  end
end
