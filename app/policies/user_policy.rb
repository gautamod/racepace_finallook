class UserPolicy < ApplicationPolicy
  def permitted_attributes
    %i{ } + if user && user.is_super_admin?
      [ role_ids: [] ]
    end
  end

  def show?
    user
  end

  def index?
    user
  end

  def show_roles?
    user && user.is_super_admin?
  end

  def update_roles?
    show_roles?
  end

  def allocate_race_result?
    true
  end
  
  def unallocate_race_result?
    true
  end
    
  def follow?
    true
  end
    
  def stop_following?
    true
  end
    
end

