class ImportedRaceResultPolicy < ApplicationPolicy
  def permitted_attributes
    %i{ }
  end

  def show?
    true
  end

  def search?
    true
  end
end
