# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

module_hello = angular.module 'hello', [ 'nvd3' ]

module_hello.controller 'graph1', [ '$scope', ($scope) ->
  $scope.options =
    chart:
      type: 'discreteBarChart'
      height: 450
      margin:
        top: 20,
        right: 20
        bottom: 60
        left: 55
      x: (d) ->
        return d.label
      y: (d) ->
        return d.value
      showValues: true
      valueFormat: (d) ->
        return d3.format(',.4f')(d)
      transitionDuration: 500
      xAxis:
        axisLabel: 'X Axis'
      yAxis:
        axisLabel: 'Y Axis'
        axisLabelDistance: 30

  $scope.data = [
    key: 'Cumulative Return'
    values: [
      label: "A"
      value: -29.765957771107
    ,
      label: "B"
      value: 0
    ,
      label: "C"
      value: 32.807804682612
    ,
      label: "D"
      value: 196.45946739256
    ,
      label: "E"
      value: 0.19434030906893
    ,
      label: "F"
      value: -98.079782601442
    ,
      label: "G"
      value: -13.925743130903
    ,
      label: "H"
      value: -5.1387322875705
    ]
  ]
]
