class Trainingplan < ActiveRecord::Base
  belongs_to :user
  has_many :planedunit, dependent: :destroy
  has_many :workout, dependent: :destroy

  resourcify #allow assignment of this resource to a role (rolify gem)

end
