class Preference < ActiveRecord::Base
  belongs_to :user

  validates :user_id, presence: true
  validates :locale, length: { maximum: 5 }
end
