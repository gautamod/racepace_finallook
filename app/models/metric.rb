class Metric < ActiveRecord::Base
  belongs_to :user
  has_one :event, class_name: "::FullcalendarEngine::Event", as: :ev, dependent: :destroy

end
