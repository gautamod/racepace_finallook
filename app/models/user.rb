class User < ActiveRecord::Base
  
  acts_as_voter
  acts_as_followable
  acts_as_follower

  resourcify

  #include PublicActivity::Common

  begin
    rolify
  rescue ActiveRecord::StatementInvalid
    # Rolify with "config.use_dynamic_shortcuts" enabled will throw an
    # exception if the table "roles" does not exist (yet). Because of
    # this the initial database setup (e.g. "rake db:setup") fails. As
    # a workaround we simply ignore this exception.
  end

  devise :async, :uid, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :lockable, :timeoutable, :omniauthable,
         :deactivatable
  
  has_one :preference, dependent: :destroy
  has_many :activity_uploads
  has_many :trainingplans, dependent: :destroy
  has_many :metrics, dependent: :destroy
  has_many :workouts, dependent: :destroy
  has_many :zones, dependent: :destroy

  before_create :build_default_preference
  
  # Method used by locale_setter to determine user-specific locale.
  def locale
    preference.locale
  end

private

  def build_default_preference
    build_preference(locale: '')
    true
  end
  
end
