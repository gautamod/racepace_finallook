class Group < ActiveRecord::Base
  validates :name, presence: true, length: { minimum: 3, maximum: 256 }
  validates :description, length: { maximum: 16384 }

  resourcify #allow assignment of this resource to a role (rolify gem)

end
