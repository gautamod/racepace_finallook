class Workout < ActiveRecord::Base
  belongs_to :sport_type
  belongs_to :trainingplan
  has_one :event, class_name: "::FullcalendarEngine::Event", as: :ev, dependent: :destroy
  has_many :laps, dependent: :destroy

  #has_many :event_series, class_name: "::FullcalendarEngine::EventSeries", foreign_key: "trainingplan_id", dependent: :destroy
  
  has_attached_file :workout_file
  	validates_attachment_content_type :workout_file, :content_type => ["application/octet-stream", "application/xml"]

end

