module ApplicationHelper

  def captcha_flash_error_needed?
    defined?(resource) &&
      !resource.errors.empty? &&
      !resource.errors.messages[:base].nil? &&
      resource.errors.messages[:base].include?(I18n.t('devise.invalid_captcha'))
  end

  def is_active_controller(controller_name)
    params[:controller] == controller_name ? "active" : nil
  end

  def is_active_action(action_name)
    params[:action] == action_name ? "active" : nil
  end

  def title(title = nil)
    if title.present?
      content_for :title, title
    else
      content_for?(:title) ? t('app.title') + ' | ' + content_for(:title) : t('app.title')
    end
  end

   def meta_keywords(tags = nil)
    if tags.present?
      content_for :meta_keywords, tags
    else
      content_for?(:meta_keywords) ? [content_for(:meta_keywords), APP_CONFIG['meta_keywords']].join(', ') : APP_CONFIG['meta_keywords']
    end
  end

  def meta_description(desc = nil)
    if desc.present?
      content_for :meta_description, desc
    else
      content_for?(:meta_description) ? content_for(:meta_description) : APP_CONFIG['meta_description']
    end
  end

end
