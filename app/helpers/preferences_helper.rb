module PreferencesHelper
  def locale_list
    list = [ [ t('common.language.auto'), '' ] ]
    Main::Application::config.i18n.available_locales.each do |locale|
      list << [ t('common.language.current', locale: locale), locale ]
    end
    list
  end
end
