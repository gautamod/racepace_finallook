module ImportedRaceResultsHelper

  def displayed_vdot(result)
    if result.vdot.to_f > 0 then
      result.vdot.round(1)
    elsif result.tritotaltime_vdot.to_f > 0 then
      result.tritotaltime_vdot.round(1)
    else 
      0
    end
  end

  def simple_encrypt(text)
  	if text.nil?
  		encr_text=""
  	elsif text.size==0
  		encr_text=""
  	else
	  	encr_text=(['*']*text.size).join
	  	encr_text[0]=text[0]
	  	encr_text[-1]=text[-1]
	end
  	encr_text
  end

end
