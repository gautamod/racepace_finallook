class ToolsController < ApplicationController
  # Allow access without login.
  skip_before_action :authenticate_user!
  
  skip_after_action :verify_authorized
  skip_after_action :verify_policy_scoped

  def calc_vdot

    #Durations
    @swim_h = params[:swim_duration_h]
    @swim_m = params[:swim_duration_m]
    @swim_s = params[:swim_duration_s]
    @swim_duration = hh_mm_ss2sec(@swim_h, @swim_m, @swim_s)

    @bike_h = params[:bike_duration_h]
    @bike_m = params[:bike_duration_m]
    @bike_s = params[:bike_duration_s]
    @bike_duration = hh_mm_ss2sec(@bike_h, @bike_m, @bike_s)
    
    @run_h = params[:run_duration_h]
    @run_m = params[:run_duration_m]
    @run_s = params[:run_duration_s]
    @run_duration = hh_mm_ss2sec(@run_h, @run_m, @run_s)
    
    @total_duration = @swim_duration.to_f + @bike_duration.to_f + @run_duration.to_f
    @total_duration_h = Time.at(@total_duration).utc.strftime("%H").to_i
    @total_duration_m = Time.at(@total_duration).utc.strftime("%M").to_i
    @total_duration_s = Time.at(@total_duration).utc.strftime("%S").to_i

    #Distances
    @swim_distance = distance2meters(params[:swim_distance],params[:swim_distance_unit])
    @bike_distance = distance2meters(params[:bike_distance],params[:bike_distance_unit])
    @run_distance = distance2meters(params[:run_distance],params[:run_distance_unit])
    
    @total_distance = @swim_distance.to_f + @bike_distance.to_f + @run_distance.to_f
    
    #Triathlon VDOT's
    @swim_vdot=Resultimporter.calc_sdot(@swim_distance, @swim_duration , @total_duration)
    @bike_vdot=Resultimporter.calc_bdot(@bike_distance, @bike_duration, @total_duration)
    @run_vdot=Resultimporter.calc_rdot(@run_distance, @run_duration, @total_duration)
    @total_vdot=Resultimporter.calc_tridot(@swim_distance, @bike_distance, @run_distance, @total_duration)


    #Jack Daniels Running VDOT
    @running_h = params[:running_duration_h]
    @running_m = params[:running_duration_m]
    @running_s = params[:running_duration_s]
    @running_duration = hh_mm_ss2sec(@running_h, @running_m, @running_s)
    @running_distance = distance2meters(params[:running_distance],params[:running_distance_unit])
    @vdot=Resultimporter.calc_vdot(@running_distance, @running_duration)

  end

  def qualigel
    @juice_ml = params[:juice_ml]
    @honney_ml = params[:honney_ml]
    @maltodextrin_g = params[:maltodextrin_g]
    @salt_g = params[:salt_g]
  
    @carbo_in_juice = @juice_ml.to_f * 17.5 / 100
    @carbo_in_honney = @honney_ml.to_f * 40 / 100
    @carbo_in_maltodextrin = @maltodextrin_g.to_f * 95 / 100
    @carbo_total = @carbo_in_juice + @carbo_in_honney + @carbo_in_maltodextrin
  end

  private

  def hh_mm_ss2sec (hh, mm, ss)
    hh.to_f*3600+mm.to_f*60+ss.to_f
  end
   
  def distance2meters (distance, unit)
    if unit == "m"
      distance_in_meters = distance.to_f
    elsif unit == "km"
      distance_in_meters = distance.to_f*1000
    elsif unit == "miles"
      distance_in_meters = distance.to_f*1609.34
    else
      puts "ERROR: unknown unit for distance: #{unit}"
      distance_in_meters = 0
    end
  end
      
        
end

