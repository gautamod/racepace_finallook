class WorkoutsController < ApplicationController

  skip_before_action :authenticate_user!
  
  skip_after_action :verify_authorized
  skip_after_action :verify_policy_scoped

  before_action :set_workout, only: [:show, :edit, :update, :destroy]

  # GET /workouts
  def index
    @workouts = Workout.all
  end

  # GET /workouts/1
  def show
    workout_filename=Paperclip.io_adapters.for(@workout.workout_file).path
    
    if workout_filename[-3..-1] == "tcx" or workout_filename[-3..-1] == "TCX"
      doc=Hash.from_xml(File.read(workout_filename))
      @doc = doc
      @creator_name = doc["TrainingCenterDatabase"]["Activities"]["Activity"]["Creator"]["Name"]
      @creator_unitid = doc["TrainingCenterDatabase"]["Activities"]["Activity"]["Creator"]["UnitId"]
      @creator_version = "#{doc["TrainingCenterDatabase"]["Activities"]["Activity"]["Creator"]["Version"]["VersionMajor"]}.#{doc["TrainingCenterDatabase"]["Activities"]["Activity"]["Creator"]["Version"]["VersionMinor"]}"
      @author_name = doc["TrainingCenterDatabase"]["Author"]["Name"]
      @author_build_version = "#{doc["TrainingCenterDatabase"]["Author"]["Build"]["Version"]["VersionMajor"]}.#{doc["TrainingCenterDatabase"]["Activities"]["Activity"]["Creator"]["Version"]["VersionMinor"]}"
      @author_langid = doc["TrainingCenterDatabase"]["Author"]["LangID"]
      @author_partnumber = doc["TrainingCenterDatabase"]["Author"]["PartNumber"]

      @activity_sport = doc["TrainingCenterDatabase"]["Activities"]["Activity"]["Sport"]
      @activity_id = doc["TrainingCenterDatabase"]["Activities"]["Activity"]["Id"]
    
      @laps = doc["TrainingCenterDatabase"]["Activities"]["Activity"]["Lap"].count.to_s
    end
    
    lap_num=0
    @marker_hash = Gmaps4rails.build_markers(@workout.laps) do | lap, marker| 
      lap_num+=1
      if lap.track.trackpoints.count > 0 #if there is a trackpoint
        marker.lat lap.track.trackpoints.last.tplatitudedegrees
        marker.lng lap.track.trackpoints.last.tplongitudedegrees
        marker.infowindow "Lap #{lap_num.to_s}\n Lap-Distance: #{lap.lpdistancemeters.round(2)}m"
        marker.picture({
          "picture" => 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|FF0000|000000',
          "width" =>  '16',        
          "height" => '16'
          })
        marker.json({:title => "laptitel"})
      end
    end

    @polylines=[]
    polyline=[]
    @workout.laps.each do | lap |
      @track=lap.track
      @track.trackpoints.each do | trackpoint |
          polyline << {"lng": trackpoint.tplongitudedegrees, "lat": trackpoint.tplatitudedegrees }
        end
      @polylines << polyline #polylines for gmaps
    end

  end

  # GET /workouts/new
  def new
    @workout = Workout.new
  end

  # GET /workouts/1/edit
  def edit
  end

  # POST /workouts
  def create
    if current_user
      @workout = current_user.workouts.create(workout_params)
      if @workout.save
        workout_file_with_path=Paperclip.io_adapters.for(@workout.workout_file).path
        tcx_json = Tcxtools.parse_tcx_file(workout_file_with_path)
        tcx_hash = JSON.parse(tcx_json)
        

        tcx_hash['laps'].each do | lap_hash |
          #create all laps
          lap=@workout.laps.create(lap_hash.except('track'))
          #create a track for each lap
          track=Track.create(lap_id: lap.id)
          trackpoints_hash=lap_hash['track']['trackpoints']
          #fixme add a rails progressbar
          #bar= ProgressBar.new(trackpoints.count, :bar, :counter, :eta)
          trackpoints_hash.each do |trackpoint_hash| 
            track.trackpoints.create(trackpoint_hash)
            #bar.increment!
          end
        end

        #create a fullcalendar event
        unless tcx_hash["activity_datetimeid"].nil?
          @workout.update(completed_datetime: tcx_hash["activity_datetimeid"])
          @workout.build_event(title: "TXC #{tcx_hash["activity_datetimeid"]}", 
              description: "-", 
              starttime: tcx_hash["activity_datetimeid"].to_datetime, 
              endtime: tcx_hash["activity_datetimeid"].to_datetime+2.hours, 
              all_day: false).save
              #fixme: endtime should be starttime + activity_duration
        end
        redirect_to :back, notice: 'Workout was successfully created.'
      else
        format.html { render :new }
        format.json { render json: @workout.errors, status: :unprocessable_entity }
        redirect_to :back, notice: 'Failed to create workout'
      end
    else
        redirect_to :back, notice: 'Have to be logged in'
    end

  end

  # PATCH/PUT /workouts/1
  def update
    if @workout.update(workout_params)
      redirect_to @workout, notice: 'Workout was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /workouts/1
  def destroy
    @workout.destroy
    redirect_to workouts_url, notice: 'Workout was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_workout
      @workout = Workout.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def workout_params
      params.require(:workout).permit(:sport_type_id, :title, :description, :planed_duration, :completed_duration, :planed_distance, :completed_distance, :planed_avg_pace, :completed_avg_pace, :planed_avg_speed, :completed_avg_speed, :planed_calories, :completed_calories, :planed_elevation_gain, :completed_elevation_gain, :completed_elevation_high, :completed_elevation_low, :planed_tss, :completed_tss, :planed_if, :completed_if, :planed_avg_hr, :completed_avg_hr, :completed_min_hr, :completed_max_hr, :planed_avg_power, :completed_avg_power, :completed_min_power, :completed_max_power, :autor, :target, :days_before_race, :planed_datetime, :completed_datetime, :athlete_id, :coach_id, :workout_file)
    end
end
