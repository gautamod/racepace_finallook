class VirtualRacesController < ApplicationController
  before_action :set_virtual_race, only: [:show, :edit, :update, :destroy]

  # GET /virtual_races
  # GET /virtual_races.json
  def index
    @virtual_races = VirtualRace.all
  end

  # GET /virtual_races/1
  # GET /virtual_races/1.json
  def show
  end

  # GET /virtual_races/new
  def new
    @virtual_race = VirtualRace.new
  end

  # GET /virtual_races/1/edit
  def edit
  end

  # POST /virtual_races
  # POST /virtual_races.json
  def create
    @virtual_race = VirtualRace.new(virtual_race_params)

    respond_to do |format|
      if @virtual_race.save
        format.html { redirect_to @virtual_race, notice: 'Virtual race was successfully created.' }
        format.json { render :show, status: :created, location: @virtual_race }
      else
        format.html { render :new }
        format.json { render json: @virtual_race.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /virtual_races/1
  # PATCH/PUT /virtual_races/1.json
  def update
    respond_to do |format|
      if @virtual_race.update(virtual_race_params)
        format.html { redirect_to @virtual_race, notice: 'Virtual race was successfully updated.' }
        format.json { render :show, status: :ok, location: @virtual_race }
      else
        format.html { render :edit }
        format.json { render json: @virtual_race.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /virtual_races/1
  # DELETE /virtual_races/1.json
  def destroy
    @virtual_race.destroy
    respond_to do |format|
      format.html { redirect_to virtual_races_url, notice: 'Virtual race was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_virtual_race
      @virtual_race = VirtualRace.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def virtual_race_params
      params.fetch(:virtual_race, {})
    end
end
