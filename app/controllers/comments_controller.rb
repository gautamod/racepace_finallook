class CommentsController < ApplicationController  
  before_action :authenticate_user!

  skip_after_action :verify_authorized
  skip_after_action :verify_policy_scoped

   def new
	@new_comment = Comment.new
	@comments = ''
	@commentable = ''
	if current_user
	    my_rr_ids = current_user.roles.where(name: "racer").pluck(:resource_id)
        @my_imported_race_results = ImportedRaceResult.where(id: my_rr_ids)
    else
        @my_imported_race_results = nil
    end
   end

  def create
    commentable = commentable_type.constantize.find(commentable_id)
    @comment = Comment.build_from(commentable, current_user.id, body)

    respond_to do |format|
      if @comment.save
        make_child_comment
        format.html  { redirect_to(:back, :notice => 'Comment was successfully added.') }
      else
        format.html  { render :action => "new" }
      end
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:body, :commentable_id, :commentable_type, :comment_id)
  end

  def commentable_type
    comment_params[:commentable_type]
  end

  def commentable_id
    comment_params[:commentable_id]
  end

  def comment_id
    comment_params[:comment_id]
  end

  def body
    comment_params[:body]
  end

  def make_child_comment
    return "" if comment_id.blank?

    parent_comment = Comment.find comment_id
    @comment.move_to_child_of(parent_comment)
  end

  def estimate_possible_team_mate_results my_imported_race_results
    possible_team_mate_results=[]
    if my_imported_race_results
      clubs=my_imported_race_results.pluck(:club)
      clubs.each do |club|
        if club != "" and club != nil
          possible_team_mate_results=ImportedRaceResult.where(club: club)
        end
      end
    end
    return possible_team_mate_results
  end

end  
