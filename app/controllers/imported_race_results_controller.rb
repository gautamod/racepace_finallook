class ImportedRaceResultsController < ApplicationController

  # Allow access without login.
  skip_before_action :authenticate_user!
  
  skip_after_action :verify_authorized
  skip_after_action :verify_policy_scoped

  def search
    # For now, access to search results is completely unrestricted.
    authorize ImportedRaceResult.new

    p_query = params[:query]

    if params[:page] && params[:page].kind_of?(String)
      p_page_number = params[:page].to_i
    end

    p_page_number = 1  unless p_page_number && p_page_number > 0
    p_page_size   = 20

    if p_page_number < 1 || p_page_number > 999999999 || p_page_size < 1 || p_page_size > 100
      flash.now[:danger] = '[# There were errors. #]'
      render action: :search
    end

    if p_query
 
      @search = ImportedRaceResult.solr_search(include: [:imported_race]) do
        keywords(p_query)
        paginate(page: p_page_number, per_page: p_page_size)
      end
      @results = @search.results
 
      @athlete_search = ImportedRaceResult.solr_search(include: [:imported_race]) do
        keywords(p_query)
        paginate(page: p_page_number, per_page: p_page_size)
      end
      @athletes = @athlete_search.results


      @race_search = ImportedRace.solr_search do
        keywords(p_query)
        paginate(page: p_page_number, per_page: p_page_size)
      end
      @races = @race_search.results
 
    end

    @teamnames = []
    @athletenames = []
    @results.each do | result | 

      unless @teamnames.include? result.club
        @teamnames << result.club
      end

      unless @athletenames.include? "#{result.surname} #{result.name}"
        @athletenames << "#{result.surname} #{result.name}"
      end

    end
    
 end
  
  def add_new_comment
    @raceresult = ImportedRaceResult.find(params[:id])
    Comment.build_from( @raceresult, current_user.id, params[:comment][:comment])
    @raceresult.create_activity :comment, owner: current_user
    redirect_to :back
  end

  def like
    @raceresult = ImportedRaceResult.find(params[:id])
    @raceresult.liked_by current_user
    @raceresult.create_activity :like, owner: current_user
    redirect_to :back
  end

  def unlike
    @raceresult = ImportedRaceResult.find(params[:id])
    @raceresult.unliked_by current_user
    @raceresult.create_activity :unlike, owner: current_user
    redirect_to :back
  end
  
  def show
    @imported_race_result = ImportedRaceResult.find(params[:id])
  end

  def index
    render nothing: true
  end
  
  def edit
    @imported_race_result = ImportedRaceResult.find(params[:id])
  end

  def update
    @raceresult.create_activity :update, owner: current_user
    render nothing: true
  end

  def new
    render nothing: true
  end

  def create
    @raceresult.create_activity :create, owner: current_user
    render nothing: true
  end

  def destroy
    @raceresult.create_activity :destroy, owner: current_user
    render nothing: true
  end
  
end
