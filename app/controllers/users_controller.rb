class UsersController < ApplicationController

  def show
    @user = User.find(params[:id])
    authorize @user
  end


  def index
    @users = policy_scope(User.all)
    authorize @users
  end

  # Roles

  def show_roles
    @user = User.find(params[:user_id])
    authorize @user
    @roles = @user.roles
  end

  def update_roles
    @user = User.find(params[:user_id])
    authorize @user
    
    @user.update(user_params)

    if @user.save
      flash[:success] = '[# Settings edited. #]'
      redirect_to user_roles_path
    else
      flash.now[:danger] = '[# There were errors. #]'
      render action: :show
    end
  end

  def allocate_race_result #this will assign this raceresult to the current_user
    authorize current_user
    raceresult = ImportedRaceResult.find(params[:id])
    allocations = Role.where(resource_type: "ImportedRaceResult").where(resource_id: raceresult.id)
    #allocations = ResultAllocation.where("imported_race_result_id= ?", raceresult.id)
    if allocations.count == 0 #result is not allocated
      #current_user.result_allocations.create(imported_race_result: raceresult)
      current_user.add_role(:racer, raceresult)
      raceresult.create_activity :allocate, owner: current_user
      flash[:success] = "This raceresult is successfully assigned to you!"
    elsif allocations.count == 1 #results is allready allocated
      if current_user.has_role?(:racer, raceresult)
        flash[:info] = "This raceresult is already assigned to you."
      else
        flash[:error] = "This raceresult is already assigned to #{allocation[0].user_id.email}!"
      end 
    else #count > 0 should be impossible because of 'has_one' relation in model
      flash[:error] = "ERROR: This raceresult is already assigned to #{allocations.count.to_s} persons!"
    end
    redirect_to :back
  end

  def unallocate_race_result #this will un-assign this raceresult to the current_user
    authorize current_user
    raceresult = ImportedRaceResult.find(params[:id])
    allocations = Role.where(resource_type: "ImportedRaceResult").where(resource_id: raceresult.id)
    #allocations = ResultAllocation.where("user_id= ? and imported_race_result_id= ?", current_user.id, raceresult.id)
    if allocations.count > 1 #count > 1 should be impossible because of 'has_one' relation in model
      allocations.destroy_all #fixme: maybe it's better to use delete_role if exists
      flash[:error] = "ERROR: This raceresult is assigned to #{allocations.count.to_s} persons!"
    else
      allocations.destroy_all #fixme: maybe it's better to use delete_role if exists
      raceresult.create_activity :unallocate, owner: current_user
      flash[:success] = "This raceresult is successfully released/unallocated!"
    end
    redirect_to :back
  end  

  def follow
    authorize current_user
    @user = User.find(params[:id])

    if current_user
      if current_user == @user
        flash[:error] = "You cannot follow yourself."
      else
        current_user.follow(@user)
        #RecommenderMailer.new_follower(@user).deliver if @user.notify_new_follower
        flash[:notice] = "You are now following #{@user.email}."
      end
    else
      flash[:error] = "You must <a href='/users/sign_in'>login</a> to follow #{@user.email}.".html_safe
    end
    redirect_to :back
  end

  def stop_following
    authorize current_user
    @user = User.find(params[:id])

    if current_user
      current_user.stop_following(@user)
      flash[:notice] = "You are no longer following #{@user.email}."
    else
      flash[:error] = "You must <a href='/users/sign_in'>login</a> to unfollow #{@user.email}.".html_safe
    end
    redirect_to :back
  end

private

  def user_params
    params.require(:user).permit(*policy(@user || User).permitted_attributes)
  end
  
end
