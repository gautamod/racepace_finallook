class ApplicationController < ActionController::Base

  include PublicActivity::StoreController
  include PunditHelper
  include NegativeCaptchaHelper

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  before_action :authenticate_user!
  
  after_action :verify_authorized, unless: :devise_controller?
  after_action :verify_policy_scoped, only: :index, unless: :devise_controller?

  # reset captcha code after each request for security
  after_action :reset_last_captcha_code!
  
  before_action :negative_captcha_setup
  before_action :negative_captcha_check
  before_action :load_activities

  layout :my_layout

  Thread.new do
    MQTT::Client.connect('localhost') do |c|
    # The block will be called when you messages arrive to the topic
      c.get('#') do |topic, message| #accept all messages 
        puts "#{topic}: #{message}"
        mqtt=MqttMessage.create(message: message, topic: topic)
        mqtt.update(
          virtual_race_id: JSON.parse(message)["virtual_race_id"].to_i,
          user_id: JSON.parse(message)["user_id"].to_i, 
          latitude: JSON.parse(message)["latitude"].to_f, 
          longitude: JSON.parse(message)["longitude"].to_f, 
          timestamp: JSON.parse(message)["timestamp"])
      end
    end
  end

  # MQTT::Client.connect('localhost') do |c|   c.publish('test', 'message') end
    
  private
    def my_layout
      devise_controller? ? "empty" : "application"
    end

    def load_activities
      if current_user
        @activities = PublicActivity::Activity.order("created_at desc").where(owner_id: [current_user.following_ids, current_user.id].flatten(1))
      else
        @activities = nil
      end
    end
    
    def after_sign_in_path_for(resource_or_scope)
	 '/dashboards/dashboard_0'
	end
  
end
