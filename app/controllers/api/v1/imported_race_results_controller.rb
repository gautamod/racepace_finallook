class Api::V1::ImportedRaceResultsController < Api::V1::ApplicationController

  def enforce_range(value, min, max)
    return min if value < min
    return max if value > max
    return value
  end

  api! "Show ImportedRaceResult specified by ID"
  def show
    # For now, access to search results is completely unrestricted.
    authorize ImportedRaceResult.new
    
    begin
      @object = ImportedRaceResult.find(params[:id])
    rescue
      head status: :not_found
      return
    end
    
    respond_to do |format|
      format.json { render json: @object }
    end
  end
    
  api! "Search Race Result by query string"
  def search
    # For now, access to search results is completely unrestricted.
    authorize ImportedRaceResult.new
    
    p_query = params[:query] || ''

    if params[:page] && params[:page].kind_of?(ActionController::Parameters)
      p_page_number = params[:page][:number].to_i
      p_page_size   = params[:page][:size].to_i
    end

    p_page_number = 1   unless p_page_number && p_page_number > 0
    p_page_size   = 25  unless p_page_size   && p_page_size   > 0

    if p_page_number < 1 || p_page_number > 999999999 || p_page_size < 1 || p_page_size > 100
      head status: :unprocessable_entity
      return
    end

    @search = ImportedRaceResult.solr_search do
      keywords(p_query)
      paginate(page: p_page_number, per_page: p_page_size)
    end
    
    links = {
      self: api_v1_imported_race_results_search_path,
    }
    meta = {
      count: @search.results.length
    }
    
    respond_to do |format|
      format.json { render json: @search.results, links: links, meta: meta, each_serializer: ImportedRaceResultSearchSerializer }
    end
  end

private

  def imported_race_result_params
    params.require(:imported_race_result).permit(*policy(@imported_race_result || ImportedRaceResult).permitted_attributes)
  end

end
