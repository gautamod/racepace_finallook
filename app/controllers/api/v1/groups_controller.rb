class Api::V1::GroupsController < Api::V1::ApplicationController

  before_action :set_group, only: [ :show, :edit, :update, :destroy ]



  api! 'Return the list of all groups'
  def index
    @groups = policy_scope(Group.all)
    authorize @groups

    respond_to do |format|
      format.json { render json: @groups }
    end
  end

  api! "Show group specified by id"
  param :id, :number, "ID of the group" 
  def show
    respond_to do |format|
      format.json { render json: @group }
    end
  end

  api! "New group (without beeing saved)"
  def new
    @group = Group.new
    authorize @group
    
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  api! "Edit group"
  param :id, :number, "ID of the group to edit" 
  def edit
    show
  end

  api! "Create new group"
  def create
    @group = Group.new(group_params)
    authorize @group
    
    respond_to do |format|
      if @group.save
        format.json { render json: @group, status: :created }
      else
        format.json { render json: @group, status: :unprocessable_entity, serializer: ActiveModel::Serializer::ErrorSerializer }
      end
    end
  end

  api! "Update group parameter"
  def update
    respond_to do |format|
      if @group.update(group_params)
        format.json { render json: @group }
      else
        format.json { render json: @group, status: :unprocessable_entity, serializer: ActiveModel::Serializer::ErrorSerializer }
      end
    end
  end

  api! "Delete group"
  def destroy
    @group.destroy
    
    respond_to do |format|
      format.json { head status: :no_content }
    end
  end

private

  def set_group
    begin
      @group = Group.find(params[:id])
      begin
        authorize @group
      rescue
        head status: :unauthorized
      end
    rescue
      head status: :not_found
    end
  end

  def group_params
    params.require(:group).permit(*policy(@group || Group).permitted_attributes)
  end

end
