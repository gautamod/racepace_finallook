class Api::V1::ApplicationController < ActionController::Base

  include PunditHelper

  respond_to :json

  # Prevent CSRF attacks.
  protect_from_forgery with: :null_session
  
  before_action :authenticate_api_user!
  
  after_action :verify_authorized
  after_action :verify_policy_scoped, only: :index

  def authenticate_api_user!
    api_user = request.headers['X-Api-User']
    api_pass = request.headers['X-Api-Password']

    if api_user && api_pass
      user = User.where(email: api_user).first
      
      if user && user.active_for_authentication? && user.valid_password?(api_pass)
        @current_user = user
      else
        @current_user = nil
      end
    end
  end
  
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  
  def user_not_authorized
    head status: :unauthorized
    false
  end

end
