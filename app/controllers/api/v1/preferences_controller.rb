class Api::V1::PreferencesController < Api::V1::ApplicationController

  api! "Show Preference specified by ID"
  error 401, "Unauthorized"
  def show
    @preference = Preference.find(params[:id])
    authorize @preference
    
    respond_to do |format|
      format.json { render json: @preference }
    end
  end

  api! "Update Preference specified by ID"
  error 401, "Unauthorized"
  def update
    @preference = Preference.find(params[:id])
    authorize @preference

    @preference.update(preference_params)
    
    if @preference.save
      respond_to do |format|
        format.json { head :no_content, status: :ok }
      end
    else
      respond_to do |format|
        format.json { render json: @preference.errors, status: :unprocessable_entity }
      end
    end
  end

private

  def preference_params
    params.require(:preference).permit(*policy(@preference || Preference).permitted_attributes)
  end

end
