class PlanedunitsController < ApplicationController

  skip_before_action :authenticate_user!
  
  skip_after_action :verify_authorized
  skip_after_action :verify_policy_scoped

  before_action :set_planedunit, only: [:show, :edit, :update, :destroy]

  # GET /planedunits
  # GET /planedunits.json
  def index
    @planedunits = Planedunit.all
  end

  # GET /planedunits/1
  # GET /planedunits/1.json
  def show
  end

  # GET /planedunits/new
  def new
    @planedunit = Planedunit.new
  end

  # GET /planedunits/1/edit
  def edit
  end

  # POST /planedunits
  # POST /planedunits.json
  def create
    @planedunit = Planedunit.new(planedunit_params)

    respond_to do |format|
      if @planedunit.save
        format.html { redirect_to @planedunit, notice: 'Planedunit was successfully created.' }
        format.json { render action: 'show', status: :created, location: @planedunit }
      else
        format.html { render action: 'new' }
        format.json { render json: @planedunit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /planedunits/1
  # PATCH/PUT /planedunits/1.json
  def update
    respond_to do |format|
      if @planedunit.update(planedunit_params)
        format.html { redirect_to @planedunit, notice: 'Planedunit was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @planedunit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /planedunits/1
  # DELETE /planedunits/1.json
  def destroy
    @planedunit.destroy
    respond_to do |format|
      format.html { redirect_to planedunits_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_planedunit
      @planedunit = Planedunit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def planedunit_params
      params.require(:planedunit).permit(:trainingplan_id, :pusporttypeid, :pudate, :puunitpoolid, :pudistance)
    end
end
