class PreferencesController < ApplicationController

  def show
    @preference = Preference.find(params[:id])
    authorize @preference
  end

  def update
    @preference = Preference.find(params[:id])
    authorize @preference

    @preference.update(preference_params)
    
    if @preference.save
      flash[:success] = '[# Settings edited. #]'
      redirect_to preference_path
    else
      flash.now[:danger] = '[# There were errors. #]'
      render action: :show
    end
  end

private

  def preference_params
    params.require(:preference).permit(*policy(@preference || Preference).permitted_attributes)
  end
  
  # Don't let locale_setter add the locale param to a POST request. We rather use the
  # locale value from the submitted form, since the user might have changed it.
  def default_url_options(options = {})
    if request.method == "GET"
      super(options)
    else
      options
    end
  end

end
