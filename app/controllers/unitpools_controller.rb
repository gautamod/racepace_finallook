class UnitpoolsController < ApplicationController
  before_action :set_unitpool, only: [:show, :edit, :update, :destroy]

  # GET /unitpools
  # GET /unitpools.json
  def index
    @unitpools = Unitpool.all
  end

  # GET /unitpools/1
  # GET /unitpools/1.json
  def show
  end

  # GET /unitpools/new
  def new
    @unitpool = Unitpool.new
  end

  # GET /unitpools/1/edit
  def edit
  end

  # POST /unitpools
  # POST /unitpools.json
  def create
    @unitpool = Unitpool.new(unitpool_params)

    respond_to do |format|
      if @unitpool.save
        format.html { redirect_to @unitpool, notice: 'Unitpool was successfully created.' }
        format.json { render action: 'show', status: :created, location: @unitpool }
      else
        format.html { render action: 'new' }
        format.json { render json: @unitpool.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /unitpools/1
  # PATCH/PUT /unitpools/1.json
  def update
    respond_to do |format|
      if @unitpool.update(unitpool_params)
        format.html { redirect_to @unitpool, notice: 'Unitpool was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @unitpool.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /unitpools/1
  # DELETE /unitpools/1.json
  def destroy
    @unitpool.destroy
    respond_to do |format|
      format.html { redirect_to unitpools_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_unitpool
      @unitpool = Unitpool.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def unitpool_params
      params.require(:unitpool).permit(:upname, :upnoiv, :upivpace, :upivdistance, :upivhr, :uppausedistance, :uppausepace)
    end
end
