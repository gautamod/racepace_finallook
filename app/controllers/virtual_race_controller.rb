class VirtualRaceController < ApplicationController

  skip_before_action :authenticate_user!
  
  skip_after_action :verify_authorized
  skip_after_action :verify_policy_scoped

  def race_view
    @marker_hash = Gmaps4rails.build_markers(MqttMessage.all) do | mqtt, marker| 
      marker.lat mqtt.latitude 
      marker.lng mqtt.longitude 
      marker.infowindow "#{User.find_by_id(mqtt.user_id).email if mqtt.user_id} #{mqtt.timestamp}"
      marker.picture({
        "picture" => 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|FF0000|000000',
        "width" =>  '16',        
        "height" => '16'
        })
      marker.json({:title => "testtitel"})
    end

    @polylines=[]
    MqttMessage.group(:user_id).each do |mqtt|
      polyline=[]
      MqttMessage.where(user_id: mqtt.user_id).each do |this_users_mqtt|
        polyline << {"lng": this_users_mqtt.longitude, "lat": this_users_mqtt.latitude }
      end

      @polylines << polyline
    end


    # @polylines= [
    #    [
    #    {"lng": mqtt.longitude, "lat": mqtt.latitude, "strokeColor": "#000"},
    #    {"lng": -66.118292, "lat": 18.466465},
    #    {"lng": -64.75737, "lat": 32.321384}
    #    ],
    #    [
    #    {"lng":   -78, "lat":   18, "strokeColor": "#0F00F0"},
    #    {"lng":  -66, "lat":  25},
    #    {"lng": -64, "lat": 30}
    #    ]
    #   ]

  end

end
