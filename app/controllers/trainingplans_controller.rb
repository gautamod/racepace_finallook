class TrainingplansController < ApplicationController

  skip_before_action :authenticate_user!
  
  skip_after_action :verify_authorized
  skip_after_action :verify_policy_scoped

  before_action :set_trainingplan, only: [:show, :edit, :update, :destroy]

  # GET /trainingplans
  # GET /trainingplans.json
  def index

    #update selected colors and show options
    params[:trainingplan_ids] && params[:trainingplan_ids].each_with_index do |id, index|
      Trainingplan.find_by_id(id).update(color: params[:trainingplan_colors][index])
      if params[:show_ids].to_a.include?(id) #then it should be shown
       Trainingplan.find_by_id(id).update(show: true)
      else
       Trainingplan.find_by_id(id).update(show: false)
      end
    end 

    @trainingplans = Trainingplan.all
    if current_user.nil?
      @my_trainingplans = []
    else
      #get current assigned trainingplans 
      @my_trainingplans = current_user.trainingplans
    end
  end

  # GET /trainingplans/1
  # GET /trainingplans/1.json
  def show
    gon.events_title=[]
    @events_title = []
    gon.events_startdatetime = []
    @events_startdatetime = []
    @trainingplan.workout.each do |workout| 
       gon.events_title << workout.title
       gon.events_startdatetime << workout.planed_datetime
  end
   # commontator_thread_show(@trainingplan)
  end

  # GET /trainingplans/new
  def new
    @trainingplan = Trainingplan.new
    @trainingplan.update(user_id: current_user.id)
  end

  # GET /trainingplans/1/edit
  def edit
  end

  # POST /trainingplans
  # POST /trainingplans.json
  def create
    @trainingplan = Trainingplan.new(trainingplan_params)

    respond_to do |format|
      if @trainingplan.save
        format.html { redirect_to @trainingplan, notice: 'Trainingplan was successfully created.' }
        format.json { render action: 'show', status: :created, location: @trainingplan }
      else
        format.html { render action: 'new' }
        format.json { render json: @trainingplan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trainingplans/1
  # PATCH/PUT /trainingplans/1.json
  def update
    respond_to do |format|
      if @trainingplan.update(trainingplan_params)
        format.html { redirect_to @trainingplan, notice: 'Trainingplan was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @trainingplan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trainingplans/1
  # DELETE /trainingplans/1.json
  def destroy
    @trainingplan.destroy
    respond_to do |format|
      format.html { redirect_to trainingplans_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trainingplan
      @trainingplan = Trainingplan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trainingplan_params
      params.require(:trainingplan).permit(:Athlete_id, :tpracedate, :tpracetime, :tpunitsperweek)
    end
end
