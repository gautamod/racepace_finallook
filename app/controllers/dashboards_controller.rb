class DashboardsController < ApplicationController
  skip_before_action :authenticate_user!
  
  skip_after_action :verify_authorized
  skip_after_action :verify_policy_scoped


  def dashboard_0
    if current_user
      my_rr_ids=current_user.roles.where(name: "racer").pluck(:resource_id)
      @my_imported_race_results = ImportedRaceResult.where(id: my_rr_ids)
    else
      @my_imported_race_results = nil
    end
    @possible_team_mate_results=estimate_possible_team_mate_results(@my_imported_race_results)
  end
  
  def dashboard_1
    @activities = PublicActivity::Activity.order("created_at desc")
  end
 
  #Check for the clubs of the imported_race_results from the current user
  #and find imported_race_results of the same club
  def estimate_possible_team_mate_results my_imported_race_results
    possible_team_mate_results=[]
    if my_imported_race_results
      clubs=my_imported_race_results.pluck(:club)
      clubs.each do |club|
        if club != "" and club != nil
          possible_team_mate_results=ImportedRaceResult.where(club: club)
        end
      end
    end
    return possible_team_mate_results
  end
     
end
