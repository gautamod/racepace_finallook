source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.4'

# Use sqlite3 as the database for Active Record
gem 'sqlite3'

# Use mysql2 as the database for Active Record
#gem 'mysql'
gem 'mysql2', '~> 0.3.13'
# Use postgres as the database for Active Record
#gem 'pg'
#gem 'taps'

# Use Haml as the templating engine
gem 'haml-rails'
gem 'redcarpet'

# Use SCSS for stylesheets
#gem 'sass-rails', '~> 5.0'
gem 'sass-rails', '~> 5.0.4'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'

# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'

# Use Bower for frontend package management
gem 'bower-rails', github: 'rharriso/bower-rails'

# Use Angular.js and helper gems for frontend
gem 'angular-rails-templates', '0.2.0'
gem 'ngannotate-rails'

# Use Bootstrap
# gem 'bootstrap-sass'   # this is managed by Bower now
gem 'bootstrap_form'
gem 'autoprefixer-rails'

# Font
gem 'font-awesome-rails', '4.3.0.0'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
gem 'active_model_serializers', '~> 0.10.0.rc4', git: 'https://github.com/rails-api/active_model_serializers.git', branch: 'master'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'
# gem 'bcrypt', '3.1.9' #needed maybe in gautam env

gem 'bcrypt-ruby', '3.1.1.rc1', :require => 'bcrypt'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
group :development do
  gem 'capistrano', '3.4.0'
  gem 'capistrano-rails'
  gem 'capistrano-rails-collection'
  gem 'capistrano-rbenv'
  gem 'capistrano-passenger'
  gem 'capistrano-scm-copy'
  gem 'ruby-termios'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
 # gem 'spring'
end

gem 'bullet'

# Development, debugging and profiling tools
group :development, :test do
  gem 'byebug'
  
  gem 'better_errors'
  gem 'binding_of_caller'
  
  gem 'pry-rails'
  gem 'erd'

  gem 'ruby-prof'
  
  gem 'foreman'
end

# Testing tools
group :development, :test do
  gem 'rspec-rails', '~> 3.4.0'
  gem 'rspec-its'

  gem 'mocha'
  gem 'factory_girl_rails'
  gem 'fixture_builder'

  gem 'capybara'
  gem 'capybara-webkit'
  gem 'connection_pool'

  gem 'simplecov'
  
  gem 'jasmine-rails'
  gem 'phantomjs'
  gem 'poltergeist'

  gem 'seed_dump'
  gem 'database_cleaner'
end

group :test do
  #gem 'vcr'
  gem 'webmock'
end

# Visualization
gem 'leaflet-rails'

# Administration
gem 'rails-admin'

# Localization
gem 'locale_setter'

# Datastore
gem 'redis'
gem 'redis-namespace'

# Background processing
gem 'sidekiq'

# File upload
gem 'paperclip'

# Authentication / Authorization
gem 'devise', '3.4.1'
gem 'devise-bootstrap-views'
gem 'devise_security_extension', '0.9.2'
gem 'devise_deactivatable'
gem 'devise_uid'
gem 'devise-async'
gem 'easy_captcha', '0.6.4'
gem 'negative_captcha'
gem 'pundit'
gem 'rolify'

# OAuth
gem 'omniauth'
gem 'omniauth-twitter'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'omniauth-gplus'
gem 'omniauth-fitbit'
gem 'omniauth-runkeeper'
gem 'omniauth-strava'

# Pagination
gem 'will_paginate'
gem 'bootstrap-will_paginate'
gem 'kaminari'

# R
gem 'rserve-client'
gem 'rserve-simpler'

# Strava
gem 'strava-api-v3'

# Search
gem 'sunspot_rails'

group :development, :test do
  gem 'sunspot_solr'
end

gem 'secondbase'
gem 'gon'

# Application components
#gem 'resultcrawler',  '>= 3.0.1', git: 'ssh://git@bitbucket.org/sports4u/resultcrawler.git',  branch: 'master'
gem 'resultimporter', '>= 3.5.7', git: 'ssh://git@bitbucket.org/sports4u/resultimporter.git', branch: 'master'
gem 'resultanalyzer', '>= 1.1.7', git: 'ssh://git@bitbucket.org/sports4u/resultanalyzer.git', branch: 'master'
gem 'descriptive_statistics' #required by resultanalyzer
gem 'fullcalendar_engine', '>=1.1.5', :git => "ssh://git@bitbucket.org/racingnetwork/rb_fullcalendar-rails-engine.git", branch: 'master'
gem 'momentjs-rails' #required for fullcalendar time conversion
#gem 'simple_form'
gem 'public_activity'
gem 'acts_as_votable'
gem 'acts_as_follower'
gem 'gmaps4rails'
gem 'awesome_nested_set'
gem 'acts_as_commentable_with_threading'
gem 'apipie-rails'
